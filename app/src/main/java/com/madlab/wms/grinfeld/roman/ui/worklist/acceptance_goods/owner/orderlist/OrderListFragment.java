package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.ui.MainActivity;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.OrderPagerFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.CheckOrderFragment;
import com.madlab.wms.grinfeld.roman.utils.DateParser;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.CheckOrderFragment.LOAD_FROM_FRAGMENT;

/**
 * Created by GrinfeldRa
 */
public class OrderListFragment extends MvpAppCompatFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, OrderListFragmentView, View.OnClickListener, KeyEventListener {

    private static final String KEY_CODE = "key_code";
    private static final String KEY_TYPE = "key_type";
    private Context context;
    private TextView txt_name, txt_date, txt_supplier, txt_contractor, empty_view;
    private ProgressOwner progressOwner;
    private ArrayList<Order> data;
    private ArrayAdapter<Order> orderListAdapter;
    private Order order;
    private ListView listView;
    private boolean isReloadData = true;

    @InjectPresenter
    OrderListPresenter presenter;


    @ProvidePresenter
    OrderListPresenter providePresenter() {
        return new OrderListPresenter();
    }

    public static OrderListFragment newInstance(String code, int type) {
        Bundle args = new Bundle();
        args.putString(KEY_CODE, code);
        args.putInt(KEY_TYPE, type);
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ProgressOwner) {
            progressOwner = (ProgressOwner) context;
        }
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).addKeyEventHandler(this);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            ((BaseFragmentActivity) getActivity()).setActionBarTitle(getString(R.string.order_list));
        }
        return inflater.inflate(R.layout.order_list_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        listView = view.findViewById(R.id.list_view);
        txt_name = view.findViewById(R.id.txt_name);
        txt_date = view.findViewById(R.id.txt_date);
        txt_supplier = view.findViewById(R.id.txt_supplier);
        txt_contractor = view.findViewById(R.id.txt_contractor);
        empty_view = view.findViewById(R.id.emptyElement);
        FloatingActionButton fb_next = view.findViewById(R.id.fb_next);
        fb_next.setOnClickListener(this);
        if (getArguments() != null) {
            String code = getArguments().getString(KEY_CODE);
            int type = getArguments().getInt(KEY_TYPE);
            if (isReloadData) {
                data = new ArrayList<>();
                orderListAdapter = new ArrayAdapter<>(context, R.layout.list_item_universal, data);
                presenter.loadOrders(code, type);
            }
        }

        listView.setAdapter(orderListAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnKeyListener(this);
        listView.setOnItemSelectedListener(this);
    }



    @Override
    public void onDetach() {
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).removeKeyEventHandler(this);
        }
        super.onDetach();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        parent.requestFocusFromTouch();
        parent.setSelection(position);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        order = (Order) parent.getItemAtPosition(position);
        txt_name.setText(getString(R.string.order_name, order.getName()));
        txt_date.setText(getString(R.string.date_order, DateParser.getDateFromLong(order.getDate())));
        txt_supplier.setText(getString(R.string.supplier, order.getClient()));
        txt_contractor.setText(getString(R.string.contractor, order.getHolder()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCompleteGetNumberOrder(Order order) {
        if (getActivity() != null && getArguments() != null) {
            int type = getArguments().getInt(KEY_TYPE);
            ((MainActivity) getActivity()).changeFragment(OrderPagerFragment.newInstance(order, type));
        }
    }

    @Override
    public void onCompleteLoadOrders(List<Order> orderList) {
        data.clear();
        data.addAll(orderList);
        if (data.size() > 0) {
            orderListAdapter.notifyDataSetChanged();
            listView.requestFocusFromTouch();
            listView.setSelection(0);
        } else {
            empty_view.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fb_next) {
            startReception();
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    isReloadData = true;
                    startReception();
                }
                return true;
            case KeyEvent.KEYCODE_L:
                if (getActivity() != null && event.getAction() == KeyEvent.ACTION_DOWN && v.getId() == R.id.list_view) {
                    isReloadData = false;
                    ((MainActivity) getActivity()).changeFragment(CheckOrderFragment.newInstance(order, LOAD_FROM_FRAGMENT));
                }
                return true;
            case KeyEvent.KEYCODE_O:
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    startUpdateDate();
                }
                return true;
            default:
                return false;
        }
    }

    private void startUpdateDate() {
        if (getArguments() != null) {
            String code = getArguments().getString(KEY_CODE);
            int type = getArguments().getInt(KEY_TYPE);
            new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.update_data))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) -> presenter.loadOrders(code, type))
                    .setNegativeButton(getString(R.string.no), null)
                    .show();
        }
    }

    private void startReception() {
        if (order != null && getArguments() != null) {
            int type = getArguments().getInt(KEY_TYPE);
            new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.start_reception, order.getName()))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) -> presenter.getNumberOrder(type, order))
                    .setNegativeButton(getString(R.string.no), null)
                    .show();
        }
    }

}
