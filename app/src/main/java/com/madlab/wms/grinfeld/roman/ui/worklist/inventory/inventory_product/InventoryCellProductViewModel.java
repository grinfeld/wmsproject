package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by grinfeldra
 */
public class InventoryCellProductViewModel extends ViewModel {

    private final MutableLiveData<Map<String, List<InventoryCellProduct>>> liveData = new MutableLiveData<>();

    private Map<String, List<InventoryCellProduct>> map = new LinkedHashMap<>();

    public MutableLiveData<Map<String, List<InventoryCellProduct>>> getLiveData() {
        return liveData;
    }


    public void addOrReplace(InventoryCellProduct product) {
        String key = product.getInventoryType() == Command.TYPE_CELL ? product.getCodeGoods() : product.getAdressCell();
        if (map != null) {
            if (map.containsKey(key)) {
                List<InventoryCellProduct> inventoryCellProducts = map.get(key);
                if (inventoryCellProducts != null) {
                    List<InventoryCellProduct> myProducts = new ArrayList<>(inventoryCellProducts);
                    for (int i = 0; i < myProducts.size(); i++) {
                        if (myProducts.get(i) == product) {
                            myProducts.set(i, product);
                        }
                    }
                    map.put(key, myProducts);
                }
            } else {
                map.put(key, Collections.singletonList(product));
            }
            liveData.setValue(map);
        }
    }


    void clearProductAndAllSeries(String key) {
        if (map != null) {
            List<InventoryCellProduct> inventoryCellProducts = map.get(key);
            if (inventoryCellProducts != null) {
                List<InventoryCellProduct> myProducts = new ArrayList<>(inventoryCellProducts);
                for (InventoryCellProduct product : myProducts) {
                    product.setEdited(true);
                    product.setCountFact(0);
                    product.setCountDefective(0);
                }
                map.put(key, myProducts);
            }
            liveData.setValue(map);
        }
    }

    void clearData() {
        if (map != null) {
            map.clear();
            liveData.setValue(map);
        }
    }

    void addOrNewSeries(InventoryCellProduct product) {
        String key = product.getInventoryType() == Command.TYPE_CELL ? product.getCodeGoods() : product.getAdressCell();
        if (map != null) {
            if (map.containsKey(key)) {
                addNewSeries(product);
            } else {
                map.put(key, Collections.singletonList(product));
            }
            liveData.setValue(map);
        }
    }

    public void addNewSeries(InventoryCellProduct product) {
        String key = product.getInventoryType() == Command.TYPE_CELL ? product.getCodeGoods() : product.getAdressCell();
        if (map != null) {
            List<InventoryCellProduct> inventoryCellProducts = map.get(key);
            if (inventoryCellProducts != null) {
                List<InventoryCellProduct> myProducts = new ArrayList<>(inventoryCellProducts);
                myProducts.add(product);
                map.put(key, myProducts);
            }
            liveData.setValue(map);
        }
    }


}
