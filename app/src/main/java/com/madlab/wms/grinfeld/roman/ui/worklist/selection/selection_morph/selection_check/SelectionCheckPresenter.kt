package com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.selection_check

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.selection.Order
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class SelectionCheckPresenter : BasePresenter<SelectionCheckView>() {

    fun reserveSelectionOrder(order: Order) {
        mCompositeDisposable.add(WMSClient().sendArray(Command.COM_RESERVE_SELECTION_BOXES(),
                Command.COM_RESERVE_SELECTION_BOXES_PARAM(order))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val dao = App.getDatabase().orderSelectionDao()
                        dao.delete(order)
                        dao.deleteAllOrderItem(order.id)
                        viewState.onCompleteReserveSelectionOrder()
                    }
                })
    }
}