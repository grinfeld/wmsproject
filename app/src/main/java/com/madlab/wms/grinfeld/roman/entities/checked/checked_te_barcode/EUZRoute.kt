package com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import com.madlab.wms.grinfeld.roman.utils.Utils

/**
 * Created by grinfeldra
 *
 * EU1916840;ГКДАЛ08214;27.09.2018;106;24607004890554;0;
 * последний параметр ответа сервера - кол-во ТЕ, не используется.
 */
@Entity
class EUZRoute(@PrimaryKey(autoGenerate = true)
               val id: Int? = null,
               var te: String, var codeGoods: String,
               var date: String, var countBox: Int,
               var barcode: String) : AbstractData() {


    override fun getLeftText(): String {
        return codeGoods
    }

    override fun getRightText(): String {
        return "$date | $countBox"
    }


    override fun equals(other: Any?): Boolean {
        if (other !is EUZRoute) {
            return false
        }
        return other.date == date && other.codeGoods == codeGoods &&
                other.te == te && other.barcode == barcode
    }

    companion object {
        fun parse(answer: String): ArrayList<EUZRoute> {
            val result = ArrayList<EUZRoute>()
            for (data in answer.split("|")) {
                val split = data.split(";")
                val countBox = Utils.toInt(split[3])
                if (countBox > 0) {
                    result.add(EUZRoute(te = split[0], codeGoods = split[1],
                            date = split[2], countBox = countBox,
                            barcode = split[4]))
                }
            }
            return result
        }
    }

}