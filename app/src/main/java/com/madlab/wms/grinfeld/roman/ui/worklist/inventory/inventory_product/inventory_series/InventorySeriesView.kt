package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_series

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView

/**
 * Created by grinfeldra
 */
interface InventorySeriesView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onInitFieldMeasure(countText: String, countDefective: String)
}