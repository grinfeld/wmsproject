package com.madlab.wms.grinfeld.roman.ui.worklist;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.CheckingActionFragment;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.InventoryCell;
import com.madlab.wms.grinfeld.roman.entities.Work;
import com.madlab.wms.grinfeld.roman.entities.Zone;
import com.madlab.wms.grinfeld.roman.ui.MainActivity;
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.InventoryListFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective.PlacementDefectiveFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode.RoutesFragment;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRa
 */
public class WorkListFragment extends MvpAppCompatFragment implements AdapterView.OnItemClickListener, WorkListView {


    private static final String WORK_LIST = "workList";
    private Context context;
    private ProgressOwner progressOwner;

    @InjectPresenter
    WorkListPresenter presenter;

    @ProvidePresenter
    WorkListPresenter providePresenter() {
        return new WorkListPresenter();
    }

    public static Fragment newInstance(String works) {
        WorkListFragment fragment = new WorkListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(WORK_LIST, works);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ProgressOwner) {
            progressOwner = ((ProgressOwner) context);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            ((BaseFragmentActivity) getActivity()).setActionBarTitle(getString(R.string.select_work_mode));
        }
        return inflater.inflate(R.layout.work_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ListView listView = view.findViewById(R.id.list_view);
        if (getArguments() != null) {
            String workList = getArguments().getString(WORK_LIST);
            if (workList != null) {
                ArrayList<Work> list = Work.getList(workList);
                ArrayAdapter<Work> adapter = new ArrayAdapter<>(context, R.layout.list_item_universal, list);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(this);
            }
        }
    }

    @Override
    public void onDetach() {
        progressOwner = null;
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Work work = (Work) parent.getItemAtPosition(position);
        presenter.startCommand(work.getPosition());
    }

    @Override
    public void openWorkFragment(Fragment fragment) {
        if (getActivity() != null && fragment != null)
            ((MainActivity) getActivity()).changeFragment(fragment);
    }

    @Override
    public void openPlacementDefectiveDialog(String timestamp) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle(R.string.select_type_placement_defective);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add(getString(R.string.placement_into_defective));
        arrayAdapter.add(getString(R.string.placement_from_defective));
        builderSingle.setAdapter(arrayAdapter, (dialog, which) ->
                openWorkFragment(new PlacementDefectiveFragment().newInstance(which, timestamp))
        );
        builderSingle.show();
    }

    @Override
    public void openZoneList(List<Zone> zones) {
        float a = 0.3f, b = 1.0f;
        MyAdapter adapter = new MyAdapter<>(context, zones);
        View rootView = getLayoutInflater().inflate(R.layout.dialog_listview, null);
        ListView listView = rootView.findViewById(R.id.list_view);
        adapter.setWeight(a, b);
        listView.setAdapter(adapter);
        Utils.setParamFromHeader(rootView, a, b, getString(R.string.code), getString(R.string.name));
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(getString(R.string.select_zone))
                .setView(rootView)
                .create();
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Zone zone = (Zone) parent.getItemAtPosition(position);
            presenter.loadInventoryListEUZFromZone(zone.getCode());
            alertDialog.dismiss();
        });
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void onCompleteLoadInventoryEUZ(List<InventoryCell> listInventoryCell) {
        if (listInventoryCell.size() == 0) {
            presenter.loadZoneInventory();
        } else {
            AlertDialog alertDialog = new
                    AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.not_save_inventory))
                    .setPositiveButton(getString(R.string.ok), (dialog, which)
                            -> openWorkFragment(InventoryListFragment.newInstance(new ArrayList<>(), Command.TYPE_EUZ)))
                    .setNegativeButton(getString(R.string.no), (dialog, which)
                            -> presenter.loadZoneInventory())
                    .create();
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }

    }

    @Override
    public void openCheckDialog() {
        ArrayAdapter adapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_item,
                getResources().getStringArray(R.array.type_check));
        View rootView = getLayoutInflater().inflate(R.layout.dialog_listview, null);
        ListView listView = rootView.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(getString(R.string.select_check))
                .setView(rootView)
                .create();
        listView.setOnItemClickListener((parent, view, position, id) -> {
            switch (position) {
                case 0:
                    openWorkFragment(new CheckingActionFragment());
                    break;
                case 1:
                    openWorkFragment(new RoutesFragment());
                    break;
            }
            alertDialog.dismiss();
        });
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }


    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }
}
