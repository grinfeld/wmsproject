package com.madlab.wms.grinfeld.roman.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBarcode;
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBoxesRoute;
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.RouteBoxes;
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.EUZRoute;
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.Route;
import com.madlab.wms.grinfeld.roman.entities.converter.DataConverter;
import com.madlab.wms.grinfeld.roman.entities.selection.Order;
import com.madlab.wms.grinfeld.roman.entities.selection.OrderItem;
import com.madlab.wms.grinfeld.roman.entities.selection.MyPrinter;
import com.madlab.wms.grinfeld.roman.entities.selection.PalletPrinted;
import com.madlab.wms.grinfeld.roman.entities.selection.SelectionBarcode;
import com.madlab.wms.grinfeld.roman.entities.selection.Weight;

/**
 * Created by grinfeldra
 */
@Database(entities = {Order.class, OrderItem.class, Weight.class,
        SelectionBarcode.class, MyPrinter.class, PalletPrinted.class,
        EUZRoute.class, Route.class, RouteBoxes.class,
        EuzBoxesRoute.class, EuzBarcode.class}, version = 5, exportSchema = false)
@TypeConverters({DataConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract OrderSelectionDao orderSelectionDao();
    public abstract CheckingDao checkingDao();
    public abstract CheckingBoxesDao checkingBoxesDao();
}
