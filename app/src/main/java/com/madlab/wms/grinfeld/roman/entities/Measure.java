package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by GrinfeldRa
 */
public class Measure implements Parcelable {

    private String name;
    private double koef;
    private boolean selected;

    private Measure(String name, double koef, boolean selected) {
        this.name = name;
        this.koef = koef;
        this.selected = selected;
    }

    private Measure(Parcel in) {
        name = in.readString();
        koef = in.readDouble();
        selected = in.readByte() != 0;
    }

    @NotNull
    @Override
    public String toString() {
        return name;
    }

    public static final Creator<Measure> CREATOR = new Creator<Measure>() {
        @Override
        public Measure createFromParcel(Parcel in) {
            return new Measure(in);
        }

        @Override
        public Measure[] newArray(int size) {
            return new Measure[size];
        }
    };

    public String getName() {
        return name;
    }

    public double getKoef() {
        return koef;
    }

    boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public static Measure getSelected(ArrayList<Measure> list) {
        for (Measure measure : list) {
            if (measure.isSelected()) {
                return measure;
            }
        }
        return null;
    }


    public static ArrayList<Measure> getMeasureList(String unit, String ei1, double koefEi1) {
        ArrayList<Measure> measureList = new ArrayList<>();
        double koef = 1;
        if (!TextUtils.isEmpty(unit)) {
            measureList.add(new Measure(unit, koef, true));
        }
        if (!TextUtils.isEmpty(ei1)) {
            measureList.add(new Measure(ei1, koefEi1, false));
        }
        return measureList;
    }

    public static LinkedHashMap<String, Double> getMapMeasure(ProductParam productParam) {
        LinkedHashMap<String, Double> map = new LinkedHashMap<>();
        String unit = productParam.getUnit();
        String ei1 = productParam.getEi1();
        String ei2 = productParam.getEi2();
        String ei3 = productParam.getEi3();
        String ei4 = productParam.getEi4();
        double koef = productParam.getKoef();
        double koefEi1 = productParam.getKoefEi1();
        double koefEi2 = productParam.getKoefEi2();
        double koefEi3 = productParam.getKoefEi3();
        double koefEi4 = productParam.getKoefEi4();
        if (!TextUtils.isEmpty(unit)) {
            map.put(unit, koef);
        }
        if (!TextUtils.isEmpty(ei1)) {
            map.put(ei1, koefEi1);
        }
        if (!TextUtils.isEmpty(ei2)) {
            map.put(ei2, koefEi2);
        }
        if (!TextUtils.isEmpty(ei3)) {
            map.put(ei3, koefEi3);
        }
        if (!TextUtils.isEmpty(ei4)) {
            map.put(ei4, koefEi4);
        }
        return map;
    }

    public static LinkedHashMap<String, Double> getMapMeasure(InventoryCellProduct product) {
        LinkedHashMap<String, Double> map = new LinkedHashMap<>();
        String unit = product.getMeasureInitName();
        String ei1 = product.getMeasureMoreName();
        double koef = 1;
        double koefEi1 = product.getMeasureQuant();
        if (!TextUtils.isEmpty(unit)) {
            map.put(unit, koef);
        }
        if (!TextUtils.isEmpty(ei1)) {
            map.put(ei1, koefEi1);
        }
        return map;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(koef);
        dest.writeByte((byte) (isSelected() ? 1 : 0));
    }
}
