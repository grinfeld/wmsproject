package com.madlab.wms.grinfeld.roman.entities.parser;

import com.madlab.wms.grinfeld.roman.entities.Product;

import java.util.ArrayList;

public class ProductParser {

    public static ArrayList<Product> getList(String dataFromServer) {
        ArrayList<Product> list = new ArrayList<>();
        String[] temp = dataFromServer.split("[|]");
        for (String s : temp){
            String[] split = s.split(";");
            String code = split[0];
            String name = split[1];
            String box = split[2];
            String thing = split[3];
            String box2 = split[4];
            String thing2 = split[5];
            String box3 = split[6];
            String thing3 = split[7];
            String unit = split[8];
            list.add(new Product(code, name, box, thing, box2, thing2, box3, thing3, unit));
        }
        return list;
    }

}
