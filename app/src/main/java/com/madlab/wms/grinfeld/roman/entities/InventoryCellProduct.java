package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.AbstractData;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.App;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by GrinfeldRA
 */
public class InventoryCellProduct extends AbstractData implements Parcelable, Cloneable {

    private int position;
    private String adressCell;
    private String numberCell;
    private String bailorName;
    private String codeGoods;
    private String goodsName;
    private double count;
    private double countFact;
    private String productionDate;
    private String expirationDate;
    private double countDefective;
    private boolean isEdited;
    private String codeSeries;
    private String party;
    private String measureInitName;
    private double measureQuant;
    private String measureMoreName;
    private int shelfLifeCount;
    private int shelfLifeType;
    public boolean textForNewSeries = false;
    private int inventoryType;
    private ArrayList<Measure> measureList;


    public InventoryCellProduct() {
    }

    public InventoryCellProduct(int position, String adressCell, String numberCell, String bailorName,
                                String codeGoods, String goodsName, double count, String productionDate,
                                String expirationDate, String measureInitName, double measureQuant,
                                String measureMoreName, double countFact, String codeSeries, String party,
                                int shelfLifeCount, boolean isEdited, int shelfLifeType, int inventoryType, ArrayList<Measure> measureList) {
        this.position = position;
        this.adressCell = adressCell;
        this.numberCell = numberCell;
        this.bailorName = bailorName;
        this.codeGoods = codeGoods;
        this.goodsName = goodsName;
        this.count = count;
        this.productionDate = productionDate;
        this.expirationDate = expirationDate;
        this.countFact = countFact;
        this.codeSeries = codeSeries;
        this.party = party;
        this.measureInitName = measureInitName;
        this.measureQuant = measureQuant;
        this.measureMoreName = measureMoreName;
        this.shelfLifeCount = shelfLifeCount;
        this.isEdited = isEdited;
        this.shelfLifeType = shelfLifeType;
        this.inventoryType = inventoryType;
        this.measureList = measureList;
    }

    protected InventoryCellProduct(Parcel in) {
        position = in.readInt();
        adressCell = in.readString();
        numberCell = in.readString();
        bailorName = in.readString();
        codeGoods = in.readString();
        goodsName = in.readString();
        count = in.readDouble();
        productionDate = in.readString();
        expirationDate = in.readString();
        countFact = in.readDouble();
        countDefective = in.readDouble();
        codeSeries = in.readString();
        party = in.readString();
        measureInitName = in.readString();
        measureQuant = in.readDouble();
        measureMoreName = in.readString();
        shelfLifeCount = in.readInt();
        isEdited = in.readByte() != 0;
        shelfLifeType = in.readInt();
        inventoryType = in.readInt();
        measureList = in.readArrayList(Measure.class.getClassLoader());
    }

    public static final Creator<InventoryCellProduct> CREATOR = new Creator<InventoryCellProduct>() {
        @Override
        public InventoryCellProduct createFromParcel(Parcel in) {
            return new InventoryCellProduct(in);
        }

        @Override
        public InventoryCellProduct[] newArray(int size) {
            return new InventoryCellProduct[size];
        }
    };

    public int getInventoryType() {
        return inventoryType;
    }

    public ArrayList<Measure> getMeasureList() {
        return measureList;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setAdressCell(String adressCell) {
        this.adressCell = adressCell;
    }

    public void setNumberCell(String numberCell) {
        this.numberCell = numberCell;
    }

    public void setBailorName(String bailorName) {
        this.bailorName = bailorName;
    }

    public void setCodeGoods(String codeGoods) {
        this.codeGoods = codeGoods;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public void setCodeSeries(String codeSeries) {
        this.codeSeries = codeSeries;
    }

    public void setParty(String party) {
        this.party = party;
    }

    public void setMeasureInitName(String measureInitName) {
        this.measureInitName = measureInitName;
    }

    public void setMeasureQuant(double measureQuant) {
        this.measureQuant = measureQuant;
    }

    public void setMeasureMoreName(String measureMoreName) {
        this.measureMoreName = measureMoreName;
    }

    public void setShelfLifeCount(int shelfLifeCount) {
        this.shelfLifeCount = shelfLifeCount;
    }

    public void setMeasureList(ArrayList<Measure> measureList) {
        this.measureList = measureList;
    }

    public static ArrayList<InventoryCellProduct> getList(String response, int typeInventory) {
        ArrayList<InventoryCellProduct> result = new ArrayList<>();
        try {
            String[] split = response.split("[|]");
            for (String data : split) {
                String[] temp = data.split(";");
                int position = Utils.toInt(temp[0]);
                String adressCell = temp[3];
                String numberCell = temp[4];
                String bailorName = temp[5];
                String codeGoods = temp[6];
                String goodsName = temp[7];
                double count = Utils.toDouble(temp[8]);
                double countFact = Utils.toDouble(temp[8]);
                String codeSeries = temp[9];
                String productionDate = temp[11];
                String expirationDate = temp[13];
                String party = temp[15];
                String measureInitName = temp[16];
                double measureQuant = Utils.toDouble(temp[17]);
                String measureMoreName = temp[18];
                int shelfCount = Utils.toInt(temp[26].replaceAll("\\s", "").trim());
                int shelfType = Utils.toInt(temp[27]);
                result.add(new InventoryCellProduct(position, adressCell, numberCell, bailorName, codeGoods,
                        goodsName, count, productionDate, expirationDate, measureInitName, measureQuant,
                        measureMoreName, countFact, codeSeries, party, shelfCount, false, shelfType,
                        typeInventory, Measure.getMeasureList(measureInitName, measureMoreName, measureQuant)));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


    public int getShelfLifeCount() {
        return shelfLifeCount;
    }

    public String getMeasureInitName() {
        return measureInitName;
    }

    public double getMeasureQuant() {
        return measureQuant;
    }

    public String getMeasureMoreName() {
        return measureMoreName;
    }

    public String getParty() {
        return party;
    }

    public String getCodeSeries() {
        return codeSeries;
    }

    public double getCountFact() {
        return countFact;
    }

    public void setCountFact(double countFact) {
        this.countFact = countFact;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    public double getCountDefective() {
        return countDefective;
    }

    public void setCountDefective(double countDefective) {
        this.countDefective = countDefective;
    }

    public int getPosition() {
        return position;
    }

    public String getAdressCell() {
        return adressCell;
    }

    public String getNumberCell() {
        return numberCell;
    }

    public String getBailorName() {
        return bailorName;
    }

    public String getCodeGoods() {
        return codeGoods;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public String getProductionDate() {
        return productionDate;
    }


    public String getExpirationDate() {
        return expirationDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getShelfLifeType() {
        return shelfLifeType;
    }

    public void setShelfLifeType(int shelfLifeType) {
        this.shelfLifeType = shelfLifeType;
    }

    public void changeSelected() {
        for (Measure measure : measureList) {
            if (measure.isSelected()) {
                measure.setSelected(false);
            } else {
                measure.setSelected(true);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(position);
        dest.writeString(adressCell);
        dest.writeString(numberCell);
        dest.writeString(bailorName);
        dest.writeString(codeGoods);
        dest.writeString(goodsName);
        dest.writeDouble(count);
        dest.writeDouble(countDefective);
        dest.writeString(productionDate);
        dest.writeString(expirationDate);
        dest.writeDouble(countFact);
        dest.writeString(codeSeries);
        dest.writeString(party);
        dest.writeString(measureInitName);
        dest.writeDouble(measureQuant);
        dest.writeString(measureMoreName);
        dest.writeInt(shelfLifeCount);
        dest.writeByte((byte) (isEdited ? 1 : 0));
        dest.writeInt(shelfLifeType);
        dest.writeInt(inventoryType);
        dest.writeList(measureList);
    }

    @Override
    public String getLeftText() {
        return textForNewSeries ? App.getAppContext().getString(R.string.date_format,
                getProductionDate(), getExpirationDate()) : inventoryType == Command.TYPE_CELL ? getCodeGoods() : getAdressCell();
    }

    @Override
    public String getRightText() {
        NumberFormat numberFormat = NumberFormat.getInstance();
        String countTxt;
        if (measureMoreName != null && !measureMoreName.isEmpty()) {
            double rest = getCountFact() % measureQuant;
            double count = (getCountFact() - rest) / measureQuant;
            double restDefective = getCountDefective() % measureQuant;
            double countDefective = (getCountDefective() - restDefective) / measureQuant;
            countTxt = App.getAppContext().getString(R.string.count_format,
                    numberFormat.format(count), measureMoreName,
                    numberFormat.format(rest), measureInitName,
                    numberFormat.format(countDefective), measureMoreName,
                    numberFormat.format(restDefective), measureInitName);
        } else {
            countTxt = App.getAppContext().getString(R.string.count_format2,
                    numberFormat.format(getCountFact()), measureInitName,
                    numberFormat.format(getCountDefective()), measureInitName);
        }
        return textForNewSeries ? countTxt : inventoryType == Command.TYPE_CELL ? getGoodsName() : countTxt;
    }


    public InventoryCellProduct cloneFromNewSeries() throws CloneNotSupportedException {
        InventoryCellProduct newSeries = (InventoryCellProduct) super.clone();
        newSeries.setPosition(0);
        newSeries.setCount(0);
        newSeries.setCountFact(0);
        newSeries.setCountDefective(0);
        newSeries.setExpirationDate(null);
        newSeries.setProductionDate(null);
        return newSeries;
    }

    @Override
    public InventoryCellProduct clone() throws CloneNotSupportedException {
        return (InventoryCellProduct) super.clone();
    }
}
