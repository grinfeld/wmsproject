package com.madlab.wms.grinfeld.roman.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TableRow;
import android.widget.TextView;

import com.madlab.wms.grinfeld.roman.common.App;
import com.madlab.wms.grinfeld.roman.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Random;

/**
 * Created by GrinfeldRa
 */
public class Utils {

    public static final String CHARSET_UTF16LE = "UTF-16LE";
    public static final String CHARSET_ASCII = "US-ASCII";


    public static void isOnline() throws Exception {
        ConnectivityManager connectivityManager = (ConnectivityManager) App.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (connectivityManager != null) {
            netInfo = connectivityManager.getActiveNetworkInfo();
        }
        if (netInfo == null) {
            throw new Exception(App.getAppContext().getString(R.string.no_Internet_connection));
        }
    }

    public static void closeKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static byte[] bitConverterGetBytes(int value) {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder());
        buffer.putInt(value);
        return buffer.array();
    }

    public static int bitConverterToInt32(byte[] bytes) {
        return ((0xff & bytes[0]) | (0xff & bytes[1]) << 8 | (0xff & bytes[2]) << 16 | (0xff & bytes[3]) << 24);
    }

    public static String getVersionName() {
        PackageManager pMan = App.getAppContext().getPackageManager();
        PackageInfo pInfo;
        try {
            pInfo = pMan.getPackageInfo(App.getAppContext().getPackageName(), 0);
        } catch (Exception e) {
            return "Unknown";
        }
        return pInfo.versionName;
    }

    public static byte[] getMacAddress() {
        WifiManager wifiManager = (WifiManager) App.getAppContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String bssid = wifiInfo.getMacAddress();
        return bssid.getBytes(Charset.forName(CHARSET_ASCII));
    }

    public static String GenerationID() {
        StringBuilder _IDString = new StringBuilder();
        byte[] _ID = new byte[10];
        Random yr = new Random();
        yr.nextBytes(_ID);
        for (byte a_ID : _ID) {
            _IDString.append(a_ID);
        }
        return _IDString.toString();
    }


    public static void setParamFromHeader(View rootView, float weightLeft, float weightRight,
                                          String textLeft, String textRight) {
        setParamHeader(rootView, weightLeft, weightRight, textLeft, textRight);
    }

    public static void setParamFromHeader(View rootView, String textLeft, String textRight) {
        setParamHeader(rootView, 0.5f, 0.5f, textLeft, textRight);
    }

    private static void setParamHeader(View rootView, float weightLeft, float weightRight,
                                       String textLeft, String textRight) {
        ViewGroup header = rootView.findViewById(R.id.header);
        TextView tvLeft = header.findViewById(R.id.txt_left);
        TextView tvRight = header.findViewById(R.id.txt_right);
        TableRow.LayoutParams params1 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, weightLeft);
        TableRow.LayoutParams params2 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, weightRight);
        tvLeft.setLayoutParams(params1);
        tvRight.setLayoutParams(params2);
        tvLeft.setText(textLeft);
        tvRight.setText(textRight);
        header.setVisibility(View.VISIBLE);
    }


    public static double toDouble(String currentItem) {
        return currentItem.isEmpty() ? 0 : Double.parseDouble(currentItem.replaceAll(",", ".").replaceAll("\\s",""));
    }

    public static float toFloat(String currentItem) {
        return currentItem.isEmpty() ? 0 : Float.parseFloat(currentItem.replaceAll(",", ".").replaceAll("\\s",""));
    }

    public static int toInt(String currentItem) {
        return currentItem.isEmpty() ? 0 : Integer.parseInt(currentItem.replaceAll(",", ".").replaceAll("\\s",""));
    }
}
