package com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective.enter_count.PlacementEnterCountFragment
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.PREFIX_CELL
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.placement_defective_fragment.*
import kotlinx.android.synthetic.main.placement_defective_fragment.list_place_view
import kotlinx.android.synthetic.main.placement_defective_fragment.list_view
import kotlinx.android.synthetic.main.placement_defective_fragment.txt_info
import kotlinx.android.synthetic.main.placement_defective_fragment.txt_name
import kotlinx.android.synthetic.main.placement_defective_fragment.txt_rest
import java.text.NumberFormat


/**
 * Created by grinfeldra
 */

const val KEY_TYPE_PLACEMENT_DEFECTIVE = "type_placement_defective"
const val KEY_TIMESTAMP = "key_timestamp"
const val TYPE_INTO_DEFECTIVE = 0
const val TYPE_FROM_DEFECTIVE = 1


class PlacementDefectiveFragment : MvpAppCompatFragment(), PlacementDefectiveView, KeyEventListener, IOnScannerEvent, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    @InjectPresenter
    lateinit var presenter: PlacementDefectivePresenter

    @ProvidePresenter
    fun providePresenter() = PlacementDefectivePresenter()

    private var scanner: AbstractScanner? = null
    private lateinit var progressOwner: ProgressOwner
    private var typeDefective: Int = 0
    private lateinit var txtCell: String
    private lateinit var cell: String
    private lateinit var timestamp: String

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    fun newInstance(typePlacementDefective: Int, timestamp: String) = PlacementDefectiveFragment().apply {
        arguments = Bundle().apply {
            putInt(KEY_TYPE_PLACEMENT_DEFECTIVE, typePlacementDefective)
            putString(KEY_TIMESTAMP, timestamp)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var title = ""
        timestamp = arguments?.getString(KEY_TIMESTAMP)!!
        typeDefective = arguments?.getInt(KEY_TYPE_PLACEMENT_DEFECTIVE)!!
        when (typeDefective) {
            TYPE_FROM_DEFECTIVE -> title = getString(R.string.placement_from_defective)
            TYPE_INTO_DEFECTIVE -> title = getString(R.string.placement_into_defective)
        }
        (activity as BaseFragmentActivity).setActionBarTitle(title)
        return inflater.inflate(R.layout.placement_defective_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (typeDefective) {
            TYPE_FROM_DEFECTIVE -> txt_info.text = getString(R.string.placement_defective_scan_from_cell)
            TYPE_INTO_DEFECTIVE -> txt_info.text = getString(R.string.placement_defective_scan_into_cell)
        }
    }

    override fun onCompleteGetInfoEuz(result: List<PlacementCell>) {
        txt_info.visibility = View.GONE
        list_place_view.visibility = View.VISIBLE
        val adapter = MyAdapter(context, result)
        list_view.adapter = adapter
        list_view.onItemClickListener = this
        list_view.onItemSelectedListener = this
        val weightLeft = 0.6f
        val weightRight = 1f
        adapter.setWeight(weightLeft, weightRight)
        Utils.setParamFromHeader(view, weightLeft, weightRight, getString(R.string.code), getString(R.string.name))
    }

    override fun onCompleteBlockCell(cell: String, cellTxt: String) {
        txt_info.text = getString(R.string.placement_defective_scan_goods, cellTxt)
        this.cell = cell
        this.txtCell = cellTxt
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val placementCell = parent?.getItemAtPosition(position) as PlacementCell
        txt_name.text = getString(R.string.footer_1, placementCell.code, placementCell.name)
        txt_rest.text = getString(R.string.footer_2,
                NumberFormat.getInstance().format(placementCell.rest).plus(" ").plus(placementCell.baseEi))
        txt_date.text = getString(R.string.footer_3, placementCell.dateEnd)
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val placementCell = parent?.getItemAtPosition(position) as PlacementCell
        (activity as MainActivity).changeFragment(PlacementEnterCountFragment().newInstance(placementCell, typeDefective))
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (txt_info.isVisible){
            if (goodsCard != null && goodsCard.barcode != null && goodsCard.barcode.substring(0, 2) == PREFIX_CELL) {
                presenter.startBlockCell(goodsCard.barcode.substring(2))
            } else {
                if (::cell.isInitialized){
                    goodsCard?.barcode?.let { presenter.getInfoEuz(it, cell, txtCell, timestamp) }
                }else{
                    showError(getString(R.string.error_placement_defective_not_enter_cell))
                }
            }
        }
    }


    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if ((keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE)
                && event?.action == KeyEvent.ACTION_UP) {
            val alertDialog = context?.let {
                AlertDialog.Builder(it)
                        .setMessage(getString(R.string.placement_close))
                        .setPositiveButton(getString(R.string.ok)) { _, _ ->
                            fragmentManager?.popBackStack()
                        }
                        .setNegativeButton(getString(R.string.cancel), null)
                        .create()
            }
            if (!alertDialog?.isShowing!!) {
                alertDialog.show()
            }
            return true
        }
        return false
    }


    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }
}