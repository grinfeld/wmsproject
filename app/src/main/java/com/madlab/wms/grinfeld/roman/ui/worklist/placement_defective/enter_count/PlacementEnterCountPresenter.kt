package com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective.enter_count

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.Measure
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell
import io.reactivex.android.schedulers.AndroidSchedulers
import java.text.NumberFormat

/**
 * Created by grinfeldra
 */
@InjectViewState
class PlacementEnterCountPresenter : BasePresenter<PlacementEnterCountView>() {

    fun startBlockCell(cell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_BLOCK_PLACEMENT_CELL(cell))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val cellTxt = it.split(";")[1]
                        viewState.onBlockCellComplete(cell, cellTxt)
                    }
                })
    }

    fun unblockCell(fromCell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_UNBLOCK_PLACEMENT_CELL(fromCell))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.onUnblockCellComplete()
                    }
                })
    }

    fun reservePlacement(p: PlacementCell, type: Int) {
        val numberFormat = NumberFormat.getInstance()
        mCompositeDisposable.add(WMSClient().send(Command.COM_RESERVE_PLACEMENT_DEFECTIVE(p.fromCell, p.toCell,
                p.code, numberFormat.format(p.totalCount), p.timestamp, p.owner, type))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.reservePlacementComplete(p)
                    }
                })
    }

    fun onInitFieldsMeasure(measure: Measure, placementCell: PlacementCell) {
        var rest = 0.0
        val accepted: Double
        val totalCount = placementCell.totalCount
        val quantum = measure.koef
        if (totalCount != 0.0) {
            if (quantum != placementCell.koef) {
                rest = totalCount % quantum
            }
            accepted = (totalCount - rest) / quantum + placementCell.accepted
        } else {
            accepted = placementCell.accepted
        }
        val required = placementCell.rest - (accepted * quantum + rest)
        viewState.initFieldsMeasure(prepareMessage(required, placementCell), prepareMessage(accepted * quantum, placementCell))
    }

    private fun prepareMessage(v: Double, placementCell: PlacementCell): String {
        var result = ""
        val numberFormat = NumberFormat.getInstance()
        if (placementCell.koef != 0.0) {
            val restEi1Required = v % placementCell.koef
            val strRestEi1 = if (restEi1Required == 0.0) "" else numberFormat.format(restEi1Required) + " " + placementCell.baseEi
            result = numberFormat.format((v - restEi1Required) / placementCell.koef) + placementCell.ei2 + " " + strRestEi1 + " ("
        }
        val endMessage = if (result.isEmpty()) "" else ")"
        result += numberFormat.format(if (v < 0) 0 else v) + placementCell.baseEi + endMessage
        return result
    }
}