package com.madlab.wms.grinfeld.roman.ui.worklist.placement.enter_count

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener

import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.Measure
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.PREFIX_CELL
import com.madlab.wms.grinfeld.roman.utils.Utils
import com.madlab.wms.grinfeld.roman.utils.toEditable
import kotlinx.android.synthetic.main.placement_fragment_enter_count.*
import kotlinx.android.synthetic.main.placement_fragment_enter_count.et_count
import kotlinx.android.synthetic.main.placement_fragment_enter_count.spinner_measure
import java.text.NumberFormat
import java.util.*

/**
 * Created by grinfeldra
 */
private const val PLACEMENT_CELL_KEY = "key1"
private const val CELL_EMPTY_LIST = "key2"

class PlacementEnterCountFragment : MvpAppCompatFragment(), KeyEventListener, IOnScannerEvent, PlacementEnterCountView, TextWatcher, AdapterView.OnItemSelectedListener {

    private lateinit var placementCell: PlacementCell
    private var progressOwner: ProgressOwner? = null
    private var scanner: AbstractScanner? = null
    @InjectPresenter
    lateinit var presenter: PlacementEnterCountPresenter
    lateinit var measure: ArrayList<Measure>

    @ProvidePresenter
    fun providePresenter() = PlacementEnterCountPresenter()

    fun newInstance(placementCell: PlacementCell, cellEmptyList: ArrayList<String>) =
            PlacementEnterCountFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(PLACEMENT_CELL_KEY, placementCell)
                    putStringArrayList(CELL_EMPTY_LIST, cellEmptyList)
                }
            }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.placement_title))
        return inflater.inflate(R.layout.placement_fragment_enter_count, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = arguments?.getStringArrayList(CELL_EMPTY_LIST)
        placementCell = arguments?.getParcelable<PlacementCell>(PLACEMENT_CELL_KEY)!!
        et_to_cell.text = placementCell.code.toEditable()
        et_name.text = placementCell.name.toEditable()
        val adapter = ArrayAdapter<String>(context!!, R.layout.list_item_universal, list)
        list_view.adapter = adapter
        et_count.addTextChangedListener(this)
        et_count.setOnKeyListener(this)
        measure = Measure.getMeasureList(placementCell.baseEi, placementCell.ei2, placementCell.koef)
        val dataAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, measure)
        spinner_measure.adapter = dataAdapter
        spinner_measure.onItemSelectedListener = this


    }


    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_DEL -> {
                if (event?.action == KeyEvent.ACTION_DOWN) {
                    if ((v?.id == R.id.et_count && et_count.text.isEmpty())) {
                        val alertDialog = context?.let {
                            AlertDialog.Builder(it)
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .setTitle(getString(R.string.confirmation))
                                    .setMessage(getString(R.string.delete_goods))
                                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                                        placementCell.accepted = 0.0
                                        placementCell.totalCount = 0.0
                                        val id = spinner_measure.selectedItemPosition
                                        val measure = measure[id]
                                        presenter.onInitFieldsMeasure(measure, placementCell)
                                    }
                                    .setNegativeButton(getString(R.string.no), null)
                                    .create()
                        }
                        if (!alertDialog?.isShowing!!) {
                            alertDialog.show()
                        }
                        return true
                    } else {
                        return false
                    }
                }
                return false
            }
            KeyEvent.KEYCODE_BACK -> {
                if (event?.action == KeyEvent.ACTION_DOWN) {
                    val alertDialog = context?.let {
                        AlertDialog.Builder(it)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle(getString(R.string.confirmation))
                                .setMessage(getString(R.string.exit_not_save))
                                .setPositiveButton(getString(R.string.ok)) { _, _ ->
                                    if (activity != null) {
                                        presenter.unblockCell(placementCell.fromCell)
                                    }
                                }
                                .setNegativeButton(getString(R.string.cancel), null)
                                .create()
                    }
                    if (!alertDialog?.isShowing!!) {
                        alertDialog.show()
                    }
                    return true
                }
                return false
            }
            KeyEvent.KEYCODE_ENTER -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    if (!TextUtils.isEmpty(et_count.text)) {
                        val id = spinner_measure.selectedItemPosition
                        val measure = measure[id]
                        placementCell.totalCount =
                                placementCell.totalCount + (Utils.toDouble(et_count.text.toString()) * measure.koef)
                        et_count.setText("")
                    } else {
                        if (placementCell.totalCount > 0) {
                            if (placementCell.toCell.isNotEmpty()) {
                                (context as BaseFragmentActivity).removeKeyEventHandler(this)
                                presenter.reservePlacement(placementCell)
                            } else {
                                showError(getString(R.string.error_enter_cell))
                            }
                        } else {
                            showError(getString(R.string.error_enter_count_placement))
                        }
                    }
                }
                return true
            }
            KeyEvent.KEYCODE_E -> {
                if (event?.action == KeyEvent.ACTION_DOWN) {
                    val id = spinner_measure.selectedItemPosition
                    if (id == measure.size.minus(1)) {
                        spinner_measure.setSelection(0)
                    } else {
                        spinner_measure.setSelection(id + 1)
                    }
                    return true
                }
                return false
            }
            else -> return false
        }
    }

    override fun initFieldsMeasure(rest: String, placement: String) {
        txt_required.text = getString(R.string.remained_placement, rest)
        txt_take.text = getString(R.string.placement, placement)
    }

    override fun reservePlacementComplete(p: PlacementCell) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.placement_cell_complete, NumberFormat.getInstance().format(p.totalCount),
                            placementCell.baseEi, placementCell.fromCellTxt, placementCell.toCellTxt))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        activity?.onBackPressed()
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onUnblockCellComplete() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.cell_unblock))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        activity?.onBackPressed()
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val measure = parent?.getItemAtPosition(position) as Measure
        presenter.onInitFieldsMeasure(measure, placementCell)
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val id = spinner_measure.selectedItemPosition
        val measure = measure[id]
        placementCell.accepted = Utils.toDouble(s.toString())
        presenter.onInitFieldsMeasure(measure, placementCell)
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (goodsCard?.barcode != null && goodsCard.barcode.substring(0, 2) == PREFIX_CELL) {
            val cell = goodsCard.barcode.substring(2)
            presenter.startBlockCell(cell)
        } else {
            showError(getString(R.string.error_barcode_cell))
        }
    }

    override fun onBlockCellComplete(cell: String, cellTxt: String) {
        et_cell.text = cellTxt.toEditable()
        placementCell.toCell = cell
        placementCell.toCellTxt = cellTxt
    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun showError(error: String?) {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).addKeyEventHandler(this)
        }
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }
}