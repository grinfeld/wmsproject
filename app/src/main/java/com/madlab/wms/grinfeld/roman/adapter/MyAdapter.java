package com.madlab.wms.grinfeld.roman.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.madlab.wms.grinfeld.roman.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by GrinfeldRA
 *
 */
public class MyAdapter<T extends AbstractData> extends BaseAdapter  {


    private Context context;
    private List<T> listItem;
    private float weightLeft = 1f;
    private float weightRight = 1f;

    public MyAdapter(Context context, List<T> listItem) {
        this.context = context;
        this.listItem = listItem;
    }

    public MyAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<T> listItem){
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    public void addData(List<T> listItem){
        this.listItem.addAll(listItem);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return listItem == null ? 0 : listItem.size();
    }

    @Override
    public T getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public List<T> getAllItem() {
        return listItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            if (inflater != null) {
                convertView = inflater.inflate(R.layout.list_item, parent, false);
                holder.list_item_left = convertView.findViewById(R.id.list_item_left);
                holder.list_item_right = convertView.findViewById(R.id.list_item_right);
                TableRow.LayoutParams paramsLeft = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, weightLeft);
                TableRow.LayoutParams paramsRight = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, weightRight);
                holder.list_item_left.setLayoutParams(paramsLeft);
                holder.list_item_right.setLayoutParams(paramsRight);
                convertView.setTag(holder);
            }
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.list_item_left.setText(getItem(position).getLeftText());
        holder.list_item_right.setText(getItem(position).getRightText());
        return convertView;
    }



    static class ViewHolder {
        TextView list_item_left, list_item_right;
    }


    public void setWeight(float weight1, float weight2) {
        this.weightLeft = weight1;
        this.weightRight = weight2;
    }

}
