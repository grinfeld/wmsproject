package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView

/**
 * Created by grinfeldra
 */
interface CheckingBoxesView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteReserve()
}