package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.ordercard;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;

import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.entities.ProductParam;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;
import com.madlab.wms.grinfeld.roman.utils.DateParser;

import java.text.NumberFormat;

import io.reactivex.android.schedulers.AndroidSchedulers;


/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class OrderCardPresenter extends BasePresenter<OrderCardView> {


    private static final String TAG = "#OrderCardPresenter";

    void onInitFieldsMeasure(ProductParam productParam, double quantum, double total_count) {
        double rest = 0;
        double accepted;
        if (total_count != 0) {
            if (quantum != productParam.getKoef()) {
                rest = total_count % quantum;
            }
            accepted = ((total_count - rest) / quantum) + productParam.getAccepted();
        } else {
            accepted = productParam.getAccepted();
        }
        double required = productParam.getRequired() - (accepted * quantum + rest);
        getViewState().onInitFieldsMeasure(prepareMessage(required, productParam), prepareMessage(accepted != 0 ? (accepted * quantum) + rest : rest, productParam));
    }

    private String prepareMessage(double v, ProductParam productParam) {
        String result = "";
        NumberFormat numberFormat = NumberFormat.getInstance();
        if (productParam.getKoefEi1() != 0) {
            double restEi1Required = v % productParam.getKoefEi1();
            String strRestEi1 = restEi1Required == 0 ? "" : numberFormat.format(restEi1Required) + " " + productParam.getUnit();
            result = numberFormat.format((v - restEi1Required) / productParam.getKoefEi1()) + productParam.getEi1() + " " + strRestEi1 + " (";
        }
        String endMessage = result.isEmpty() ? "" : ")";
        result += numberFormat.format(v) + productParam.getUnit() + endMessage;
        return result;
    }


    void onDeleteCountProductInOrder() {
        getViewState().onDeleteCountProductInOrder();
    }

    void getParamOnPallet(String numDoc, String dateDoc, String barcode) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_PARAM_ON_PALLET(numDoc, dateDoc, barcode))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteLoadParamPallet(response, barcode);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    void loadParamFromScanCode(Order order, ScannerData scannerData, int type) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_PARAM_ON_GOODS(0, order.getName(), DateParser.getDateToStringFromLongToRequest(order.getDate()), type, scannerData.getBarcode()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                            if (response.contains("ОК")) {
                                ProductParam productParam = new ProductParam(response);
                                if (type == Command.TYPE_GET) {
                                    productParam.setRequired(productParam.getRequired() - productParam.getAccepted());
                                    productParam.setAccepted(0);
                                }
                                if (productParam.getUnit().equals("кг")) {
                                    productParam.setKoefEi1(0);
                                    productParam.setEi1("");
                                }
                                productParam.setKoef(1);
                                getViewState().onCompleteLoadParamGoods(productParam, scannerData);
                            } else {
                                try {
                                    String[] error = response.split(";");
                                    getViewState().showError(error[1]);
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    getViewState().showError(e.getMessage());
                                }
                            }
                        },
                        throwable -> getViewState().showError(throwable.getMessage())));
    }


    void reserveOrder(int typeOrder, String numDoc, String dateDoc, String euz, String count, String te, String cellNumber, String prodDate, String expirationDate, String lootNumber, int type) {
        String s = Command.COM_RESERVE(typeOrder, numDoc, dateDoc, euz,
                count, te, cellNumber, prodDate, expirationDate, lootNumber, type);
        Log.d(TAG, s);
        mCompositeDisposable.add(new WMSClient()
                .send(s)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteReserveOrder();
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }
}
