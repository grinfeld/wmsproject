package com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter

import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.selection_check.SelectionCheckFragment
import kotlinx.android.synthetic.main.selection_boxes_fragment.*
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.dao.OrderSelectionDao
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.selection.*
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.selection_enter_count.SelectionEnterCountFragment
import com.madlab.wms.grinfeld.roman.utils.*
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by grinfeldra
 */
const val META_CTRL_LEFT_ON = 12288
const val KEY_TASK_NUMBER = "task_number"
const val KEY_TYPE_SELECTION = "type_selection"
const val CLASSIC_SELECTION: Byte = 0
const val TE_SELECTION: Byte = 1
const val BOXES_SELECTION: Byte = 2
const val PREFIX_CELL: String = "99"

class SelectionsMorphFragment : MvpAppCompatFragment(), KeyEventListener, IOnScannerEvent, SelectionsMorphView, IOnSwipe, IRFACItemClick {


    @InjectPresenter
    lateinit var presenter: SelectionsMorphPresenter

    @ProvidePresenter
    fun providePresenter() = SelectionsMorphPresenter()

    private lateinit var currentItem: OrderItem
    private lateinit var order: Order
    private var scanner: AbstractScanner? = null
    private lateinit var mTaskNumber: String
    private lateinit var progressOwner: ProgressOwner
    private lateinit var dao: OrderSelectionDao
    private var typeSelection: Byte? = null
    private lateinit var alertDialogScanTE: AlertDialog
    private lateinit var alertDialogScanZGS: AlertDialog

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    fun newInstance(mTaskNumber: String, typeSelection: Byte) = SelectionsMorphFragment().apply {
        arguments = Bundle().apply {
            putString(KEY_TASK_NUMBER, mTaskNumber)
            putByte(KEY_TYPE_SELECTION, typeSelection)
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

    override fun onFirstViewAttach() {
        typeSelection = arguments?.getByte(KEY_TYPE_SELECTION)
        when (typeSelection) {
            BOXES_SELECTION -> showBoxesInfo()
            TE_SELECTION -> showDialogScanTe()
        }
    }

    private fun showDialogScanTe() {
        if (!::order.isInitialized) {
            val taskNum = arguments?.getString(KEY_TASK_NUMBER)
            dao = App.getDatabase().orderSelectionDao()
            order = dao.getOrder(taskNum)
        }
        alertDialogScanTE = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.scan_te_dialog,
                            if (order.scanTe != null) order.scanTe else getString(R.string.not_selected)))
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }!!
        if (!alertDialogScanTE.isShowing) {
            alertDialogScanTE.setCancelable(false)
            alertDialogScanTE.show()
        }
    }


    private fun showDialogZGS() {
        alertDialogScanZGS = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.scan_zgs_dialog))
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }!!
        if (!alertDialogScanZGS.isShowing) {
            alertDialogScanZGS.setCancelable(false)
            alertDialogScanZGS.show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dao = App.getDatabase().orderSelectionDao()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var title: String? = null
        when (arguments?.getByte(KEY_TYPE_SELECTION)) {
            CLASSIC_SELECTION -> title = getString(R.string.selection)
            TE_SELECTION -> title = getString(R.string.selection_te)
            BOXES_SELECTION -> title = getString(R.string.selection_boxes)
        }
        (activity as BaseFragmentActivity).setActionBarTitle(title)
        return inflater.inflate(R.layout.selection_boxes_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFloating()
        view.setOnTouchListener(OnSwipeTouchListener(context, this))
        cb_missing.setOnClickListener {
            currentItem.mMissing = cb_missing.isChecked
            dao.update(currentItem)
        }
        mTaskNumber = arguments?.getString(KEY_TASK_NUMBER)!!
        mTaskNumber.let {
            dao.getLiveOrder(it).observe(viewLifecycleOwner, Observer { t ->
                if (t != null) {
                    order = t
                    et_route.text = getString(R.string.et_route_format, t.mRoute, t.currentPosition + 1, t.mRowsCount).toEditable()
                    dao.getLiveOrderItem(t.currentPosition, order.id).observe(viewLifecycleOwner, Observer { item ->
                        if (item != null) {
                            currentItem = item
                            et_client.text = (if (item.mClient.isEmpty()) getString(R.string.str_summary) else item.mClient).toEditable()
                            if (typeSelection == BOXES_SELECTION) {
                                et_box_name.text = getString(R.string.et_box_name_format, item.mBoxName, item.mBoxNumber,
                                        if (item.mIsBoxSeparate) getString(R.string.box_type_separate) else getString(R.string.box_type_precast)).toEditable()
                            } else if (typeSelection == TE_SELECTION) {
                                et_box_name.text = item.mTask.toEditable()
                            }
                            et_cell.text = item.mCellAddress.toEditable()
                            cb_podpitka.isChecked = item.mPodpitka
                            et_code.text = item.code.toEditable()
                            et_name.text = item.name.toEditable()
                            et_need.text = item.need.toEditable()
                            et_exist.text = item.exist.toEditable()
                            var start = item.mBestBeforeStart
                            var end = item.mBestBeforeEnd
                            start = start.substring(6).plus(".").plus(start.substring(4, 6)).plus(".").plus(start.substring(0, 4))
                            end = end.substring(6).plus(".").plus(end.substring(4, 6)).plus(".").plus(end.substring(0, 4))
                            et_date.text = getString(R.string.date_format, start, end).toEditable()
                            if (item.mEst.compareTo(0f) > 1) {
                                cb_missing.isEnabled = false
                            } else {
                                cb_missing.isEnabled = true
                                cb_missing.isChecked = item.mMissing
                            }
                        }
                    })
                }
            })
        }
    }

    private fun completeDialog() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(if (order.isComplete(order.id, order.mPercentMaxWeight))
                        getString(R.string.order_with_boxes_complete) else
                        getString(R.string.order_with_boxes_not_complete))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        presenter.reserveSelectionOrder(order)
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }


    private fun selectPrinter(mTENumber: String) {
        val printers = dao.printers
        val adapter = MyAdapter(context, printers)
        val rootView = layoutInflater.inflate(R.layout.dialog_listview, null)
        rootView.findViewById<TextView>(R.id.tv_message).text =
                getString(R.string.will_be_print, mTENumber)
        val listView = rootView.findViewById<ListView>(R.id.list_view)
        listView.adapter = adapter
        Utils.setParamFromHeader(rootView, getString(R.string.printer_lbl), getString(R.string.host_printer_lbl))
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setTitle(getString(R.string.select_printer))
                    .setView(rootView)
                    .create()
        }
        listView.setOnItemClickListener { parent, _, position, _ ->
            val printer = parent.getItemAtPosition(position) as MyPrinter
            presenter.sendToPrint(printer.host, order.mID, mTENumber)
            alertDialog?.dismiss()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }


    private fun selectPallet(palletPrintedList: List<PalletPrinted>) {
        val alertDialog = context?.let {
            val adapter = ArrayAdapter<PalletPrinted>(it, android.R.layout.select_dialog_item)
            adapter.addAll(palletPrintedList)
            AlertDialog.Builder(it)
                    .setTitle(getString(R.string.select_pallet))
                    .setAdapter(adapter) { _, which ->
                        val palletPrinted = adapter.getItem(which) as PalletPrinted
                        selectPrinter(palletPrinted.getmTENumber().toString())
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }

    override fun onSwipeRight() {
        onLeft()
    }

    override fun onSwipeLeft() {
        onRight()
    }

    private fun onLeft() {
        val currentPosition = order.currentPosition
        if (currentPosition != 0) {
            order.currentPosition = currentPosition.minus(1)
            if (typeSelection == BOXES_SELECTION) {
                if (currentItem.mIsWeightGoods || currentItem.mMissing || currentItem.mEst.compareTo(0) == 1) {
                    dao.update(order)
                }
            } else {
                dao.update(order)
            }
        }
    }

    private fun onRight() {
        val mRowsCount = order.mRowsCount.minus(1)
        val currentPosition = order.currentPosition
        if (mRowsCount != currentPosition) {
            order.currentPosition = currentPosition.plus(1)
            if (typeSelection == BOXES_SELECTION) {
                if (currentItem.mIsWeightGoods || currentItem.mMissing || currentItem.mEst.compareTo(0) == 1) {
                    val nextItem = dao.getOrderItem(currentPosition + 1, order.id)
                    val palletPrinted = order.palletPrintedList
                            .single { p -> p.getmTENumber() == nextItem.mTENumber }
                    if (currentItem.mTENumber != nextItem.mTENumber && !palletPrinted.isPrinted) {
                        selectPrinter(nextItem.mTENumber.toString())
                        palletPrinted.isPrinted = true
                        val indexOf = order.palletPrintedList.indexOf(palletPrinted)
                        order.palletPrintedList[indexOf] = palletPrinted
                        dao.update(order)
                    } else {
                        dao.update(order)
                    }
                }
            } else {
                dao.update(order)
            }
        }
    }

    private fun setFloating() {
        if (activity != null) {
            val items = java.util.ArrayList<RFACLabelItem<*>>()
            items.add(RFACLabelItem<Int>()
                    .setLabel(getString(R.string.close_selection))
                    .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                    .setLabelColor(Color.WHITE)
                    .setWrapper(0)
            )
            items.add(RFACLabelItem<Int>()
                    .setLabel(getString(R.string.selection_check_title))
                    .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                    .setLabelColor(Color.WHITE)
                    .setWrapper(1)
            )
            items.add(RFACLabelItem<Int>()
                    .setLabel(getString(R.string.del_all_goods))
                    .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                    .setLabelColor(Color.WHITE)
                    .setWrapper(2)
            )
            items.add(RFACLabelItem<Int>()
                    .setLabel(getString(R.string.del_goods_current_selection))
                    .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                    .setLabelColor(Color.WHITE)
                    .setWrapper(3)
            )
            items.add(RFACLabelItem<Int>()
                    .setLabel(getString(R.string.enter_count))
                    .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                    .setLabelColor(Color.WHITE)
                    .setWrapper(4)
            )
            if (typeSelection == null) {
                typeSelection = arguments?.getByte(KEY_TYPE_SELECTION)
            }
            if (typeSelection != CLASSIC_SELECTION) {
                var title = ""
                when (typeSelection) {
                    BOXES_SELECTION -> title = getString(R.string.str_boxes_info)
                    TE_SELECTION -> title = getString(R.string.str_te_info)
                }
                items.add(RFACLabelItem<Int>()
                        .setLabel(title)
                        .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                        .setLabelColor(Color.WHITE)
                        .setWrapper(5)
                )
            }
            if (typeSelection == BOXES_SELECTION) {
                items.add(RFACLabelItem<Int>()
                        .setLabel(getString(R.string.select_pallet))
                        .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                        .setLabelColor(Color.WHITE)
                        .setWrapper(6)
                )
            }
            items.add(RFACLabelItem<Int>()
                    .setLabel(getString(R.string.str_exit_main_menu))
                    .setLabelBackgroundDrawable(ContextCompat.getDrawable(context!!, R.drawable.background_lbl_fab))
                    .setLabelColor(Color.WHITE)
                    .setWrapper(7)
            )

            (activity as BaseFragmentActivity).setFloatingMenu(items, this)
        }
    }

    override fun onClickRFA(position: Int, item: RFACLabelItem<*>?) {
        when (item?.wrapper) {
            0 -> completeSelection()
            1 -> startCheckOrderFragment(false)
            2 -> showDelDialog(true)
            3 -> showDelDialog(false)
            4 -> enterCount()
            5 -> showInfo()
            6 -> selectPallet()
            7 -> exitMainMenu()
        }
    }

    private fun selectPallet() {
        val palletPrintedList = order.palletPrintedList
        if (palletPrintedList.size > 0) {
            selectPallet(palletPrintedList)
        }
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_A) {
            if (event?.action == KeyEvent.ACTION_UP) {
                if (typeSelection == BOXES_SELECTION) {
                    selectPallet()
                }
            }
            true
        } else if (keyCode == KeyEvent.KEYCODE_B) {
            if (event?.action == KeyEvent.ACTION_UP) {
                showInfo()
            }
            true
        } else if (keyCode == KeyEvent.KEYCODE_H) {
            if (event?.action == KeyEvent.ACTION_UP) {

            }
            true
        } else if (keyCode == KeyEvent.KEYCODE_E) {
            if (event?.action == KeyEvent.ACTION_UP) {
                completeSelection()
            }
            true
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            if (event?.action == KeyEvent.ACTION_UP) {
                onRight()
                true
            } else {
                true
            }
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            if (event?.action == KeyEvent.ACTION_UP) {
                onLeft()
                true
            } else {
                true
            }
        } else if (keyCode == KeyEvent.KEYCODE_K && event?.action == KeyEvent.ACTION_UP) {
            enterCount()
            true
        } else if (keyCode == KeyEvent.KEYCODE_P && event?.action == KeyEvent.ACTION_UP) {
            startCheckOrderFragment(false)
            true
        } else if (keyCode == KeyEvent.KEYCODE_DEL && event?.action == KeyEvent.ACTION_UP) {
            if (event.metaState == META_CTRL_LEFT_ON) {
                showDelDialog(true)
            } else {
                showDelDialog(false)
            }
            true
        } else if (keyCode == KeyEvent.KEYCODE_X && event?.action == KeyEvent.ACTION_UP) {
            if (event.metaState == META_CTRL_LEFT_ON) {
                exitMainMenu()
            }
            true
        } else if ((keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE)
                && event?.action == KeyEvent.ACTION_UP) {
            completeSelection()
            true
        } else {
            false
        }
    }

    private fun showInfo() {
        when (typeSelection) {
            BOXES_SELECTION -> showDialogPutToBox()
            TE_SELECTION -> showDialogScanTe()
        }
    }

    private fun exitMainMenu() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.str_exit_main_menu))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        val fm = activity?.supportFragmentManager
                        for (i in 0 until fm?.backStackEntryCount!!) {
                            fm.popBackStack()
                        }
                        (activity as MainActivity).setInvisibleFloating()
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
        }
        alertDialog?.create()
        alertDialog?.show()
    }

    private fun enterCount() {
        val enterK: Byte = 1
        if (typeSelection == TE_SELECTION && order.scanTe == null) {
            showDialogScanTe()
        } else {
            if (order.mEnterK == enterK) {
                (activity as MainActivity).changeFragment(SelectionEnterCountFragment().newInstance(currentItem, order))
            } else {
                showError(getString(R.string.insufficient_rights))
            }
        }
    }

    private fun completeSelection() {
        when (typeSelection) {
            TE_SELECTION -> {
                if (order.isCompletePlacing(order.id)) {
                    completeDialog()
                } else {
                    showDialogZGS()
                }
            }
            else -> completeDialog()
        }
    }

    private fun startCheckOrderFragment(isExit: Boolean) {
        val items = ArrayList<OrderItem>()
        val allOrders = dao.getAllOrders(order.id)
        for (item in allOrders) {
            if (!item.mIsWeightGoods) {
                if (item.mEst < item.mNado) {
                    items.add(item)
                }
            } else {
                if (item.mHeadsCount == 0) {
                    if (item.mNado != item.mEst) {
                        if (item.mNado - item.mEst > 0) {
                            if ((item.mNado - item.mEst).compareTo(item.mHeadsWeight) > 0) {
                                items.add(item)
                            }
                        }
                    }
                } else {
                    if (item.mHeadsCount > item.mHeads) {
                        items.add(item)
                    } else {
                        if ((item.mNado - item.mEst).compareTo(item.mNado * order.mPercentMaxWeight) > 0) {
                            items.add(item)
                        }
                    }
                }
            }
        }
        if (items.size > 0) {
            (activity as MainActivity).changeFragment(SelectionCheckFragment().newInstance(items, isExit, order))
        } else {
            when (typeSelection) {
                TE_SELECTION -> {
                    if (order.isComplete(order.id, order.mPercentMaxWeight) && order.isCompletePlacing(order.id)) {
                        completeDialog()
                    } else {
                        val unplaced = ArrayList<OrderItem>()
                        for (item in allOrders) {
                            if (item.mPlacedPieces == 0.0 || item.mEst.compareTo(item.mPlacedPieces) > 0) {
                                unplaced.add(item)
                            }
                        }
                        if (unplaced.size > 0) {
                            showDialogUnplaced(unplaced, isExit)
                        }
                    }
                }
                else -> {
                    if (order.isComplete(order.id, order.mPercentMaxWeight)) {
                        completeDialog()
                    }
                }
            }
        }
    }

    private fun showDialogUnplaced(unplaced: ArrayList<OrderItem>, isExit: Boolean) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.order_unplaced))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        (activity as MainActivity).changeFragment(SelectionCheckFragment().newInstance(unplaced, isExit, order))
                    }
        }
        alertDialog?.create()
        alertDialog?.show()
    }

    private fun showDialogPutToBox() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.put_to_box_message, currentItem.mBoxNumber.toString()))
                    .setPositiveButton(getString(R.string.ok)) { _, _ -> fragmentManager?.popBackStack() }
        }
        alertDialog?.create()
        alertDialog?.show()
    }


    private fun showDelDialog(all: Boolean) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(if (all) getString(R.string.del_all_goods) else getString(R.string.del_goods_current_selection))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        if (all) {
                            val allItems = dao.getAllOrders(order.id)
                            allItems?.forEach { it -> clear(it, dao) }
                        } else {
                            clear(currentItem, dao)
                        }
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
        }
        alertDialog?.create()
        alertDialog?.show()
    }

    private fun clear(orderItem: OrderItem, dao: OrderSelectionDao) {
        orderItem.mHeads = orderItem.mPlacedHeads
        orderItem.mEst = orderItem.mPlacedPieces
        orderItem.mMissing = false
        dao.update(orderItem)
    }

    override fun onCompleteCheckTe(te: String) {
        alertDialogScanTE.dismiss()
        val oldTe = order.scanTe
        if (oldTe != null) {
            val allOrders = dao.getAllOrders(order.id)
            for (item in allOrders) {
                if (oldTe == item.mTE && !order.isCompletePlacingItem(item)) {
                    item.mTE = te
                    dao.update(item)
                }
            }
        }
        order.scanTe = te
        dao.update(order)
    }

    override fun onCompleteSendPart() {
        alertDialogScanZGS.dismiss()
        if (!order.isCompletePlacing(order.id)) {
            showDialogScanTe()
        }
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (::alertDialogScanZGS.isInitialized && alertDialogScanZGS.isShowing) {
            if (goodsCard?.barcode != null && goodsCard.barcode.substring(0, 2) == PREFIX_CELL) {
                val args = ArrayList<String>()
                val candidates = ArrayList<_62f>()
                val allOrders = dao.getAllOrders(order.id)
                for (orderItem in allOrders) {
                    val headsPlaced = orderItem.mHeads - orderItem.mPlacedHeads
                    val piecesPlaced = orderItem.mEst - orderItem.mPlacedPieces
                    if (orderItem.mTE == order.scanTe) {
                        args.add(String.format(Locale.US, "%s;%s;%s;%s;%.3f;%s;%s;%s;%s;%s;",
                                orderItem.mTask,
                                orderItem.code,
                                0,
                                headsPlaced,
                                piecesPlaced,
                                orderItem.mTE, goodsCard.barcode.substring(2),
                                orderItem.mCell, orderItem.mBestBeforeStart, orderItem.mBestBeforeEnd))
                        candidates.add(_62f(orderItem.Index, headsPlaced, piecesPlaced))
                    }
                }
                if (candidates.size < 1) {
                    alertDialogScanZGS.dismiss()
                    showError(getString(R.string.error_not_found_enter_count))
                } else {
                    presenter.sendPart(args, candidates, order.id)
                }
            } else {
                showError(getString(R.string.error_barcode_cell))
            }
        } else if (::alertDialogScanTE.isInitialized && alertDialogScanTE.isShowing) {
            if (goodsCard?.te != null) {
                presenter.checkTe(goodsCard.te)
            } else {
                showError(getString(R.string.error_barcode_te))
            }
        } else {
            if (typeSelection == TE_SELECTION && order.scanTe == null) {
                showDialogScanTe()
            } else {
                var isExist = false
                for (selectionBarcode in order.mBarcode) {
                    if (selectionBarcode.bacrode == goodsCard?.barcode) {
                        if (currentItem.code == selectionBarcode.code) {
                            (activity as MainActivity).changeFragment(SelectionEnterCountFragment().newInstance(currentItem, order))
                            isExist = true
                            break
                        }
                        break
                    }
                }
                if (!isExist) {
                    Toast.makeText(context, getString(R.string.error_barcode), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onCompleteReserveSelectionOrder() {
        val fm = activity?.supportFragmentManager
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.selection_is_complete))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        fm?.popBackStack()
                    }
                    .setNegativeButton(getString(R.string.cancel)) { _, _ ->
                        for (i in 0 until fm?.backStackEntryCount!!) {
                            fm.popBackStack()
                        }
                        (activity as MainActivity).setInvisibleFloating()
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }


    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }

    private fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)

    private fun showBoxesInfo() {
        val boxes = ArrayList<Box>()
        val taskNum = arguments?.getString(KEY_TASK_NUMBER)
        val order = dao.getOrder(taskNum)
        val orderItems = dao.getAllOrders(order.id)
        for (item in orderItems) {
            if (!item.mIsBoxSeparate) {
                if (boxes.size == 0) {
                    boxes.add(Box(item.mBoxNumber, item.mBoxName, 1))
                } else {
                    var wasFind = false
                    for (i in 0 until boxes.size) {
                        val box = boxes[i]
                        if (box.description == item.mBoxName && box.id == item.mBoxNumber) {
                            box.count++
                            boxes[i] = box
                            wasFind = true
                            break
                        }
                    }
                    if (!wasFind) {
                        boxes.add(Box(item.mBoxNumber, item.mBoxName, 1))
                    }
                }
            }
        }
        //Собираем сообщение
        if (boxes.size > 0) {
            val differentBoxes = ArrayList<Box>()
            for (item in boxes) {
                if (differentBoxes.size == 0) {
                    differentBoxes.add(Box(item.id, item.description, 1))
                } else {
                    var wasFind = false
                    for (i in 0 until differentBoxes.size) {
                        val box = differentBoxes[i]
                        if (box.description == item.description) {
                            box.count++
                            differentBoxes[i] = box
                            wasFind = true
                            break
                        }
                    }
                    if (!wasFind) {
                        differentBoxes.add(Box(item.id, item.description, 1))
                    }
                }
            }
            var mes = "Требуется взять:\n"
            for (item in differentBoxes) {
                mes = String.format("%s%s - %s\n", mes, item.description, item.count)
            }
            val alertDialog = context?.let {
                AlertDialog.Builder(it)
                        .setMessage(mes)
                        .setNegativeButton(getString(R.string.ok), null)
                        .create()
            }
            if (!alertDialog?.isShowing!!) {
                alertDialog.show()
            }
        }
    }
}