package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.entities.Bailor;
import com.madlab.wms.grinfeld.roman.entities.parser.BailorParser;
import com.madlab.wms.grinfeld.roman.ui.MainActivity;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.OrderListFragment;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.List;


/**
 * Created by GrinfeldRa
 * список поклажедателей
 */
public class BailorListFragment extends MvpAppCompatFragment implements AdapterView.OnItemClickListener {

    private static final String KEY_STR_PARAM = "key_str_param";
    private static final String KEY_TYPE = "key_type";
    private Context context;

    private ListView listView;


    public static BailorListFragment newInstance(String param, int type) {
        Bundle args = new Bundle();
        args.putString(KEY_STR_PARAM, param);
        args.putInt(KEY_TYPE, type);
        BailorListFragment fragment = new BailorListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (listView != null) {
            listView.setFocusable(true);
            listView.requestFocus();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            ((BaseFragmentActivity) getActivity()).setActionBarTitle(getString(R.string.select_owner));
        }
        return inflater.inflate(R.layout.bailor_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        listView = view.findViewById(R.id.list_view);
        if (getArguments() != null) {
            List<Bailor> data = BailorParser.getList(getArguments().getString(KEY_STR_PARAM));
            MyAdapter myAdapter = new MyAdapter<>(context, data);
            float weightLeft = 0.25f;
            float weightRight = 0.75f;
            myAdapter.setWeight(weightLeft, weightRight);
            Utils.setParamFromHeader(view, weightLeft, weightRight, getString(R.string.code), getString(R.string.bailor));
            listView.setAdapter(myAdapter);
            listView.setOnItemClickListener(this);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bailor bailor = (Bailor) parent.getItemAtPosition(position);
        if (getActivity() != null)
            if (getArguments() != null) {
                ((MainActivity) getActivity())
                        .changeFragment(OrderListFragment.newInstance(bailor.getCode(),
                                getArguments().getInt(KEY_TYPE)));
            }

    }

}
