package com.madlab.wms.grinfeld.roman.ui.worklist.inventory;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.App;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.InventoryCell;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner;
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner;
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent;
import com.madlab.wms.grinfeld.roman.ui.MainActivity;
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.InventoryProductList;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRa
 */
public class InventoryListFragment extends MvpAppCompatFragment implements IOnScannerEvent, InventoryListView, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener, View.OnKeyListener, View.OnClickListener {

    private static final String TAG = "#InventoryListFragment";
    private static final String KEY_TYPE_INVENTORY = "type_inventory";
    private static final String KEY_LIST_INVENTORY = "list_inventory";
    private Context context;
    private ProgressOwner progressOwner;
    private AbstractScanner scanner;
    private MyAdapter listAdapter;
    private List<InventoryCell> data;
    private InventoryCell inventoryCell;
    private TextView txt_number,
            txt_start_date,
            txt_type_cell,
            txt_cell;
    private ListView listView;

    @InjectPresenter
    InventoryListPresenter presenter;

    @ProvidePresenter
    InventoryListPresenter providePresenter() {
        return new InventoryListPresenter();
    }


    public static InventoryListFragment newInstance(ArrayList<InventoryCell> inventoryCells, int type) {
        InventoryListFragment fragment = new InventoryListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE_INVENTORY, type);
        bundle.putParcelableArrayList(KEY_LIST_INVENTORY, inventoryCells);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ProgressOwner) {
            progressOwner = ((ProgressOwner) context);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            if (getArguments() != null) {
                String title = "";
                switch (getArguments().getInt(KEY_TYPE_INVENTORY)) {
                    case Command.TYPE_CELL:
                        title = getString(R.string.inventory_cell);
                        break;
                    case Command.TYPE_EUZ:
                        title = getString(R.string.inventory_euz);
                        break;
                }
                ((BaseFragmentActivity) getActivity()).setActionBarTitle(title);
            }
        }
        return inflater.inflate(R.layout.inventory_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = view.findViewById(R.id.list_view);
        txt_number = view.findViewById(R.id.txt_number);
        txt_start_date = view.findViewById(R.id.txt_start_date);
        txt_type_cell = view.findViewById(R.id.txt_type_cell);
        txt_cell = view.findViewById(R.id.txt_cell);
        FloatingActionButton fb_next = view.findViewById(R.id.fb_next);
        data = new ArrayList<>();
        listAdapter = new MyAdapter<>(context, data);
        Utils.setParamFromHeader(view, 1, 1, getString(R.string.cell), getString(R.string.state));
        listView.setAdapter(listAdapter);
        listView.setOnItemSelectedListener(this);
        listView.setOnItemClickListener(this);
        listView.setOnKeyListener(this);
        fb_next.setOnClickListener(this);

        if (getArguments() != null) {
            int type = getArguments().getInt(KEY_TYPE_INVENTORY);
            ArrayList<InventoryCell> inventoryCells = getArguments().getParcelableArrayList(KEY_LIST_INVENTORY);
            if (inventoryCells != null && inventoryCells.size() > 0) {
                data.clear();
                data.addAll(inventoryCells);
                listAdapter.notifyDataSetChanged();
            } else {
                presenter.loadInventory(type);
            }
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        if (listView != null) {
            listView.setFocusable(true);
            listView.requestFocus();
        }
        scanner = new FactoryScanner().getScanner(context, this);
        scanner.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        scanner.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        scanner.onStop();
    }

    @Override
    public void onDataScanner(ScannerData scannerData) {
        String barcode = scannerData.getBarcode();
        if (barcode != null && getArguments() != null) {
            switch (getArguments().getInt(KEY_TYPE_INVENTORY)) {
                case Command.TYPE_CELL:
                    String prefixBarcode = scannerData.getBarcode().substring(0, 2);
                    String mBarcode = barcode.substring(2);
                    if (prefixBarcode.equals("99")) {
                        presenter.loadDataFromCellBarcode(mBarcode, 1);
                    } else {
                        showError(App.getAppContext().getString(R.string.error_not_the_barcode));
                    }
                    break;
                case Command.TYPE_EUZ:
                    //presenter.loadProductFromBarcode(goodsCard.barcode)
                    break;
            }
        }
    }

    @Override
    public void onStatusUpdateScanner(String scanStatus) {
        Log.d(TAG, scanStatus);
    }

    @Override
    public void onErrorScanner(String error) {
        showError(error);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        inventoryCell = (InventoryCell) parent.getItemAtPosition(position);
        txt_number.setText(getString(R.string.inventory_num, inventoryCell.getNumber_inventory()));
        txt_start_date.setText(getString(R.string.inventory_start, inventoryCell.getStart_date()));
        String type_cell;
        switch (inventoryCell.getType_cell()) {
            case 1:
                type_cell = getString(R.string.inventory_type1);
                break;
            case 2:
                type_cell = getString(R.string.inventory_type2);
                break;
            case 3:
                type_cell = getString(R.string.inventory_type3);
                break;
            default:
                type_cell = "";
        }
        txt_type_cell.setText(getString(R.string.inventory_type, type_cell));
        txt_cell.setText(getString(R.string.inventory_cell_num, inventoryCell.getNumber_cell()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        parent.requestFocusFromTouch();
        parent.setSelection(position);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                onClickInventoryItem();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onCompleteLoadInventoryCell(List<InventoryCell> inventoryCells) {
        data.clear();
        data.addAll(inventoryCells);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCompleteLoadProductInInventoryCell(ArrayList<InventoryCellProduct> list, String createDate, String inventoryNum) {
        if (getActivity() != null) {
            if (getArguments() != null) {
                ((MainActivity) getActivity()).changeFragment(InventoryProductList.newInstance(list, inventoryNum, createDate, getArguments().getInt(KEY_TYPE_INVENTORY)));
            }
        }
    }

    @Override
    public void showDialog(String barcode) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.confirmation))
                .setMessage(getString(R.string.error_inventory))
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> presenter.loadDataFromCellBarcode(barcode, 0))
                .setNegativeButton(getString(R.string.no), null)
                .show();
    }

    @Override
    public void onCompleteLoadDataFromCellBarcode(String numberInventory, String createDate) {
        if (getArguments() != null) {
            presenter.loadProductInInventoryCell(numberInventory, createDate, getArguments().getInt(KEY_TYPE_INVENTORY));
        }
    }

    @Override
    public void onClick(View v) {
        onClickInventoryItem();
    }


    private void onClickInventoryItem() {
        if (inventoryCell != null) {
            if (inventoryCell.getState_cell() == 1) {
                presenter.loadDataFromCellBarcode(inventoryCell.getBarcode(), 1);
            } else {
                if (getArguments() != null) {
                    presenter.loadProductInInventoryCell(inventoryCell.getNumber_inventory(), inventoryCell.getCreate_date(), getArguments().getInt(KEY_TYPE_INVENTORY));
                }
            }
        }
    }
}
