package com.madlab.wms.grinfeld.roman.utils

import android.text.Editable

/**
 * Created by grinfeldra
 */
fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)