package com.madlab.wms.grinfeld.roman.ui.worklist.shipment

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment
import io.reactivex.android.schedulers.AndroidSchedulers
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by grinfeldra
 */
@InjectViewState
class ShipmentPresenter : BasePresenter<ShipmentView>() {

    fun loadItem(barcode: String) {
        add(WMSClient().send(Command.COM_GET_ITEM_SHIPMENT(0, barcode,
                SimpleDateFormat("dd.mm.yyyy", Locale.US).format(Date())))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .subscribe {
                    if (it.contains("ERROR")) {
                        viewState.hideProgress()
                        viewState.showError(it.split(";")[1])
                    } else {
                        val split = it.split(";")
                        val type = split[5]
                        val docNum = split[6]
                        val nameOwner = split[7]
                        loadItemInfo(type, docNum, nameOwner)
                    }
                })
    }

    private fun loadItemInfo(type: String, docNum: String, nameOwner: String) {
        add(WMSClient().send(Command.COM_GET_ITEM_INFO_SHIPMENT(type, docNum, nameOwner))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val data = Shipment.parse(it)
                        viewState.completeLoadItemInfo(data, docNum, nameOwner)
                    }
                })
    }


}