package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order;


import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.GoodsInDoc;
import com.madlab.wms.grinfeld.roman.entities.ProductParam;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;

import java.util.List;

/**
 * Created by GrinfeldRa
 */
interface OrderView extends BaseView {

    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadParamGoods(ProductParam productParam, ScannerData scannerData, int position);
    @StateStrategyType(SkipStrategy.class)
    void onCloseOrder(String s);
    @StateStrategyType(SkipStrategy.class)
    void onCompleteDeleteGoods();
    @StateStrategyType(SkipStrategy.class)
    void onCompleteGetGoodsInOrder(List<GoodsInDoc> goodsInDocs);
    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadParamPallet(String byteToString, ScannerData scannerData);
    @StateStrategyType(SkipStrategy.class)
    void onCompleteSentToPrintTE();
    @StateStrategyType(SkipStrategy.class)
    void onErrorLoadParamGoods(ScannerData scannerData, String s);
}
