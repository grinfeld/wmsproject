package com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph

import android.app.AlertDialog
import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.selection.Order
import com.madlab.wms.grinfeld.roman.entities.selection._62f
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
@InjectViewState
class SelectionsMorphPresenter : BasePresenter<SelectionsMorphView>() {

    fun reserveSelectionOrder(order: Order) {
        mCompositeDisposable.add(WMSClient().sendArray(Command.COM_RESERVE_SELECTION_BOXES(),
                Command.COM_RESERVE_SELECTION_BOXES_PARAM(order))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val dao = App.getDatabase().orderSelectionDao()
                        dao.delete(order)
                        dao.deleteAllOrderItem(order.id)
                        viewState.onCompleteReserveSelectionOrder()
                    }
                })
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.onFirstViewAttach()
    }

    fun sendToPrint(host: String, mID: String, mTE: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_SEND_TO_PRINT_SELECTION(host, mID, mTE))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    isSuccess(it)
                })
    }

    fun checkTe(te: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_CHECK_TE(te))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.hideProgress()
                        viewState.onCompleteCheckTe(te)
                    }
                })
    }

    fun sendPart(args: ArrayList<String>, candidatesIndex: ArrayList<_62f>, id: Int) {
        mCompositeDisposable.add(WMSClient().sendArray(Command.COM_PLACEMENT_ZGS(), args)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val dao = App.getDatabase().orderSelectionDao()
                        for (item in candidatesIndex) {
                            val orderItem = dao.getOrderItem(item.index, id)
                            orderItem.mPlacedHeads = orderItem.mPlacedHeads + item.heads
                            orderItem.mPlacedPieces = orderItem.mPlacedPieces + item.pieces
                            dao.update(orderItem)
                        }
                        viewState.hideProgress()
                        viewState.onCompleteSendPart()
                    }
                })
    }
}