package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_editor_count

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct
import com.madlab.wms.grinfeld.roman.entities.Measure
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.ordercard.IDateAutocomplete
import com.madlab.wms.grinfeld.roman.utils.FocusedTextWatcher
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.InventoryCellProductViewModel
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.InventoryProductList
import com.madlab.wms.grinfeld.roman.utils.DateValidator
import com.madlab.wms.grinfeld.roman.utils.toEditable
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.*
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.et_count
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.et_expiration_day
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.et_expiration_mouth
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.et_expiration_year
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.et_prod_day
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.et_prod_mouth
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.et_prod_year
import kotlinx.android.synthetic.main.inventory_editor_count_fragment.spinner_measure
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by GrinfeldRa
 */

private const val PRODUCT = "cell_product"
private const val METHOD = "method"

class InventoryEditorCountFragment : MvpAppCompatFragment(), TextWatcher, InventoryEditorCountView, KeyEventListener, AdapterView.OnItemSelectedListener, IDateAutocomplete {

    @InjectPresenter
    lateinit var presenter: InventoryEditorCountPresenter

    @ProvidePresenter
    fun providePresenter() = InventoryEditorCountPresenter()

    private var totalCount = 0.0
    private var totalCountDefective = 0.0
    private var countPack = 0
    private var countPackDefective = 0
    private var measure: LinkedHashMap<String, Double>? = null
    private var product: InventoryCellProduct? = null
    private var method: Int? = null
    private var model: InventoryCellProductViewModel? = null


    fun newInstance(product: InventoryCellProduct, method: Int) = InventoryEditorCountFragment().apply {
        arguments = Bundle().apply {
            putParcelable(PRODUCT, product)
            putInt(METHOD, method)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(activity!!).get(InventoryCellProductViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.inventory_editor_count_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        product = arguments?.getParcelable<InventoryCellProduct>(PRODUCT)
        method = arguments?.getInt(METHOD)
        if (activity != null) {
            (activity as BaseFragmentActivity).setActionBarTitle(product?.numberCell)
        }
        initDateField()
        initListeners()
    }

    private fun initListeners() {
        et_count.setOnKeyListener(this)
        et_count_defective.setOnKeyListener(this)
        et_count.addTextChangedListener(this)
        et_count_defective.addTextChangedListener(this)
        spinner_measure.onItemSelectedListener = this
        et_prod_day.addTextChangedListener(FocusedTextWatcher(et_prod_day, this))
        et_prod_mouth.addTextChangedListener(FocusedTextWatcher(et_prod_mouth, this))
        et_prod_year.addTextChangedListener(FocusedTextWatcher(et_prod_year, this))
        et_expiration_day.addTextChangedListener(FocusedTextWatcher(et_expiration_day, this))
        et_expiration_mouth.addTextChangedListener(FocusedTextWatcher(et_expiration_mouth, this))
        et_expiration_year.addTextChangedListener(FocusedTextWatcher(et_expiration_year, this))
    }

    private fun initDateField() {
        txt_code.text = product?.codeGoods
        txt_name.text = product?.goodsName
        val numberFormat = NumberFormat.getInstance()
        txt_inventory.text = getString(R.string.inventory, numberFormat.format(product?.count), product?.measureInitName)
        if (product?.isEdited!!) {
            et_count.text = numberFormat.format(product?.countFact).replace("\\s+".toRegex(), "").toEditable()
        } else {
            et_count.text = numberFormat.format(product?.count).replace("\\s+ ".toRegex(), "").toEditable()
        }
        et_count.placeCursorToEnd()
        et_count_defective.text = numberFormat.format(product?.countDefective).replace("\\s+".toRegex(), "").toEditable()
        et_count_defective.placeCursorToEnd()
        val productionDate = product?.productionDate?.split(".")
        val expirationDate = product?.expirationDate?.split(".")
        if (productionDate != null) {
            et_prod_day.text = productionDate[0].toEditable()
            et_prod_day.placeCursorToEnd()
            et_prod_mouth.text = productionDate[1].toEditable()
            et_prod_mouth.placeCursorToEnd()
            et_prod_year.text = productionDate[2].toEditable()
            et_prod_year.placeCursorToEnd()
        }
        if (expirationDate != null) {
            et_expiration_day.text = expirationDate[0].toEditable()
            et_expiration_day.placeCursorToEnd()
            et_expiration_mouth.text = expirationDate[1].toEditable()
            et_expiration_mouth.placeCursorToEnd()
            et_expiration_year.text = expirationDate[2].toEditable()
            et_expiration_year.placeCursorToEnd()
        }
        measure = Measure.getMapMeasure(product)
        if (measure != null) {
            val list = ArrayList<String>()
            for ((key) in measure as LinkedHashMap<String, Double>) {
                list.add(key)
            }
            val dataAdapter = context?.let { ArrayAdapter(it, android.R.layout.simple_spinner_item, list) }
            spinner_measure.adapter = dataAdapter!!
            spinner_measure_defective.adapter = dataAdapter
            spinner_measure_defective.isEnabled = false
        }
    }


    private fun EditText.placeCursorToEnd() {
        this.setSelection(this.text.length)
    }


    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_DEL -> {
                if (event?.action == KeyEvent.ACTION_DOWN) {
                    if ((v?.id == R.id.et_count && countPack != 0 && et_count.text.isEmpty())
                            || (v?.id == R.id.et_count_defective && countPackDefective != 0 && et_count_defective.text.isEmpty())) {
                        val alertDialog = context?.let {
                            AlertDialog.Builder(it)
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .setTitle(getString(R.string.confirmation))
                                    .setMessage(getString(R.string.delete_goods))
                                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                                        presenter.onDeleteCountProduct(v)
                                    }
                                    .setNegativeButton(getString(R.string.no), null)
                                    .create()
                        }
                        if (!alertDialog?.isShowing!!) {
                            alertDialog.show()
                        }
                        return true
                    } else {
                        return false
                    }
                }
                return false
            }
            KeyEvent.KEYCODE_BACK -> {
                if (event?.action == KeyEvent.ACTION_DOWN) {
                    val alertDialog = context?.let {
                        AlertDialog.Builder(it)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle(getString(R.string.confirmation))
                                .setMessage(getString(R.string.exit_not_save))
                                .setPositiveButton(getString(R.string.ok)) { _, _ ->
                                    activity?.onBackPressed()
                                }
                                .setNegativeButton(getString(R.string.cancel), null)
                    }
                    alertDialog?.create()
                    alertDialog?.show()
                    return true
                }
                return false
            }
            KeyEvent.KEYCODE_ENTER -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    val position = spinner_measure.selectedItemPosition
                    val nameMeasure = spinner_measure.adapter.getItem(position) as String
                    val quantum: Double = measure?.get(nameMeasure)!!
                    if (activity?.currentFocus?.id == R.id.et_count && !TextUtils.isEmpty(et_count.text)) {
                        countPack += 1
                        totalCount += et_count.text.toString().toDouble() * quantum
                        txt_total_count.text = countPack.toString()
                        et_count.setText("")
                    } else if (activity?.currentFocus?.id == R.id.et_count_defective && !TextUtils.isEmpty(et_count_defective.text)) {
                        countPackDefective += 1
                        totalCountDefective += et_count_defective.text.toString().toDouble() * quantum
                        txt_total_count_defective.text = countPackDefective.toString()
                        et_count_defective.setText("")
                    } else {
                        if (isNotEmptyFields()) {
                            if (isDateFormatCorrect()) {
                                (context as BaseFragmentActivity).removeKeyEventHandler(this)
                                product?.isEdited = true
                                product?.countFact = if (totalCount == 0.0) product?.countFact!! else totalCount
                                product?.countDefective = if (totalCountDefective == 0.0) product?.countDefective!! else totalCountDefective
                                product?.productionDate = getDateProd(false)
                                product?.expirationDate = getDateExpiration(false)
                                if (method == InventoryProductList.METHOD_ADD_OR_EDIT) {
                                    model?.addOrReplace(product!!)
                                } else {
                                    model?.addNewSeries(product!!)
                                }
                                activity?.onBackPressed()
                            } else {
                                Toast.makeText(context, getString(R.string.invalid_date_format), Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            Toast.makeText(context, getString(R.string.fill_in_all_fields), Toast.LENGTH_SHORT).show()
                        }
                        return false
                    }
                }
                return true
            }
            KeyEvent.KEYCODE_E -> {
                if (event?.action == KeyEvent.ACTION_DOWN) {
                    if (measure != null) {
                        val id = spinner_measure.selectedItemPosition
                        if (id == measure?.size?.minus(1)) {
                            spinner_measure.setSelection(0)
                            spinner_measure_defective.setSelection(0)
                        } else {
                            spinner_measure.setSelection(id + 1)
                            spinner_measure_defective.setSelection(id + 1)
                        }
                    }
                    return true
                }
                return false
            }
            else -> return false
        }
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val position = spinner_measure.selectedItemPosition
        val nameMeasure = spinner_measure.adapter.getItem(position) as String
        val quantum: Double = measure?.get(nameMeasure)!!
        val accepted = if (s.toString().isEmpty()) 0.0 else s.toString().toDouble()
        if (et_count.text.hashCode() == s.hashCode()) {
            product?.countFact = accepted
        } else if (et_count_defective.text.hashCode() == s.hashCode()) {
            product?.countDefective = accepted
        }
        presenter.onInitFieldsMeasure(product, quantum, totalCount, totalCountDefective)
    }

    private fun isNotEmptyFields(): Boolean {
        return (totalCountDefective != 0.0 || totalCount != 0.0) &&
                !TextUtils.isEmpty(et_prod_day.text) &&
                !TextUtils.isEmpty(et_prod_mouth.text) &&
                !TextUtils.isEmpty(et_prod_year.text) &&
                !TextUtils.isEmpty(et_expiration_day.text) &&
                !TextUtils.isEmpty(et_expiration_mouth.text) &&
                !TextUtils.isEmpty(et_expiration_year.text)
    }

    private fun isDateFormatCorrect(): Boolean {
        val dateValidator = DateValidator()
        return dateValidator.validate(getDateProd(true)) && dateValidator.validate(getDateExpiration(true))
    }


    private fun getDateProd(validator: Boolean): String {
        val prodDay = et_prod_day.text.toString()
        val prodMouth = et_prod_mouth.text.toString()
        val prodYear = et_prod_year.text.toString()
        return if (validator) String.format("%s%s%s", prodYear, prodMouth, prodDay)
        else String.format("%s.%s.%s", prodDay, prodMouth, prodYear)
    }

    private fun getDateExpiration(validator: Boolean): String {
        val expirationDay = et_expiration_day.text.toString()
        val expirationMouth = et_expiration_mouth.text.toString()
        val expirationYear = et_expiration_year.text.toString()
        return if (validator) String.format("%s%s%s", expirationYear, expirationMouth, expirationDay)
        else String.format("%s.%s.%s", expirationDay, expirationMouth, expirationYear)
    }

    override fun onDeleteCountProduct(v: View) {
        if (v.id == R.id.et_count) {
            totalCount = 0.0
            countPack = 0
            txt_total_count.text = ""
        } else if (v.id == R.id.et_count_defective) {
            totalCountDefective = 0.0
            countPackDefective = 0
            txt_total_count_defective.text = ""
        }
        val position = spinner_measure.selectedItemPosition
        val nameMeasure = spinner_measure.adapter.getItem(position) as String
        var quantum = measure?.get(nameMeasure)
        quantum = quantum ?: 1.0
        presenter.onInitFieldsMeasure(product, quantum, totalCount, totalCountDefective)
    }

    override fun showError(error: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val nameMeasure = parent?.getItemAtPosition(position) as String
        val quantum: Double = measure?.get(nameMeasure)!!
        presenter.onInitFieldsMeasure(product, quantum, totalCount, totalCountDefective)
    }

    override fun onInitFieldsMeasure(accepted: String, acceptedDefective: String) {
        txt_fact.text = getString(R.string.fact, accepted)
        txt_defective.text = getString(R.string.defective_label, acceptedDefective)
    }

    override fun onDateAutocomplete() {
        try {
            if (product != null) {
                var dateProd = getDateProd(true)
                if (DateValidator().validate(dateProd)) {
                    val shelfLife = product?.shelfLifeCount
                    val format = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
                    val calendar = Calendar.getInstance()
                    calendar.time = format.parse(dateProd)
                    when (product?.shelfLifeType) {
                        1 -> shelfLife?.let { calendar.add(Calendar.YEAR, it) }
                        2 -> shelfLife?.let { calendar.add(Calendar.MONTH, it) }
                        3 -> shelfLife?.let { calendar.add(Calendar.DATE, it) }
                    }
                    dateProd = format.format(calendar.time)
                    et_expiration_day.setText(dateProd.substring(6, 8))
                    et_expiration_mouth.setText(dateProd.substring(4, 6))
                    et_expiration_year.setText(dateProd.substring(0, 4))
                }
            }
        } catch (e: ParseException) {

        }

    }

}