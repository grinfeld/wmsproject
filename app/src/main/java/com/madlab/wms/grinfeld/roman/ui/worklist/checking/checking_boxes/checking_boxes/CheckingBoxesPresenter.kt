package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.RouteBoxes
import io.reactivex.android.schedulers.AndroidSchedulers
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by grinfeldra
 */
@InjectViewState
class CheckingBoxesPresenter : BasePresenter<CheckingBoxesView>() {

    fun reserveChecking(route: RouteBoxes) {
        val param = ArrayList<String>()
        param.add(String.format(Locale.US, "%s;%s;%s;", route.docNum, route.date, route.euzBoxesRoutes.size))
        val numberFormat = NumberFormat.getInstance()
        for (euz in route.euzBoxesRoutes) {
            param.add(String.format(Locale.US, "%s;;;%s;;0;0;%s;", euz.codeGoods, numberFormat.format(euz.enterCount), euz.numBoxes))
        }
        add(WMSClient().sendArray(Command.RESERVE_CHECKING_BOXES(), param)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val dao = App.getDatabase().checkingBoxesDao()
                        dao.delete(route)
                        viewState.onCompleteReserve()
                    }
                })
    }

}