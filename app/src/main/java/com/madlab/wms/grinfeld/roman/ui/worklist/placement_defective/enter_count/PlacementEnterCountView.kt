package com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective.enter_count

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell

/**
 * Created by grinfeldra
 */
interface PlacementEnterCountView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onBlockCellComplete(cell: String, cellTxt: String)

    @StateStrategyType(SkipStrategy::class)
    fun onUnblockCellComplete()

    @StateStrategyType(SkipStrategy::class)
    fun reservePlacementComplete(p: PlacementCell)

    @StateStrategyType(SkipStrategy::class)
    fun initFieldsMeasure(rest: String, placement: String)
}