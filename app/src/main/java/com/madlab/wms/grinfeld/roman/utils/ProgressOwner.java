package com.madlab.wms.grinfeld.roman.utils;

/**
 * Created by GrinfeldRa
 */
public interface ProgressOwner {

    void setProgressState(boolean refreshing);

}
