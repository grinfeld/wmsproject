package com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentOwner
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class ShipmentOwnerMorphPresenter : BasePresenter<ShipmentOwnerMorphView>() {

    fun loadShipment(type: Int) {
        add(WMSClient().send(Command.COM_GET_SHIPMENT(type))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val result = ShipmentOwner.parse(it)
                        viewState.completeLoadShipment(result)
                    }
                })
    }

    fun loadItem(shipment: ShipmentOwner) {
        var type = shipment.type
        if (shipment.type == SELECTION_BOX) {
            type = 3
        }
        val code = shipment.code
        val date = shipment.date
        add(WMSClient().send(Command.COM_GET_ITEM_SHIPMENT(type, code, date))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .subscribe {
                    if (it.contains("ERROR")) {
                        viewState.hideProgress()
                        viewState.showError(it.split(";")[1])
                    } else {
                        val split = it.split(";")
                        val t = split[5]
                        val docNum = split[6]
                        val nameOwner = split[7]
                        loadItemInfo(t, docNum, nameOwner)
                    }
                })
    }

    private fun loadItemInfo(type: String, docNum: String, nameOwner: String) {
        add(WMSClient().send(Command.COM_GET_ITEM_INFO_SHIPMENT(type, docNum, nameOwner))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val data = Shipment.parse(it)
                        viewState.completeLoadItemInfo(data, docNum, nameOwner)
                    }
                })
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.onFirstViewAttach()
    }

}