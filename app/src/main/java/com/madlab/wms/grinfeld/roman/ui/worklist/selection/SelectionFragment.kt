package com.madlab.wms.grinfeld.roman.ui.worklist.selection

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.selection.MyPrinter
import com.madlab.wms.grinfeld.roman.entities.selection.Order
import com.madlab.wms.grinfeld.roman.entities.selection.OrderItem
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.SelectionsMorphFragment
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.selection_fragment.*

/**
 * Created by grinfeldra
 */
class SelectionFragment : MvpAppCompatFragment(), SelectionView, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    private var progressOwner: ProgressOwner? = null
    private lateinit var mTaskNumber: String
    private var typeSelection: Byte? = null
    private lateinit var order: Order
    private lateinit var orderItem: OrderItem

    @InjectPresenter
    lateinit var presenter: SelectionPresenter


    @ProvidePresenter
    fun providePresenter() = SelectionPresenter()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.selection_title))
        return inflater.inflate(R.layout.selection_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.newSelection()
    }


    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }


    override fun onCompleteGetListPrinters(mTaskNumber: String, typeSelection: Byte) {
        val printers = App.getDatabase().orderSelectionDao().printers
        this.mTaskNumber = mTaskNumber
        this.typeSelection = typeSelection
        val adapter = MyAdapter(context, printers)
        list_view.adapter = adapter
        list_view.onItemClickListener = this
        list_view.onItemSelectedListener = this
        val dao = App.getDatabase().orderSelectionDao()
        order = dao.getOrder(mTaskNumber)
        val currentPosition = order.currentPosition
        val id = order.id
        orderItem = dao.getOrderItem(currentPosition, id)
        tv_load.text = getString(R.string.will_be_print, orderItem.mTENumber.toString())
        Utils.setParamFromHeader(view, getString(R.string.printer_lbl), getString(R.string.host_printer_lbl))
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.select_printer))
    }

    override fun onNotAvailable() {
        tv_load.visibility = View.GONE
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.no_available))
                    .setNegativeButton(getString(R.string.ok), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val printerHost = (parent?.getItemAtPosition(position) as MyPrinter).host
        presenter.sendToPrint(printerHost, order.mID, orderItem.mTENumber.toString())

    }

    override fun onCompleteSendToPrint() {
        (activity as MainActivity).changeFragment(typeSelection?.let { SelectionsMorphFragment().newInstance(mTaskNumber, it) })
    }

    override fun onSelectionStart(mTaskNumber: String, typeSelection: Byte) {
        (activity as MainActivity).changeFragment(SelectionsMorphFragment().newInstance(mTaskNumber, typeSelection))
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val printer = parent?.getItemAtPosition(position) as MyPrinter
        txt_path.text = printer.host
    }

    override fun onErrorGetBarcode(orderData: String, orderID: String, orderType: String) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.error_load_barcode))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        presenter.getBarcode(orderData, orderID, orderType)
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }


}