package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.ordercard;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.ProductParam;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;

/**
 * Created by GrinfeldRa
 */
public interface OrderCardView extends BaseView {

    @StateStrategyType(SkipStrategy.class)
    void onInitFieldsMeasure(String required, String accepted);

    @StateStrategyType(SkipStrategy.class)
    void onDeleteCountProductInOrder();

    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadParamPallet(String response, String barcode);

    @StateStrategyType(SkipStrategy.class)
    void onCompleteReserveOrder();

    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadParamGoods(ProductParam productParam, ScannerData scannerData);
}
