package com.madlab.wms.grinfeld.roman.utils;

import android.view.View;

/**
 * Created by GrinfeldRa
 */
public interface KeyEventListener extends View.OnKeyListener {

    boolean isVisible();

    View getView();

}
