package com.madlab.wms.grinfeld.roman.dao

import androidx.room.*
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.RouteBoxes

/**
 * Created by grinfeldra
 */
@Dao
interface CheckingBoxesDao {

    @Query("SELECT * FROM RouteBoxes where `docNum` =:numRoute")
    fun getRoute(numRoute: String) : RouteBoxes

    @Insert
    fun insert(route: RouteBoxes)

    @Update
    fun update(route: RouteBoxes)

    @Delete
    fun delete(route: RouteBoxes)
}