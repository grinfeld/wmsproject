package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes.checking_enter_count

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBoxesRoute
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.RouteBoxes
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import kotlinx.android.synthetic.main.checking_boxes_enter_count_fragment.*
import java.text.NumberFormat

/**
 * Created by grinfeldra
 */
const val KEY_NUM_DOC = "num_doc"
const val KEY_ID_EUZ_BOXES = "euz_boxes"

class CheckingEnterCountFragment : MvpAppCompatFragment(), TextWatcher, KeyEventListener {

    lateinit var euzBoxesRoute: EuzBoxesRoute
    lateinit var route: RouteBoxes
    lateinit var numberFormat: NumberFormat

    fun newInstance(numDoc: String, idEuzBoxesRoute: Long) = CheckingEnterCountFragment().apply {
        arguments = Bundle().apply {
            putString(KEY_NUM_DOC, numDoc)
            putLong(KEY_ID_EUZ_BOXES, idEuzBoxesRoute)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.selection_check_title))
        return inflater.inflate(R.layout.checking_boxes_enter_count_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val id = arguments?.getLong(KEY_ID_EUZ_BOXES)!!
        val numDoc = arguments?.getString(KEY_NUM_DOC)!!
        val checkingBoxesDao = App.getDatabase().checkingBoxesDao()
        route = checkingBoxesDao.getRoute(numDoc)
        euzBoxesRoute = route.euzBoxesRoutes.find { euzBoxesRoute -> euzBoxesRoute.id == id }!!
        et_code.setText(euzBoxesRoute.codeGoods)
        et_name.setText(euzBoxesRoute.nameGoods)
        numberFormat = NumberFormat.getInstance()
        et_enter.setText(getString(R.string.check_boxes_enter_count_format,
                numberFormat.format(euzBoxesRoute.enterCount), euzBoxesRoute.baseEi))
        et_count.addTextChangedListener(this)
        et_count.setOnKeyListener(this)
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    val alertDialog = context?.let {
                        AlertDialog.Builder(it)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle(getString(R.string.confirmation))
                                .setMessage(getString(R.string.cancel_changes))
                                .setPositiveButton(getString(R.string.ok)) { _, _ ->
                                    fragmentManager?.popBackStack()
                                }
                                .setNegativeButton(getString(R.string.cancel), null)
                                .create()
                    }
                    if (!alertDialog?.isShowing!!) {
                        alertDialog.show()
                    }
                }
                true
            }
            KeyEvent.KEYCODE_ENTER -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    val enterCount = et_count.text.toString().toDouble()
                    val count = euzBoxesRoute.count
                    when {
                        enterCount == count -> {
                            save(enterCount)
                        }
                        enterCount < count -> {
                            saveEuzBoxesRouteDialog(getString(R.string.enter_count_less_than_count,
                                    numberFormat.format(count - enterCount), euzBoxesRoute.baseEi), enterCount)
                        }
                        else -> {
                            saveEuzBoxesRouteDialog(getString(R.string.enter_count_more_than_count,
                                    numberFormat.format(enterCount - count), euzBoxesRoute.baseEi), enterCount)
                        }
                    }
                }
                true
            }
            else -> false
        }
    }

    private fun saveEuzBoxesRouteDialog(title: String, enterCount: Double) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.enter_count_error))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        saveDialog(title, enterCount)
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    private fun saveDialog(title: String, enterCount: Double) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(title)
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        save(enterCount)
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }


    private fun save(enterCount: Double) {
        val dao = App.getDatabase().checkingBoxesDao()
        val indexOf = route.euzBoxesRoutes.indexOf(euzBoxesRoute)
        euzBoxesRoute.checked = true
        euzBoxesRoute.enterCount = enterCount
        route.euzBoxesRoutes[indexOf] = euzBoxesRoute
        dao.update(route)
        fragmentManager?.popBackStack()
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        et_enter.setText(getString(R.string.check_boxes_enter_count_format, if (s.isNullOrEmpty()) "0" else s, euzBoxesRoute.baseEi))
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }
}