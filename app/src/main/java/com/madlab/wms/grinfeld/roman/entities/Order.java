package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.wms.grinfeld.roman.adapter.AbstractData;
import com.madlab.wms.grinfeld.roman.utils.DateParser;

public class Order extends AbstractData implements Parcelable {

    private static final String TAG = Order.class.getSimpleName();

    private String name;
    private long date;
    private String client;
    private String status;
    private String holder;
    private int countLines;
    private double weight;
    private double amount;
    private String shortName;
    private String numDoc;
    private String dateDoc;


    public Order(String name, long date, String client, String status, String holder, int countLines, double weight, double amount, String shortName) {
        this.name = name;
        this.date = date;
        this.client = client;
        this.status = status;
        this.holder = holder;
        this.countLines = countLines;
        this.weight = weight;
        this.amount = amount;
        this.shortName = shortName;
    }

    protected Order(Parcel in) {
        name = in.readString();
        date = in.readLong();
        client = in.readString();
        status = in.readString();
        holder = in.readString();
        countLines = in.readInt();
        weight = in.readDouble();
        amount = in.readDouble();
        shortName = in.readString();
        numDoc = in.readString();
        dateDoc = in.readString();
    }

    public String getDateDoc() {
        return dateDoc;
    }

    public void setDateDoc(String dateDoc) {
        this.dateDoc = dateDoc;
    }



    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public long getDate() {
        return date;
    }

    public String getClient() {
        return client;
    }

    public String getStatus() {
        return status;
    }

    public String getHolder() {
        return holder;
    }

    public int getCountLines() {
        return countLines;
    }

    public double getWeight() {
        return weight;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return getClient();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeLong(date);
        dest.writeString(client);
        dest.writeString(status);
        dest.writeString(holder);
        dest.writeInt(countLines);
        dest.writeDouble(weight);
        dest.writeDouble(amount);
        dest.writeString(shortName);
        dest.writeString(numDoc);
        dest.writeString(dateDoc);
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    @Override
    public String getLeftText() {
        return getName();
    }

    @Override
    public String getRightText() {
        return DateParser.getDateFromLong(getDate());
    }
}
