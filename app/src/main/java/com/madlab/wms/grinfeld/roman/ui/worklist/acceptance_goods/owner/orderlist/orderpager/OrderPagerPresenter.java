package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.utils.DateParser;

import io.reactivex.android.schedulers.AndroidSchedulers;


/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class OrderPagerPresenter extends BasePresenter<OrderPagerView> {


    void loadGoods(Order order) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_PRODUCTS(order.getName(), DateParser.getDateToStringFromLongToRequest(order.getDate())))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                            if (isSuccess(response)) {
                                if (!response.contains("OK")) {
                                    getViewState().isAllGoodsAcceptedInOrder(false);
                                } else {
                                    getViewState().isAllGoodsAcceptedInOrder(true);
                                }
                            }
                        },
                        throwable -> getViewState().showError(throwable.getMessage())));
    }

    void checkOrder(Order order, int isClose, int orderType) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_CHECK_ORDER_DOC(order.getNumDoc(), order.getDateDoc(), orderType, isClose))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        String message = response.substring(3).replaceAll(";", "");
                        getViewState().onCompleteCheckOrder(message, order, isClose);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

    void closeOrder(Order order, int isClose, int orderType) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_CLOSE_ORDER_DOC(order.getNumDoc(), order.getDateDoc(), orderType, isClose))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCloseOrder(response.split(";")[1]);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }
}
