package com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell

/**
 * Created by grinfeldra
 */
interface PlacementDefectiveView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteBlockCell(cell: String, cellTxt: String)

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteGetInfoEuz(result: List<PlacementCell>)

}