package com.madlab.wms.grinfeld.roman.ui.authorization;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageButton;

import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.IRFACItemClick;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.ui.MainActivity;
import com.madlab.wms.grinfeld.roman.ui.settings.SettingsFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.WorkListFragment;
import com.madlab.wms.grinfeld.roman.utils.Preference;
import com.madlab.wms.grinfeld.roman.utils.Utils;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by GrinfeldRa
 */
public class AuthorizationFragment extends MvpAppCompatFragment implements AuthorizationView, View.OnClickListener, TextView.OnEditorActionListener {

    private ProgressOwner progressOwner;
    private EditText etLogin;
    private Button btnLogin;
    private Context context;
    private AppCompatImageButton img_btn_settings, img_btn_update;
    private TextView txt_version_app;

    @InjectPresenter
    AuthorizationPresenter presenter;

    @ProvidePresenter
    AuthorizationPresenter providePresenter() {
        return new AuthorizationPresenter();
    }

    public static Fragment newInstance() {
        return new AuthorizationFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ProgressOwner) {
            progressOwner = ((ProgressOwner) context);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            ((BaseFragmentActivity) getActivity()).setActionBarTitle(getString(R.string.str_auth));
        }
        return inflater.inflate(R.layout.auth_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        etLogin = view.findViewById(R.id.edtxtLogin);
        btnLogin = view.findViewById(R.id.btnLogin);
        img_btn_settings = view.findViewById(R.id.img_btn_settings);
        img_btn_update = view.findViewById(R.id.img_btn_update);
        txt_version_app = view.findViewById(R.id.txt_version_app);
        etLogin.setOnEditorActionListener(this);
        btnLogin.setOnClickListener(this);
        img_btn_update.setOnClickListener(this);
        img_btn_settings.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        etLogin.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_login), null);
        ColorStateList tint = ContextCompat.getColorStateList(context, R.color.colorAccent);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            img_btn_settings.setBackgroundTintList(tint);
            img_btn_update.setBackgroundTintList(tint);
        } else {
            ViewCompat.setBackgroundTintList(img_btn_settings, tint);
            ViewCompat.setBackgroundTintList(img_btn_update, tint);
        }
        txt_version_app.setText(getString(R.string.version_app, Utils.getVersionName()));
    }




    @Override
    public void onDetach() {
        progressOwner = null;
        super.onDetach();
    }

    private void onRefreshData() {
        presenter.authAndGetWorkList();
    }

    @Override
    public void openWorkListFragment(String works) {
        if (getActivity() != null)
            ((MainActivity) getActivity()).changeFragment(WorkListFragment.newInstance(works));
    }

    @Override
    public void onUpdateApk(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onNoUpdateRequired() {
        Toast.makeText(context, R.string.no_update_required, Toast.LENGTH_LONG).show();
    }


    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        btnLogin.setEnabled(false);
        img_btn_update.setEnabled(false);
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        btnLogin.setEnabled(true);
        img_btn_update.setEnabled(true);
        progressOwner.setProgressState(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_btn_settings:
                if (getActivity() != null)
                    ((MainActivity) getActivity()).changeFragment(new SettingsFragment());
                break;
            case R.id.btnLogin:
                if (etLogin.getText().length() != 4) {
                    etLogin.setError(getString(R.string.Incorrectly_code));
                } else {
                    if (TextUtils.isEmpty(Preference.getServer()) || Preference.getPort() == 0) {
                        etLogin.setError(getString(R.string.Incorrectly_settings));
                    } else {
                        Preference.setLoginCode(etLogin.getText().toString());
                        onRefreshData();
                    }
                }
                break;
            case R.id.img_btn_update:
                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(R.string.confirmation)
                        .setMessage("Обновить версию программы?")
                        .setNegativeButton("Нет", (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Обновить", (dialog, which) -> {
                            presenter.updateApk();
                        })
                        .create();
                if (!alertDialog.isShowing()) {
                    alertDialog.show();
                }
                break;
        }

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onRefreshData();
            return true;
        }
        return false;
    }

}
