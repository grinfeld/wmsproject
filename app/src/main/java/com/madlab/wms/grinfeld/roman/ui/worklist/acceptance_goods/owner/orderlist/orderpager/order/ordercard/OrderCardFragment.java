package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.ordercard;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.GoodsInDoc;
import com.madlab.wms.grinfeld.roman.entities.Measure;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.entities.ProductParam;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner;
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner;
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent;
import com.madlab.wms.grinfeld.roman.utils.DateParser;
import com.madlab.wms.grinfeld.roman.utils.DateValidator;
import com.madlab.wms.grinfeld.roman.utils.FocusedTextWatcher;
import com.madlab.wms.grinfeld.roman.utils.Preference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by GrinfeldRa
 */
public class OrderCardFragment extends MvpAppCompatFragment implements OrderCardView,
        TextWatcher, AdapterView.OnItemSelectedListener, IDateAutocomplete, KeyEventListener, IOnScannerEvent {

    private static final String KEY_SCANNER_DATA = "key_scanner_data";
    private static final String KEY_ORDER = "key_order";
    private static final String KEY_POSITION = "key_type";
    private static final String KEY_TYPE_ORDER = "key_type_order";
    private static final String KEY_DATA_IN_ORDER = "key_data_in_order";
    private static final String KEY_RESPONSE = "key_resp";
    private static final String TAG = "#OrderCard";

    private Context context;
    private ProgressOwner progressOwner;
    private EditText et_count, et_euz, et_name, et_number_pallet,
            et_prod_day, et_prod_mouth, et_prod_year,
            et_expiration_day, et_expiration_mouth, et_expiration_year,
            et_number_loot;
    private TextView txt_required, txt_take, txt_count_pack;
    private Spinner spinner_measure;
    private ProductParam productParam;
    private Order order;
    private ScannerData scannerData;
    private LinkedHashMap<String, Double> measure;
    private AbstractScanner scanner;
    private double total_count = 0;
    private int count_pack = 0;
    private ArrayList<GoodsInDoc> goodsInDocs;
    private int typeOrder = 0;

    @InjectPresenter
    OrderCardPresenter presenter;

    @ProvidePresenter
    OrderCardPresenter providePresenter() {
        return new OrderCardPresenter();
    }

    public static OrderCardFragment newInstance(ProductParam productParam, ScannerData scannerData,
                                                Order order, int position, ArrayList<GoodsInDoc> goodsInDocs,
                                                int typeOrder) {
        Bundle args = new Bundle();
        if (productParam != null) {
            args.putParcelable(KEY_RESPONSE, productParam);
        }
        args.putInt(KEY_POSITION, position);
        args.putInt(KEY_TYPE_ORDER, typeOrder);
        args.putParcelable(KEY_SCANNER_DATA, scannerData);
        args.putParcelable(KEY_ORDER, order);
        args.putParcelableArrayList(KEY_DATA_IN_ORDER, goodsInDocs);
        OrderCardFragment fragment = new OrderCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ProgressOwner) {
            progressOwner = ((ProgressOwner) context);
        }
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).addKeyEventHandler(this);
        }
    }

    @Override
    public void onDetach() {
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).removeKeyEventHandler(this);
        }
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            ((BaseFragmentActivity) getActivity()).setActionBarTitle(getString(R.string.enter_euz));
        }
        return inflater.inflate(R.layout.order_card_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            typeOrder = getArguments().getInt(KEY_TYPE_ORDER);
            productParam = getArguments().getParcelable(KEY_RESPONSE);
            order = getArguments().getParcelable(KEY_ORDER);
            goodsInDocs = getArguments().getParcelableArrayList(KEY_DATA_IN_ORDER);
            scannerData = getArguments().getParcelable(KEY_SCANNER_DATA);
            if (getArguments().getInt(KEY_POSITION, Command.TYPE_GET) != Command.TYPE_GET) {
                initGoodInOrder();
            }
            bindView(view);
            initDataField();
        }
    }

    private void bindView(View view) {
        et_number_loot = view.findViewById(R.id.et_number_loot);
        et_number_pallet = view.findViewById(R.id.et_cell);
        et_euz = view.findViewById(R.id.et_to_cell);
        txt_count_pack = view.findViewById(R.id.txt_count_pack);
        et_name = view.findViewById(R.id.et_name);
        txt_take = view.findViewById(R.id.txt_take);
        txt_required = view.findViewById(R.id.txt_required);
        spinner_measure = view.findViewById(R.id.spinner_measure);
        et_count = view.findViewById(R.id.et_count);
        et_prod_day = view.findViewById(R.id.et_prod_day);
        et_prod_mouth = view.findViewById(R.id.et_prod_mouth);
        et_prod_year = view.findViewById(R.id.et_prod_year);
        et_expiration_day = view.findViewById(R.id.et_expiration_day);
        et_expiration_mouth = view.findViewById(R.id.et_expiration_mouth);
        et_expiration_year = view.findViewById(R.id.et_expiration_year);
        et_prod_day.addTextChangedListener(new FocusedTextWatcher(et_prod_day, this));
        et_prod_mouth.addTextChangedListener(new FocusedTextWatcher(et_prod_mouth, this));
        et_prod_year.addTextChangedListener(new FocusedTextWatcher(et_prod_year, this));
        et_expiration_day.addTextChangedListener(new FocusedTextWatcher(et_expiration_day, this));
        et_expiration_mouth.addTextChangedListener(new FocusedTextWatcher(et_expiration_mouth, this));
        et_expiration_year.addTextChangedListener(new FocusedTextWatcher(et_expiration_year, this));
        spinner_measure.setOnItemSelectedListener(this);
    }

    private void initDataField() {
        if (productParam != null) {
            measure = Measure.getMapMeasure(productParam);
            et_euz.setText(productParam.getCode());
            et_name.setText(productParam.getName());
        }
        et_number_pallet.setText(scannerData.getTe() == null ? "" : scannerData.getTe());
        et_number_loot.setText(scannerData.getLootNumber() == null ? "" : scannerData.getLootNumber());

        if (scannerData.getProduction_date() != null && scannerData.getProduction_date().getTime() > 0) {
            String[] production = DateParser.getDateFromLong(scannerData.getProduction_date().getTime()).split("[.]");
            et_prod_day.setText(production[0]);
            et_prod_mouth.setText(production[1]);
            et_prod_year.setText(production[2]);
        }
        if (scannerData.getExpiration_date() != null && scannerData.getExpiration_date().getTime() > 0) {
            String[] expiration = DateParser.getDateFromLong(scannerData.getExpiration_date().getTime()).split("[.]");
            et_expiration_day.setText(expiration[0]);
            et_expiration_mouth.setText(expiration[1]);
            et_expiration_year.setText(expiration[2]);

        }
        if (measure != null) {
            List<String> list = new ArrayList<>();
            for (Map.Entry<String, Double> entry : measure.entrySet()) {
                list.add(entry.getKey());
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
            spinner_measure.setAdapter(dataAdapter);
            et_count.setText(scannerData.getCount() == null ? "" : scannerData.getCount());
        }
        et_count.addTextChangedListener(this);
        et_count.setOnKeyListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        scanner = new FactoryScanner().getScanner(context, this);
        scanner.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        scanner.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        scanner.onStop();
    }


    @Override
    public void showError(String error) {
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).addKeyEventHandler(this);
            et_count.setOnKeyListener(this);
        }
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle(getString(R.string.confirmation))
                            .setMessage(getString(R.string.exit_not_save))
                            .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                                if (getActivity() != null) {
                                    getActivity().onBackPressed();
                                }
                            })
                            .setNegativeButton(getString(R.string.cancel), null);
                    alertDialog.create();
                    alertDialog.show();
                    return true;
                }
                return false;
            case KeyEvent.KEYCODE_E:
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (measure != null) {
                        int id = spinner_measure.getSelectedItemPosition();
                        if (id == measure.size() - 1) {
                            spinner_measure.setSelection(0);
                        } else {
                            spinner_measure.setSelection(id + 1);
                        }
                    }
                    return true;
                }
                return false;
            case KeyEvent.KEYCODE_DEL:
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (productParam != null && v.getId() == R.id.et_count && ((TextView) v).getText().length() == 0) {
                        AlertDialog alertDialog = new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle(getString(R.string.confirmation))
                                .setMessage(getString(R.string.delete_goods))
                                .setPositiveButton(getString(R.string.yes), (dialog, which) -> presenter.onDeleteCountProductInOrder())
                                .setNegativeButton(getString(R.string.no), null)
                                .create();
                        if (!alertDialog.isShowing()) {
                            alertDialog.show();
                        }

                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            case KeyEvent.KEYCODE_ENTER:
                if (productParam != null) {
                    int position = spinner_measure.getSelectedItemPosition();
                    String nameMeasure = (String) spinner_measure.getAdapter().getItem(position);
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        if (!TextUtils.isEmpty(et_count.getText())) {
                            if (nameMeasure.equals("кг")) {
                                count_pack = count_pack + 1;
                                total_count = total_count + Double.parseDouble(et_count.getText().toString());
                                txt_count_pack.setText(String.valueOf(count_pack));
                                et_count.setText("");
                            } else {
                                Double quantum = measure.get(nameMeasure);
                                total_count = total_count + (Double.parseDouble(et_count.getText().toString()) * (quantum == null ? 1 : quantum));
                                et_count.setText("");
                            }
                        } else {
                            if (isNotEmptyFields()) {
                                if (isDateFormatCorrect()) {
                                    ((BaseFragmentActivity) context).removeKeyEventHandler(this);
                                    et_count.setOnKeyListener(null);
                                    String number_pallet = typeOrder == Command.TYPE_ORDER ? et_number_pallet.getText().toString() : "";
                                    String euz = et_euz.getText().toString();
                                    String dateProd = getDateProd();
                                    String dateExp = getDateExpiration();
                                    String numLoot = et_number_loot.getText().toString();
                                    String sCount = et_count.getText().toString();
                                    Double quantum = measure.get(nameMeasure);
                                    double mCount = Double.parseDouble(sCount.isEmpty() ? "0" : sCount) * (quantum == null ? 1 : quantum);
                                    if (total_count != 0) {
                                        mCount = mCount + total_count;
                                    }
                                    String count = String.valueOf(mCount);
                                    presenter.reserveOrder(typeOrder, order.getNumDoc(), order.getDateDoc(), euz, count, number_pallet, "", dateProd, dateExp, numLoot, getArguments() != null ? getArguments().getInt(KEY_POSITION) : 0);
                                } else {
                                    Toast.makeText(context, getString(R.string.invalid_date_format), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(context, getString(R.string.fill_in_all_fields), Toast.LENGTH_SHORT).show();
                            }
                            return false;
                        }
                    }
                }
                return true;
            default:
                return false;
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (productParam != null) {
            int position = spinner_measure.getSelectedItemPosition();
            String nameMeasure = (String) spinner_measure.getAdapter().getItem(position);
            Double quantum = measure.get(nameMeasure);
            quantum = quantum == null ? 1 : quantum;
            double accepted = (s.toString().isEmpty() ? 0 : Double.parseDouble(s.toString()));// * quantum;
            productParam.setAccepted(accepted);
            presenter.onInitFieldsMeasure(productParam, quantum, total_count);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onInitFieldsMeasure(String required, String accepted) {
        txt_required.setText(getString(R.string.required, required));
        txt_take.setText(getString(R.string.take, accepted));
    }


    @Override
    public void onDeleteCountProductInOrder() {
        if (productParam != null) {
            total_count = 0;
            count_pack = 0;
            txt_count_pack.setText("");
            int position = spinner_measure.getSelectedItemPosition();
            String nameMeasure = (String) spinner_measure.getAdapter().getItem(position);
            Double quantum = measure.get(nameMeasure);
            quantum = quantum == null ? 1 : quantum;
            presenter.onInitFieldsMeasure(productParam, quantum, total_count);
        }
    }

    @Override
    public void onCompleteLoadParamPallet(String response, String barcode) {
        if (response.contains("OK")) {
            et_number_pallet.setText(barcode);
        } else {
            try {
                String[] error = response.split(";");
                Toast.makeText(context, error[1], Toast.LENGTH_LONG).show();
            } catch (ArrayIndexOutOfBoundsException e) {
                showError(e.getMessage());
            }
        }
    }

    @Override
    public void onCompleteLoadParamGoods(ProductParam productParam, ScannerData scannerData) {
        String te = this.scannerData.getTe();
        scannerData.setTe(te);
        this.scannerData = scannerData;
        this.productParam = productParam;
        initGoodInOrder();
        initDataField();
    }


    @Override
    public void onCompleteReserveOrder() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }


    @Override
    public void onDataScanner(ScannerData scannerData) {
        switch (typeOrder) {
            case Command.TYPE_ORDER:
                if (scannerData.getTe() != null) {
                    if (Preference.getOrderConditionMode() == Command.TYPE_DEFECTIVE) {
                        Toast.makeText(context, getString(R.string.te_no_need), Toast.LENGTH_SHORT).show();
                    } else {
                        presenter.getParamOnPallet(order.getNumDoc(), order.getDateDoc(), scannerData.getTe());
                    }
                } else {
                    presenter.loadParamFromScanCode(order, scannerData, Command.TYPE_GET);
                }
                break;
            case Command.TYPE_RETURN:
                if (scannerData != null && scannerData.getBarcode() != null) {
                    presenter.loadParamFromScanCode(order, scannerData, Command.TYPE_GET);
                } else {
                    showError(getString(R.string.scan_euz));
                }
                break;
        }
    }

    @Override
    public void onStatusUpdateScanner(String scanStatus) {

    }

    @Override
    public void onErrorScanner(String error) {

    }

    private boolean isNotEmptyFields() {
        boolean b = false;
        if (typeOrder == Command.TYPE_RETURN) {
            b = true;
        } else if (Preference.getOrderConditionMode() == Command.TYPE_DEFECTIVE) {
            b = true;
        } else if (!TextUtils.isEmpty(et_number_pallet.getText())) {
            b = true;
        }
        return b && (!TextUtils.isEmpty(et_count.getText()) || total_count != 0) &&
                !TextUtils.isEmpty(et_prod_day.getText()) &&
                !TextUtils.isEmpty(et_prod_mouth.getText()) &&
                !TextUtils.isEmpty(et_prod_year.getText()) &&
                !TextUtils.isEmpty(et_expiration_day.getText()) &&
                !TextUtils.isEmpty(et_expiration_mouth.getText()) &&
                !TextUtils.isEmpty(et_expiration_year.getText());
    }

    private boolean isDateFormatCorrect() {
        DateValidator dateValidator = new DateValidator();
        return dateValidator.validate(getDateProd()) && dateValidator.validate(getDateExpiration());
    }


    private String getDateProd() {
        String prod_day = et_prod_day.getText().toString();
        String prod_mouth = et_prod_mouth.getText().toString();
        String prod_year = et_prod_year.getText().toString();
        return String.format("%s%s%s", prod_year, prod_mouth, prod_day);
    }

    private String getDateExpiration() {
        String expiration_day = et_expiration_day.getText().toString();
        String expiration_mouth = et_expiration_mouth.getText().toString();
        String expiration_year = et_expiration_year.getText().toString();
        return String.format("%s%s%s", expiration_year, expiration_mouth, expiration_day);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (productParam != null) {
            String nameMeasure = (String) parent.getItemAtPosition(position);
            Double quantum = measure.get(nameMeasure);
            presenter.onInitFieldsMeasure(productParam, quantum == null ? 1 : quantum, total_count);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDateAutocomplete() {
        try {
            if (productParam != null) {
                String dateProd = getDateProd();
                if (new DateValidator().validate(dateProd)) {
                    int shelfLife = productParam.getShelfLife();
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(format.parse(dateProd));
                    int shelfLifeType = productParam.getShelfLifeType();
                    switch (shelfLifeType) {
                        case 1:
                            calendar.add(Calendar.YEAR, shelfLife);
                            break;
                        case 2:
                            calendar.add(Calendar.MONTH, shelfLife);
                            break;
                        case 3:
                            calendar.add(Calendar.DATE, shelfLife);
                            break;
                    }
                    dateProd = format.format(calendar.getTime());
                    et_expiration_day.setText(dateProd.substring(6, 8));
                    et_expiration_mouth.setText(dateProd.substring(4, 6));
                    et_expiration_year.setText(dateProd.substring(0, 4));
                }
            }
        } catch (ParseException e) {
            Log.d(TAG, getString(R.string.invalid_date_format));
        }
    }

    private void initGoodInOrder() {
        for (GoodsInDoc goodsInDoc : goodsInDocs) {
            if (productParam != null) {
                if (goodsInDoc.getCode().equals(productParam.getCode())) {
                    double v = Double.parseDouble(goodsInDoc.getCount());
                    double d = productParam.getRequired() - v;
                    productParam.setAccepted(v);
                    //productParam.setRequired(d);
                    break;
                }
            }
        }
    }

}