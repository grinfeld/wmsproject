package com.madlab.wms.grinfeld.roman.ui.authorization;


import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.SMBClient;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.api.Command;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by GrinfeldRa
 */

@InjectViewState
public class AuthorizationPresenter extends BasePresenter<AuthorizationView> {

    void authAndGetWorkList() {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_AUTHORIZATION())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                            if (isSuccess(response)) {
                                getViewState().openWorkListFragment(response);
                            }
                        },
                        throwable -> getViewState().showError(throwable.getMessage() == null ? throwable.toString() : throwable.getMessage())
                ));
    }


    void updateApk() {
        mCompositeDisposable.add(new SMBClient().updateApk
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(data -> {
                            if (!data.equals(SMBClient.ACTUAL_VERSION)) {
                                getViewState().onUpdateApk(Uri.parse(data));
                            } else {
                                getViewState().onNoUpdateRequired();
                            }
                        },
                        throwable -> getViewState().showError(throwable.getMessage() == null ? throwable.toString() : throwable.getMessage())
                ));
    }

}
