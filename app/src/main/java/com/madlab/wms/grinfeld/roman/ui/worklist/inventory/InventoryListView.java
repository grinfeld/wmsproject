package com.madlab.wms.grinfeld.roman.ui.worklist.inventory;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.InventoryCell;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRa
 */
public interface InventoryListView extends BaseView {

    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadInventoryCell(List<InventoryCell> inventoryCells);
    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadProductInInventoryCell(ArrayList<InventoryCellProduct> list, String createDate, String inventoryNum);
    @StateStrategyType(SkipStrategy.class)
    void showDialog(String barcode);
    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadDataFromCellBarcode(String numberInventory, String createDate);
}
