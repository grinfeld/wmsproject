package com.madlab.wms.grinfeld.roman.ui.worklist.placement_transit_te

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.PREFIX_CELL
import kotlinx.android.synthetic.main.placement_transit_fragment.*

/**
 * Created by grinfeldra
 */
class PlacementTransitFragment : MvpAppCompatFragment(), PlacementTransitView, KeyEventListener, IOnScannerEvent {

    @InjectPresenter
    lateinit var presenter: PlacementTransitPresenter

    @ProvidePresenter
    fun providePresenter() = PlacementTransitPresenter()

    private var scanner: AbstractScanner? = null
    private lateinit var progressOwner: ProgressOwner

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.placement_transit))
        return inflater.inflate(R.layout.placement_transit_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_info.text = getString(R.string.scan_te_for_placement)
    }


    override fun onCompleteReservePlacementTransit() {
        showError(getString(R.string.reserve_placement_complete))
        et_te.setText("")
        et_cell.setText("")
        txt_info.text = getString(R.string.scan_te_for_placement)
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (et_te.text.isEmpty()) {
            if (goodsCard?.te != null) {
                et_te.setText(goodsCard.te)
                txt_info.text = getString(R.string.scan_сel_for_placement)
            } else {
                showError(getString(R.string.error_scan_te))
            }
        } else {
            if (goodsCard?.barcode != null && goodsCard.barcode.substring(0, 2) == PREFIX_CELL) {
                et_cell.setText(goodsCard.barcode.substring(2))
                showPlacementDialog()
            }
        }
    }

    private fun showPlacementDialog() {
        val te = et_te.text.toString()
        val cell = et_cell.text.toString()
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.reserve_placement, te, cell))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        presenter.reservePlacementTransit(te, cell)
                    }
                    .setNegativeButton(getString(R.string.cancel)) { _, _ ->
                        et_te.setText("")
                        et_cell.setText("")
                        txt_info.text = getString(R.string.scan_te_for_placement)
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if ((keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE)
                && event?.action == KeyEvent.ACTION_UP) {
            val alertDialog = context?.let {
                AlertDialog.Builder(it)
                        .setMessage(getString(R.string.placement_close))
                        .setPositiveButton(getString(R.string.ok)) { _, _ ->
                            fragmentManager?.popBackStack()
                        }
                        .setNegativeButton(getString(R.string.cancel), null)
                        .create()
            }
            if (!alertDialog?.isShowing!!) {
                alertDialog.show()
            }
            return true
        }
        return false
    }


    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }
}