package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product;

import android.util.Pair;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;
import com.madlab.wms.grinfeld.roman.entities.Measure;
import com.madlab.wms.grinfeld.roman.entities.ProductParam;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;


/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class InventoryProductPresenter extends BasePresenter<InventoryProductView> {


    void initFieldMeasure(InventoryCellProduct product, InventoryCellProductViewModel model) {
        Measure measure = Measure.getSelected(product.getMeasureList());
        List<InventoryCellProduct> list = getListInventorySeries(product.getCodeGoods(), model);
        Pair<Double, Double> countFactAndDefective = getCountFactAndDefective(list);
        if (countFactAndDefective != null && measure != null) {
            double restCount = 0;
            double restDefective = 0;
            if (measure.getKoef() != 1) {
                restCount = countFactAndDefective.first % measure.getKoef();
                restDefective = countFactAndDefective.second % measure.getKoef();
            }
            NumberFormat numberFormat = NumberFormat.getInstance();
            String countText = numberFormat.format((countFactAndDefective.first - restCount) / measure.getKoef()) + " "
                    + measure.getName() + (restCount == 0 ? "" : numberFormat.format(restCount) + " " + product.getMeasureInitName());
            String countDefective = numberFormat.format((countFactAndDefective.second - restDefective) / measure.getKoef()) + " "
                    + measure.getName() + (restDefective == 0 ? "" : numberFormat.format(restDefective) + " " + product.getMeasureInitName());
            getViewState().onInitFieldMeasure(countText, countDefective);
        }

    }


    void reserveCellInventory(String number_inventory, String create_date,
                              Map<String, List<InventoryCellProduct>> map, int typeInventory) {
        ArrayList<InventoryCellProduct> data = new ArrayList<>();
        for (Map.Entry<String, List<InventoryCellProduct>> entry : map.entrySet()) {
            data.addAll(entry.getValue());
        }
        mCompositeDisposable.add(new WMSClient()
                .sendArray(Command.COM_RESERVE_INVENTORY_CELL(), Command.PARAM_RESERVE_INVENTORY_CELL(number_inventory, create_date, data, typeInventory))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                            if (isSuccess(response)) {
                                getViewState().onCompleteReserveCellInventory();
                            }
                        },
                        throwable -> getViewState().showError(throwable.getMessage())));

    }

    void getBailorProductFromBarcode(String barcode, InventoryCellProductViewModel model) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_PRODUCT_FROM_BARCODE(barcode))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        String[] mResult = response.substring(4).split(";");
                        String codeGoods = mResult[0];
                        String bailor = mResult[2];
                        List<InventoryCellProduct> listInventorySeries = getListInventorySeries(codeGoods, model);
                        if (listInventorySeries != null && listInventorySeries.size() > 0) {
                            getViewState().onScannerExistProduct(listInventorySeries);
                        } else {
                            getProduct(codeGoods, bailor);
                        }
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    private void getProduct(String codeGoods, String bailor) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_PARAM_ON_GOODS(2, bailor, "", Command.TYPE_EDIT, codeGoods))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        ProductParam productParam = new ProductParam(response);
                        if (productParam.getUnit().equals("кг")) {
                            productParam.setKoefEi1(0);
                            productParam.setEi1("");
                        }
                        productParam.setKoef(1);
                        InventoryCellProduct product = new InventoryCellProduct();
                        product.setPosition(0);
                        product.setBailorName(bailor);
                        product.setCodeGoods(productParam.getCode());
                        product.setGoodsName(productParam.getName());
                        product.setMeasureInitName(productParam.getUnit());
                        product.setMeasureMoreName(productParam.getEi1());
                        product.setMeasureQuant(productParam.getKoefEi1());
                        product.setShelfLifeCount(productParam.getShelfLife());
                        product.setShelfLifeType(productParam.getShelfLifeType());
                        product.setCodeSeries("");
                        product.setParty("");
                        product.setMeasureList(Measure.getMeasureList(productParam.getUnit(), productParam.getEi1(), productParam.getKoefEi1()));
                        getViewState().onCompleteGetProduct(product);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    private List<InventoryCellProduct> getListInventorySeries(String codeGoods, InventoryCellProductViewModel model) {
        Map<String, List<InventoryCellProduct>> value = model.getLiveData().getValue();
        if (value != null) {
            List<InventoryCellProduct> inventoryCellProducts = value.get(codeGoods);
            if (inventoryCellProducts != null) {
                return inventoryCellProducts;
            }
        }
        return null;
    }

    private Pair<Double, Double> getCountFactAndDefective(List<InventoryCellProduct> list) {
        double fact = 0, defective = 0;
        if (list != null) {
            for (InventoryCellProduct product : list) {
                fact += product.getCountFact();
                defective += product.getCountDefective();
            }
            return new Pair<>(fact, defective);
        }
        return null;
    }

}
