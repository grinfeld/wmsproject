package com.madlab.wms.grinfeld.roman.utils;

public class DataParser {

    public static int getCount(String s) {
            final StringBuilder sb = new StringBuilder(s.length());
            for(int i = 0; i < s.length(); i++){
                final char c = s.charAt(i);
                if(c > 47 && c < 58){
                    sb.append(c);
                }
            }
            return Integer.parseInt(sb.toString());
    }

    public static String[] getArray(String s, String c) {
        return s.split(c);
    }
}
