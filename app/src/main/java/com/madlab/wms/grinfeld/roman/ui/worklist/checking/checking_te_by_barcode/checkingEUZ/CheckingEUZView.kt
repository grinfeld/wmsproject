package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode.checkingEUZ

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.EUZRoute
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
interface CheckingEUZView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onCompleteReload(euzRoutes: ArrayList<EUZRoute>)
    @StateStrategyType(SkipStrategy::class)
    fun onCountChange(countBox: Int)
}