package com.madlab.wms.grinfeld.roman.entities;

import java.util.ArrayList;

public class Work {

    private static boolean priem = false;
    private static boolean vozvrat = false;
    private static boolean razmes_podpit = false;
    private static boolean otbor = false;
    private static boolean razmes_TE_v_Zgz = false;
    private static boolean otgruzka = false;
    private static boolean invent_Cell = false;
    private static boolean invent_EUZ = false;
    private static boolean upakovka = false;
    private static boolean razm_Voz = false;
    private static boolean razmes_Brak = false;
    private static boolean goodsList = false;
    private static boolean calculator = false;
    private static boolean arrangement = false;
    private static boolean takeTransitTE = false;
    private static boolean shippingInBox = false;


    private static final Work admission = new Work("Прием", 0);
    private static final Work refund = new Work("Возврат", 1);
    private static final Work accommodation = new Work("Размещение", 2);
    private static final Work accommodationAndRecharge = new Work("Размещение и подпитка", 3);
    private static final Work selection = new Work("Отбор", 4);
    private static final Work placingTransitTE = new Work("Размещ. транзитной ТЕ", 5);
    private static final Work shipment = new Work("Отгрузка", 6);
    private static final Work cellInventory = new Work("Инвентаризация ячейки", 7);
    private static final Work inventoryOfTheEYS = new Work("Инвентаризация ЕУЗ", 8);
    private static final Work checking = new Work("Проверка", 9);
    private static final Work placementOfReturns = new Work("Размещение возвратов", 10);
    private static final Work placementOfDefective = new Work("Размещение брака", 11);
    private static final Work referenceBookOfGoods = new Work("Справочник товаров", 12);
    private static final Work myCalculator = new Work("Калькулятор", 13);
    private static final Work myArrangement = new Work("Компоновка", 14);
    private static final Work acceptanceOfTransitTE = new Work("Приемка транзитных ТЕ", 15);

    private String name;
    private int position;

    private Work(String name, int position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ArrayList<Work> getList(String command) {
        ArrayList<Work> list = new ArrayList<>();
        String[] parseCommand = command.split(";");
        int length = parseCommand.length - 1;
        if (length < 14) goodsList = true;
        if (length < 15) calculator = true;
        if (length < 16) arrangement = true;
        if (length < 17) takeTransitTE = true;
        if (length < 18) shippingInBox = true;
        for (int i = 1; i < length; i++) {
            switch (i){
                case 1:
                    priem = parseCommand[i].equals("1");
                    break;
                case 2:
                    vozvrat = parseCommand[i].equals("1");
                    break;
                case 3:
                    razmes_podpit = parseCommand[i].equals("1");
                    break;
                case 4:
                    otbor = parseCommand[i].equals("1");
                    break;
                case 5:
                    razmes_TE_v_Zgz = parseCommand[i].equals("1");
                    break;
                case 6:
                    otgruzka = parseCommand[i].equals("1");
                    break;
                case 7:
                    invent_Cell = parseCommand[i].equals("1");
                    break;
                case 8:
                    invent_EUZ = parseCommand[i].equals("1");
                    break;
                case 9:
                    upakovka = parseCommand[i].equals("1");
                    break;
                case 10:
                    razm_Voz = parseCommand[i].equals("1");
                    break;
                case 11:
                    razmes_Brak = parseCommand[i].equals("1");
                    break;
                case 12:

                    break;
                case 13:
                    goodsList = parseCommand[i].equals("1");
                    break;
                case 14:
                    calculator = parseCommand[i].equals("1");
                    break;
                case 15:
                    arrangement = parseCommand[i].equals("1");
                    break;
                case 16:
                    takeTransitTE = parseCommand[i].equals("1");
                    break;
                case 17:
                    shippingInBox = parseCommand[i].equals("1");
                    break;
            }
        }
        if (priem){
            list.add(admission);
        }
        if (vozvrat){
            list.add(refund);
        }
        if (razmes_podpit){
            list.add(accommodation);
            list.add(accommodationAndRecharge);
        }
        if (otbor){
            list.add(selection);
        }
        if (razmes_TE_v_Zgz){
            list.add(placingTransitTE);
        }
        if (otgruzka){
            list.add(shipment);
        }
        if (invent_Cell){
            list.add(cellInventory);
        }
        if (invent_EUZ){
            list.add(inventoryOfTheEYS);
        }
        if (upakovka){
            list.add(checking);
        }
        if (razm_Voz){
            list.add(placementOfReturns);
        }
        if (razmes_Brak){
            list.add(placementOfDefective);
        }
        if (goodsList){
            list.add(referenceBookOfGoods);
        }
        if (calculator){
            list.add(myCalculator);
        }
        if (arrangement){
            list.add(myArrangement);
        }
        if (takeTransitTE){
            list.add(acceptanceOfTransitTE);
        }
        return list;
    }

}
