package com.madlab.wms.grinfeld.roman.entities.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBarcode
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBoxesRoute
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.EUZRoute
import com.madlab.wms.grinfeld.roman.entities.selection.PalletPrinted
import com.madlab.wms.grinfeld.roman.entities.selection.SelectionBarcode
import com.madlab.wms.grinfeld.roman.entities.selection.Weight

/**
 * Created by grinfeldra
 */
class DataConverter {

    @TypeConverter
    fun fromEuzBarcode(value: ArrayList<EuzBarcode>): String {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<EuzBarcode>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toEuzBarcode(value: String): ArrayList<EuzBarcode> {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<EuzBarcode>>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromEuzBoxesRoute(value: ArrayList<EuzBoxesRoute>): String {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<EuzBoxesRoute>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toEuzBoxesRoute(value: String): ArrayList<EuzBoxesRoute> {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<EuzBoxesRoute>>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromEUZRouteList(value: ArrayList<EUZRoute>): String {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<EUZRoute>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toEUZRouteList(value: String): ArrayList<EUZRoute> {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<EUZRoute>>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromWeightList(value: List<Weight>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Weight>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toWeightList(value: String): List<Weight> {
        val gson = Gson()
        val type = object : TypeToken<List<Weight>>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromSelectionBarcodeList(value: List<SelectionBarcode>): String {
        val gson = Gson()
        val type = object : TypeToken<List<SelectionBarcode>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toSelectionBarcodeList(value: String): List<SelectionBarcode> {
        val gson = Gson()
        val type = object : TypeToken<List<SelectionBarcode>>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromString(value: List<String>): String {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toString(value: String): List<String> {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromPalletPrintedList(value: List<PalletPrinted>): String {
        val gson = Gson()
        val type = object : TypeToken<List<PalletPrinted>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toPalletPrintedList(value: String): List<PalletPrinted> {
        val gson = Gson()
        val type = object : TypeToken<List<PalletPrinted>>() {}.type
        return gson.fromJson(value, type)
    }

}