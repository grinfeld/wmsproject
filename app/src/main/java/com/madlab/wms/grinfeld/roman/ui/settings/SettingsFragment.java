package com.madlab.wms.grinfeld.roman.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;

import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;

/**
 * Created by GrinfeldRa
 */
public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        if (getActivity() != null) {
            ((BaseFragmentActivity)getActivity()).setActionBarTitle(getString(R.string.settings));
        }
        addPreferencesFromResource(R.xml.pref);
        EditTextPreference server = findPreference(getString(R.string.pref_server_key));

        EditTextPreference port = findPreference(getString(R.string.pref_port_key));
        String strServer = server.getText();
        String strPort = port.getText();
        server.setSummary(strServer == null || strServer.trim().isEmpty() ? server.getSummary() : strServer);
        port.setSummary(strPort == null || strPort.trim().isEmpty() ? port.getSummary() : strPort);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getPreferenceManager().getSharedPreferences();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        EditTextPreference editTextPreference = findPreference(key);
        editTextPreference.setSummary(editTextPreference.getText().trim().isEmpty() ? editTextPreference.getSummary() : editTextPreference.getText());
    }
}
