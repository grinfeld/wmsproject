package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.EUZRoute
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.Route
import io.reactivex.android.schedulers.AndroidSchedulers


/**
 * Created by grinfeldra
 */
@InjectViewState
class RoutesPresenter : BasePresenter<RoutesView>() {

    fun getEuzFromRoute(route: Route) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_EUZ_FROM_ROUTE(route.docNum))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val euzRoutes = EUZRoute.parse(it)
                        val checkingDao = App.getDatabase().checkingDao()
                        val dbRoute: Route? = checkingDao.getRoute(route.docNum)
                        if (dbRoute == null) {
                            route.euzRoutes.addAll(euzRoutes)
                            checkingDao.insert(route)
                            viewState.onCompleteGetEuzFromRoute(route.docNum)
                        } else {
                            if (dbRoute.euzRoutes.size == 0) {
                                viewState.showError(App.getAppContext().getString(R.string.check_complete))
                            } else {
                                viewState.onCompleteGetEuzFromRoute(route.docNum)
                            }
                        }
                    }
                })
    }

    fun loadListRoute() {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_LIST_ROUTE())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val routes = Route.parseAnswer(it)
                                .sortedWith(compareBy(Route::name))
                        viewState.onCompleteLoadListRoute(routes)
                    }
                })
    }


}