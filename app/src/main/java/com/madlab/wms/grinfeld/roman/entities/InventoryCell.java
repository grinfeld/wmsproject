package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.AbstractData;
import com.madlab.wms.grinfeld.roman.common.App;

import java.util.ArrayList;
import java.util.List;

import kotlinx.android.parcel.Parcelize;

/**
 * Created by GrinfeldRa
 */
public class InventoryCell extends AbstractData implements Parcelable {

    //65 346;20190606;06.06.2019 11:04;2;3;330100120;3-01-001-2;
    private String number_inventory,
            create_date,
            start_date,
            number_cell,
            barcode;
    private int state_cell, type_cell;

    private InventoryCell(String number_inventory, String create_date, String start_date, String number_cell, int state_cell, int type_cell, String barcode) {
        this.number_inventory = number_inventory;
        this.create_date = create_date;
        this.start_date = start_date;
        this.number_cell = number_cell;
        this.state_cell = state_cell;
        this.type_cell = type_cell;
        this.barcode = barcode;
    }

    protected InventoryCell(Parcel in) {
        number_inventory = in.readString();
        create_date = in.readString();
        start_date = in.readString();
        number_cell = in.readString();
        barcode = in.readString();
        state_cell = in.readInt();
        type_cell = in.readInt();
    }

    public static final Creator<InventoryCell> CREATOR = new Creator<InventoryCell>() {
        @Override
        public InventoryCell createFromParcel(Parcel in) {
            return new InventoryCell(in);
        }

        @Override
        public InventoryCell[] newArray(int size) {
            return new InventoryCell[size];
        }
    };

    public static List<InventoryCell> getListInventoryCell(String response) {
        List<InventoryCell> result = new ArrayList<>();
        try {
            String[] temp = response.split("[|]");
            for (String s : temp) {
                String[] split = s.split(";");
                String number_inventory = split[0];
                String create_date = split[1];
                String start_date = split[2];
                int state_cell = Integer.parseInt(split[3]);
                int type_cell = Integer.parseInt(split[4]);
                String barcode = split[5];
                String number_cell = split[6];
                result.add(new InventoryCell(number_inventory, create_date, start_date, number_cell, state_cell, type_cell, barcode));
            }
        } catch (Exception e) {

        }
        return result;
    }

    public String getCreate_date() {
        return create_date;
    }

    public String getNumber_inventory() {
        return number_inventory;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getNumber_cell() {
        return number_cell;
    }

    public int getState_cell() {
        return state_cell;
    }

    public int getType_cell() {
        return type_cell;
    }

    public String getBarcode() {
        return barcode;
    }

    @Override
    public String getLeftText() {
        return getNumber_cell();
    }

    @Override
    public String getRightText() {
        switch (getState_cell()) {
            case 1:
                return App.getAppContext().getString(R.string.state_create);
            case 2:
                return App.getAppContext().getString(R.string.state_start);
            case 3:
                return App.getAppContext().getString(R.string.state_complete);
            default:
                return "unknown";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number_inventory);
        dest.writeString(create_date);
        dest.writeString(start_date);
        dest.writeString(number_cell);
        dest.writeString(barcode);
        dest.writeInt(state_cell);
        dest.writeInt(type_cell);
    }
}
