package com.madlab.wms.grinfeld.roman.ui.worklist.selection

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.selection.MyPrinter

/**
 * Created by grinfeldra
 */
interface SelectionView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onCompleteGetListPrinters(mTaskNumber: String, typeSelection: Byte)

    @StateStrategyType(SkipStrategy::class)
    fun onNotAvailable()

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteSendToPrint()

    @StateStrategyType(SkipStrategy::class)
    fun onSelectionStart(mTaskNumber: String, typeSelection: Byte)

    @StateStrategyType(SkipStrategy::class)
    fun onErrorGetBarcode(orderData: String, orderID: String, orderType: String)
}