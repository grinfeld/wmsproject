package com.madlab.wms.grinfeld.roman.client;

import android.util.Log;

import com.madlab.wms.grinfeld.roman.utils.Preference;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static com.madlab.wms.grinfeld.roman.utils.Utils.CHARSET_ASCII;
import static com.madlab.wms.grinfeld.roman.utils.Utils.CHARSET_UTF16LE;
import static com.madlab.wms.grinfeld.roman.utils.Utils.bitConverterGetBytes;
import static com.madlab.wms.grinfeld.roman.utils.Utils.bitConverterToInt32;

/**
 * Created by GrinfeldRa
 */

public class WMSClient {

    private static final int MAX_SEND_TIMEOUT_MS = 60;
    private static final String TAG = "#WMSClient";
    private Socket socket;
    private DataInputStream in = null;
    private OutputStream out = null;


    public Observable<String> send(String command) {
        Log.d("TAG", command);
        return Observable.just(command)
                .subscribeOn(Schedulers.io())
                .doOnNext(cmd -> Utils.isOnline())
                .doOnNext(s -> connect())
                .timeout(MAX_SEND_TIMEOUT_MS, TimeUnit.SECONDS)
                .map(bytes -> prepareMessage(command))
                .doOnNext(this::sendBytes)
                .map(bytes -> WMSClient.this.receive())
                .map(this::parseByte)
                .timeout(MAX_SEND_TIMEOUT_MS, TimeUnit.SECONDS)
                .doFinally(this::disconnect);
    }

    public Observable<String> sendArray(String command, List<String> listParam) {
        return Observable.just(command)
                .subscribeOn(Schedulers.io())
                .doOnNext(cmd -> Utils.isOnline())
                .doOnNext(s -> connect())
                .timeout(MAX_SEND_TIMEOUT_MS, TimeUnit.SECONDS)
                .map(bytes -> prepareArrayMessage(command, listParam))
                .doOnNext(this::sendBytes)
                .map(bytes -> WMSClient.this.receive())
                .map(this::parseByte)
                .timeout(MAX_SEND_TIMEOUT_MS, TimeUnit.SECONDS)
                .doFinally(this::disconnect)
                .doOnError(throwable -> disconnect());
    }

    private void createSocket() throws IOException {
        socket = new Socket();
        InetSocketAddress socketAddress = new InetSocketAddress(Preference.getServer(), Preference.getPort());
        socket.connect(socketAddress);
    }

    private void connect() throws IOException {
        createSocket();
        if (socket.isConnected()) {
            in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            out = socket.getOutputStream();
        }
    }

    private void disconnect() throws IOException {
        if (in != null) {
            in.close();
        }
        if (out != null) {
            out.close();
        }
        if (socket != null) {
            socket.close();
        }
        socket = null;
        in = null;
        out = null;
    }


    private void sendBytes(byte[] message) throws NullPointerException, IOException {
        out.write(message);
    }


    private byte[] receive() throws NullPointerException, IOException, InterruptedException {
        int lengthHeader = 19;
        byte[] answerHeader = new byte[lengthHeader];
        in.readFully(answerHeader);
        byte[] bytesLength = Arrays.copyOfRange(answerHeader, 15, lengthHeader);
        int lengthBody = bitConverterToInt32(bytesLength);
        byte[] answer = new byte[lengthBody];
        in.readFully(answer);
        return answer;
    }

    private String parseByte(byte[] result) {
        if (result[0] == 0) {
            return new String(Arrays.copyOfRange(result, 5, result.length), Charset.forName(CHARSET_UTF16LE));
        } else {
            StringBuilder builder = new StringBuilder();
            byte[] lengthCommand = Arrays.copyOfRange(result, 1, 2);
            byte[] bytesRows = Arrays.copyOfRange(result, 5, 9);
            int maxRows = bitConverterToInt32(bytesRows);
            int currentPos = lengthCommand[0] + 5 + 4;
            for (int i = 0; i < maxRows; i++) {
                byte[] bytes = Arrays.copyOfRange(result, currentPos, currentPos + 4);
                int rowLength = bitConverterToInt32(bytes);
                currentPos += 4;
                String splitter = i == 0 ? "" : "|";
                builder.append(splitter);
                byte[] dataRow = Arrays.copyOfRange(result, currentPos, currentPos + rowLength);
                builder.append(new String(dataRow, Charset.forName(CHARSET_UTF16LE)));
                currentPos += rowLength;
            }
            return builder.toString();
        }
    }

    private byte[] prepareMessage(String command) {
        String generationID = Utils.GenerationID();
        String title = "sevco";
        String userID = Preference.getLoginCode();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String date = dateFormat.format(new Date());
        String hash = String.format("%s_%s_%s", command.hashCode(), generationID, date);
        byte[] titleBuff, userIDBuff, buffMLeng, buffMAC, hashbuff, hashbuffleng, databuff, databuffleng, datatype;
        userIDBuff = userID.getBytes(Charset.forName(CHARSET_ASCII));
        titleBuff = title.getBytes(Charset.forName(CHARSET_ASCII));
        buffMLeng = BigInteger.valueOf(Utils.getMacAddress().length).toByteArray();
        buffMAC = Utils.getMacAddress();
        hashbuffleng = BigInteger.valueOf(hash.getBytes(Charset.forName(CHARSET_ASCII)).length).toByteArray();
        hashbuff = hash.getBytes(Charset.forName(CHARSET_ASCII));
        datatype = new byte[]{0};
        databuffleng = new byte[]{Integer.valueOf(command.getBytes(Charset.forName(CHARSET_UTF16LE)).length).byteValue(), 0, 0, 0};
        databuff = command.getBytes(Charset.forName(CHARSET_UTF16LE));
        int length = titleBuff.length + userIDBuff.length + buffMLeng.length + buffMAC.length + hashbuffleng.length + hashbuff.length + datatype.length + databuffleng.length + databuff.length;
        ByteBuffer message = ByteBuffer.allocate(length);
        message.put(titleBuff);
        message.put(userIDBuff);
        message.put(buffMLeng);
        message.put(buffMAC);
        message.put(hashbuffleng);
        message.put(hashbuff);
        message.put(databuffleng);
        message.put(datatype);
        message.put(databuff);
        return message.array();
    }


    private byte[] prepareArrayMessage(String command, List<String> listParam) {
        String generationID = Utils.GenerationID();

        String title = "Sevco";
        String userID = Preference.getLoginCode();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String date = dateFormat.format(new Date());
        String hash = String.format("%s_%s_%s", command.hashCode(), generationID, date);
        byte[] titleBuff, userIDBuff, buffMACLength, buffMAC, hashBuff, hashBuffLength, dataBuff, dataBuffLength, dataType;
        userIDBuff = userID.getBytes(Charset.forName(CHARSET_ASCII));
        titleBuff = title.getBytes(Charset.forName(CHARSET_ASCII));
        buffMAC = Utils.getMacAddress();
        buffMACLength = BigInteger.valueOf(buffMAC.length).toByteArray();
        hashBuff = hash.getBytes(Charset.forName(CHARSET_ASCII));
        hashBuffLength = BigInteger.valueOf(hashBuff.length).toByteArray();
        dataType = new byte[]{1};
        dataBuff = command.getBytes(Charset.forName(CHARSET_UTF16LE));
        dataBuffLength = bitConverterGetBytes(dataBuff.length);
        byte[] countLines = bitConverterGetBytes(listParam.size());
        int realDataLength = countLines.length + dataBuff.length + 4;
        for (String lineData : listParam) {
            byte[] lineBuff = lineData.getBytes(Charset.forName(CHARSET_UTF16LE));
            byte[] lineBuffLength = bitConverterGetBytes(lineBuff.length);
            realDataLength += lineBuff.length + lineBuffLength.length;
        }
        byte[] fullDataLength = bitConverterGetBytes(realDataLength);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        stream.write(titleBuff, 0, titleBuff.length);
        stream.write(userIDBuff, 0, userIDBuff.length);
        stream.write(buffMACLength, 0, buffMACLength.length);
        stream.write(buffMAC, 0, buffMAC.length);
        stream.write(hashBuffLength, 0, hashBuffLength.length);
        stream.write(hashBuff, 0, hashBuff.length);
        stream.write(fullDataLength, 0, fullDataLength.length);
        stream.write(dataType, 0, dataType.length);
        stream.write(dataBuffLength, 0, dataBuffLength.length);
        stream.write(dataBuff, 0, dataBuff.length);
        stream.write(countLines, 0, countLines.length);
        for (String lineData : listParam) {
            byte[] lineBuff = lineData.getBytes(Charset.forName(CHARSET_UTF16LE));
            byte[] lineBuffLength = bitConverterGetBytes(lineBuff.length);
            stream.write(lineBuffLength, 0, lineBuffLength.length);
            stream.write(lineBuff, 0, lineBuff.length);
        }
        return stream.toByteArray();
    }

}
