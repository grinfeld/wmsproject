package com.madlab.wms.grinfeld.roman.common;


import com.arellomobile.mvp.MvpPresenter;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by GrinfeldRa
 */
public abstract class BasePresenter<View extends BaseView> extends MvpPresenter<View> {

    protected final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private void disposeAll() {
        mCompositeDisposable.dispose();
    }

    protected void add(Disposable disposable){
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void onDestroy() {
        disposeAll();
    }

    protected boolean isSuccess(String response) {
        if (response.contains("OK")) {
            return true;
        } else if (response.contains("ERROR")) {
            getViewState().showError(response.split(";")[1]);
            return false;
        }
        return true;
    }

}
