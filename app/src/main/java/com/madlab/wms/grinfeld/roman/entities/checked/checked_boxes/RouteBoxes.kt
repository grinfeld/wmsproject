package com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.madlab.wms.grinfeld.roman.entities.converter.DataConverter

/**
 * Created by grinfeldra
 *
 * 000000000000008;20200211;1;Н Челны (левая);0015700000007022020;0;31;1;1;1;0015700000007022020;+18 Корма(вес);
 *
 */
@Entity
data class RouteBoxes(@PrimaryKey(autoGenerate = true) var id: Long?,
                 var docNum: String, var date: String, var nameRoute: String, var barcodeRoute: String, var zone: String) {

    @TypeConverters(DataConverter::class)
    var euzBoxesRoutes = java.util.ArrayList<EuzBoxesRoute>()

    @TypeConverters(DataConverter::class)
    var euzBarcode = java.util.ArrayList<EuzBarcode>()

    constructor() : this(null, "", "", "", "", "")

    companion object {
        fun parseAnswer(answer: String, answer2: String): RouteBoxes {
            val data = answer.split("|")
            val firstLine = data[0].split(";")
            val result = RouteBoxes()
            result.docNum = firstLine[0]
            result.date = firstLine[1]
            result.nameRoute = firstLine[3]
            result.barcodeRoute = firstLine[4]
            result.zone = firstLine[11]
            val subList = data.subList(1, data.size)
            result.euzBoxesRoutes.addAll(EuzBoxesRoute.parseAnswer(subList))
            result.euzBarcode.addAll(EuzBarcode.parseAnswer(answer2))
            return result
        }
    }

}