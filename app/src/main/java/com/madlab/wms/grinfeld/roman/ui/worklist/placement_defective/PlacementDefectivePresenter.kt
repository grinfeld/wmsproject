package com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class PlacementDefectivePresenter : BasePresenter<PlacementDefectiveView>() {

    fun startBlockCell(cell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_BLOCK_PLACEMENT_CELL(cell))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val cellTxt = it.split(";")[1]
                        viewState.onCompleteBlockCell(cell, cellTxt)
                    }
                })
    }

    fun getInfoEuz(code: String, cell: String, cellTxt: String, timestamp: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_INFO_EUZ_FROM_CODE(cell, code))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val result: List<PlacementCell> =
                                PlacementCell.parseAnswer42(it, cell, timestamp, cellTxt)
                        viewState.onCompleteGetInfoEuz(result)
                    }
                })
    }


}