package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.Route
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode.checkingEUZ.CheckingEUZFragment
import kotlinx.android.synthetic.main.routes_fragment.*
import java.util.*

/**
 * Created by grinfeldra
 */

class RoutesFragment : MvpAppCompatFragment(), RoutesView, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener, IOnScannerEvent {

    @InjectPresenter
    lateinit var presenter: RoutesPresenter

    @ProvidePresenter
    fun providePresenter() = RoutesPresenter()

    private lateinit var progressOwner: ProgressOwner

    private lateinit var scanner: AbstractScanner

    private var adapter: ArrayAdapter<Route>? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.list_routes))
        return inflater.inflate(R.layout.routes_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ArrayAdapter(context!!, R.layout.list_item_universal, ArrayList())
        list_view.adapter = adapter
        list_view.onItemSelectedListener = this
        list_view.onItemClickListener = this
        presenter.loadListRoute()
    }

    override fun onCompleteLoadListRoute(routes: List<Route>) {
        adapter?.clear()
        adapter?.addAll(routes)
        adapter?.notifyDataSetChanged()
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val route = parent?.getItemAtPosition(position) as Route
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.start_check))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        presenter.getEuzFromRoute(route)
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val route = parent?.getItemAtPosition(position) as Route
        txt_doc_num.text = route.docNum
    }

    override fun onCompleteGetEuzFromRoute(routeNum: String) {
        (activity as MainActivity).changeFragment(CheckingEUZFragment().newInstance(routeNum))
    }


    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        val barcode = goodsCard?.barcode?.trim()
        if (barcode != null && barcode.contains("M") && barcode.contains("L")) {
            val routeBarcode = barcode?.substring(barcode.indexOf("M") + 1,
                    barcode.indexOf("L"))
            var isExistRoute: Route? = null
            if (adapter != null) {
                for (i in 0 until adapter!!.count) {
                    val item = adapter?.getItem(i)
                    if (item?.docNum?.equals(routeBarcode)!!) {
                        isExistRoute = item
                        break
                    }
                }
                if (isExistRoute != null) {
                    presenter.getEuzFromRoute(isExistRoute)
                } else {
                    showError(getString(R.string.route_not_exist))
                }
            }
        } else {
            showError(getString(R.string.error_route_barcode))
        }
    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun onPause() {
        super.onPause()
        scanner.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner.onStop()
    }

}