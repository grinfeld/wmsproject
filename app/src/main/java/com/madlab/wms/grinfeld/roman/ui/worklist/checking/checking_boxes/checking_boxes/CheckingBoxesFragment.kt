package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBarcode
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBoxesRoute
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.RouteBoxes
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes.checking_enter_count.CheckingEnterCountFragment
import com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes.checking_validation.CheckingValidationFragment
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import kotlinx.android.synthetic.main.checking_boxes_fragment.*

/**
 * Created by grinfeldra
 */
const val DOC_NUM = "doc_num"

class CheckingBoxesFragment : MvpAppCompatFragment(), KeyEventListener, IOnScannerEvent, CheckingBoxesView {

    @InjectPresenter
    lateinit var presenter: CheckingBoxesPresenter

    @ProvidePresenter
    fun providePresenter() = CheckingBoxesPresenter()

    lateinit var route: RouteBoxes
    private var scanner: AbstractScanner? = null
    private var boxNum: Int? = null
    lateinit var sortedWith: List<EuzBoxesRoute>
    private var progressOwner: ProgressOwner? = null

    fun newInstance(docNum: String) = CheckingBoxesFragment().apply {
        arguments = Bundle().apply {
            putString(DOC_NUM, docNum)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.selection_check_title))
        return inflater.inflate(R.layout.checking_boxes_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val docNum = arguments?.getString(DOC_NUM)
        val checkingBoxesDao = App.getDatabase().checkingBoxesDao()
        route = checkingBoxesDao.getRoute(docNum!!)
        et_route.setText(getString(R.string.check_boxes_route, route.nameRoute))
        et_zone.setText(getString(R.string.check_boxes_zone, route.zone))
        sortedWith = route.euzBoxesRoutes.sortedWith(compareBy(EuzBoxesRoute::numBoxes))
        val boxesRoute: EuzBoxesRoute? = sortedWith.first { euzBoxesRoute -> !euzBoxesRoute.checked }
        if (boxesRoute != null) {
            et_scan_message.setText(getString(R.string.check_boxes_scan_message, boxesRoute.numBoxes.toString()))
        } else {
            completeDialog(true)
        }
    }

    override fun onCompleteReserve() {
        fragmentManager?.popBackStack()
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    closeDialog()
                }
                true
            }
            KeyEvent.KEYCODE_E -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    val boxesRoute: EuzBoxesRoute? = sortedWith.first { euzBoxesRoute -> !euzBoxesRoute.checked }
                    if (boxesRoute != null) {
                        completeDialog(false)
                    } else {
                        completeDialog(true)
                    }
                }
                true
            }
            KeyEvent.KEYCODE_P -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    val filter = sortedWith.filter { euzBoxesRoute -> !euzBoxesRoute.checked }
                    if (filter.isNotEmpty()) {
                        (activity as MainActivity).changeFragment(CheckingValidationFragment().newInstance(filter))
                    } else {
                        completeDialog(true)
                    }
                }
                true
            }
            else -> false
        }
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        val barcode = goodsCard?.barcode?.trim()
        if (barcode != null) {
            if (boxNum == null) {
                if (barcode.contains("M") && barcode.contains("K")) {
                    //boxNum = "050||50"
                    boxNum = barcode.substring(barcode.indexOf("K") + 1).toInt()
                    et_scan_message.setText(getString(R.string.check_boxes_scan_message2, boxNum.toString()))
                } else {
                    showError("Сканируйте ШК короба")
                }
            } else {
                if (barcode.contains("M") && barcode.contains("K")) {
                    boxNum = barcode.substring(barcode.indexOf("K") + 1).toInt()
                    et_scan_message.setText(getString(R.string.check_boxes_scan_message2, boxNum.toString()))
                } else {
                    val barcodeEuz: EuzBarcode? = route.euzBarcode.first { euzBarcode -> euzBarcode.barcode == barcode }
                    if (barcodeEuz != null) {
                        val boxesRoute: EuzBoxesRoute? = route.euzBoxesRoutes.first { euzBoxesRoute -> euzBoxesRoute.codeGoods == barcodeEuz.codeGoods }
                        if (boxesRoute != null) {
                            (activity as MainActivity).changeFragment(CheckingEnterCountFragment().newInstance(route.docNum, boxesRoute.id!!))
                        } else {
                            showError("ЕУЗ не найден")
                        }
                    } else {
                        showError("ЕУЗ не найден")
                    }
                }
            }
        } else {

        }
    }


    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    private fun closeDialog() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.close_check_boxes))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        fragmentManager?.popBackStack()
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    private fun completeDialog(isComplete: Boolean) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(if (isComplete) getString(R.string.check_boxes_complete) else getString(R.string.not_all_check_complete))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        presenter.reserveChecking(route)
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }
}