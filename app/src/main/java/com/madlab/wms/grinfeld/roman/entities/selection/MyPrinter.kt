package com.madlab.wms.grinfeld.roman.entities.selection

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import com.madlab.wms.grinfeld.roman.common.App


/**
 * Created by grinfeldra
 */
@Entity
class MyPrinter(@PrimaryKey(autoGenerate = true)
                val id: Int? = null,
                @ColumnInfo(name = "host")
                val host: String,
                @ColumnInfo(name = "name")
                val name: String) : AbstractData() {

    override fun getLeftText(): String {
        return name
    }

    override fun getRightText(): String {
        return host
    }

    companion object {
        fun parseResponse(response: String) {
            val dao = App.getDatabase().orderSelectionDao()
            dao.deleteAllPrinter()
            val split = response.split("|")
            split.forEach {
                val values = it.split(";")
                dao.insert(MyPrinter(host = values[0], name = values[1]))
            }
        }
    }

}