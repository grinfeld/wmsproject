package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_series

import android.util.Pair
import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct
import com.madlab.wms.grinfeld.roman.entities.Measure
import java.text.NumberFormat

/**
 * Created by grinfeldra
 */
@InjectViewState
class InventorySeriesPresenter : BasePresenter<InventorySeriesView>() {

    internal fun initFieldMeasure(product: InventoryCellProduct) {
        val measure = Measure.getSelected(product.measureList)
        val countFactAndDefective : Pair<Double, Double> = Pair(product.countFact, product.countDefective)
        if (measure != null) {
            var restCount = 0.0
            var restDefective = 0.0
            if (measure.koef != 1.0) {
                restCount = countFactAndDefective.first % measure.koef
                restDefective = countFactAndDefective.second % measure.koef
            }
            val numberFormat = NumberFormat.getInstance()
            val countText = (numberFormat.format((countFactAndDefective.first - restCount) / measure.koef) + " "
                    + measure.name + if (restCount == 0.0) "" else numberFormat.format(restCount) + " " + product.measureInitName)
            val countDefective = (numberFormat.format((countFactAndDefective.second - restDefective) / measure.koef) + " "
                    + measure.name + if (restDefective == 0.0) "" else numberFormat.format(restDefective) + " " + product.measureInitName)
            viewState.onInitFieldMeasure(countText, countDefective)
        }

    }



}