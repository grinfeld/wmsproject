package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_select_euz

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct

/**
 * Created by grinfeldra
 */
interface InventorySelectEUZView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteLoadProductInInventoryEUZ(list: ArrayList<InventoryCellProduct>?,
                                            createDate: String, inventoryNum: String)
}