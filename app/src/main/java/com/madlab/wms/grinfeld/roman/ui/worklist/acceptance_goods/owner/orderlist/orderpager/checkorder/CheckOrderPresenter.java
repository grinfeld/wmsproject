package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.entities.Product;
import com.madlab.wms.grinfeld.roman.entities.parser.ProductParser;
import com.madlab.wms.grinfeld.roman.utils.DateParser;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;


/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class CheckOrderPresenter extends BasePresenter<CheckOrderView> {

    void loadGoods(Order order) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_PRODUCTS(order.getName(), DateParser.getDateToStringFromLongToRequest(order.getDate())))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                            if (isSuccess(response)) {
                                if (!response.contains("OK")) {
                                    ArrayList<Product> list = ProductParser.getList(response);
                                    getViewState().onCompleteLoadGoods(list);
                                } else {
                                    getViewState().onCompleteLoadGoods(new ArrayList<>());
                                }
                            }
                        },
                        throwable -> getViewState().showError(throwable.getMessage())));
    }


    void enterBarcode(String productName, String barcode, int coefficient) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_ENTER_BARCODE(productName, barcode, coefficient))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteEnterBarcode();
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

}
