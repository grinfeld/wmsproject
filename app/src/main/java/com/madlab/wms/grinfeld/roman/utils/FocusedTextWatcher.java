package com.madlab.wms.grinfeld.roman.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.ordercard.IDateAutocomplete;

/**
 * Created by GrinfeldRa
 */
public class FocusedTextWatcher implements TextWatcher {

    private EditText editText;
    private IDateAutocomplete iDateAutocomplete;

    public FocusedTextWatcher(EditText editText, IDateAutocomplete iDateAutocomplete) {
        super();
        this.editText = editText;
        this.iDateAutocomplete = iDateAutocomplete;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (editText.getId() == R.id.et_prod_year && s.toString().length() == 4){
            iDateAutocomplete.onDateAutocomplete();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        int maxLength = editText.getId() == R.id.et_prod_year || editText.getId() == R.id.et_expiration_year ? 4 : 2;
        if (s.toString().length() == maxLength) {
            if (editText != null) {
                View v = editText.focusSearch(View.FOCUS_RIGHT);
                if (v != null)
                    v.requestFocus();
            }
        } else if (s.toString().length() == 0) {
            View v = editText.focusSearch(View.FOCUS_LEFT);
            if (v != null) {
                v.requestFocus();
                EditText editText = (EditText) v;
                editText.setSelection(editText.getText().toString().length());
            }
        }
    }
}
