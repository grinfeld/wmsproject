package com.madlab.wms.grinfeld.roman.entities.placement

import android.os.Parcelable
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.parcel.Parcelize

/**
 * Created by grinfeldra
 */
//ОТТРН75487;Торт"Прага" 0,6;360;;360;Шт.;720369;25.05.2019;25.05.2020;60 Уп. ;
//ОТТРН75487;Торт"Прага" 0,6;360;;360;Шт.;720369;25.05.2019;25.05.2020;ОТТРН;Ответохранение Транзит;0;;1;1;0;6;Уп.;;;;;;;0;0;
@Parcelize
class PlacementCell(var code: String, var name: String, var rest: Double, var expected: Double,
                    var freely: Double, var baseEi: String, var codeSeries: String,
                    var dateStart: String, var dateEnd: String, var otherEiTxt: String,
                    var owner: String, var koef: Double, var ei2: String,
                    var fromCell: String, var toCell: String, var totalCount: Double,
                    var timestamp: String, var fromCellTxt: String, var toCellTxt: String,
                    var accepted: Double) : AbstractData(), Parcelable {

    override fun getLeftText(): String {
        return code
    }

    override fun getRightText(): String {
        return name
    }

    companion object {
        fun parseAnswer(answer40: String, fromCell: String, timestamp: String, cellTxt: String): List<PlacementCell> {
            val result = ArrayList<PlacementCell>()
            val split = answer40.split("|")
            for (value in split) {
                val split1 = value.split(";")
                val code = split1[0]
                val name = split1[1]
                val rest = Utils.toDouble(split1[2])
                val expected = Utils.toDouble(split1[3])
                val freely = Utils.toDouble(split1[4])
                val baseEi = split1[5]
                val codeSeries = split1[6]
                val dateStart = split1[7]
                val dateEnd = split1[8]
                val otherEiTxt = split1[9]
                result.add(PlacementCell(code, name, rest, expected,
                        freely, baseEi, codeSeries, dateStart, dateEnd, otherEiTxt,
                        owner = "", koef = 0.0, ei2 = "", fromCell = fromCell, toCell = "",
                        totalCount = 0.0, timestamp = timestamp, fromCellTxt = cellTxt,
                        toCellTxt = "", accepted = 0.0))
            }
            return result
        }

        fun parseAnswer42(answer42: String, fromCell: String, timestamp: String, cellTxt: String): List<PlacementCell> {
            val result = ArrayList<PlacementCell>()
            val split = answer42.split("|")
            for (value in split) {
                val split1 = value.split(";")
                val code = split1[0]
                val name = split1[1]
                val rest = Utils.toDouble(split1[2])
                val expected = Utils.toDouble(split1[3])
                val freely = Utils.toDouble(split1[4])
                val baseEi = split1[5]
                val codeSeries = split1[6]
                val dateStart = split1[7]
                val dateEnd = split1[8]
                val owner = split1[9]
                val koef = Utils.toDouble(split1[16])
                val ei2 = split1[17]
                result.add(PlacementCell(code, name, rest, expected,
                        freely, baseEi, codeSeries, dateStart, dateEnd, otherEiTxt = "",
                        owner = owner, koef = koef, ei2 = ei2, fromCell = fromCell, toCell = "",
                        totalCount = 0.0, timestamp = timestamp, fromCellTxt = cellTxt,
                        toCellTxt = "", accepted = 0.0))
            }
            return result
        }
    }

}