package com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentOwner
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.shipment_enter.ShipmentEnterFragment
import kotlinx.android.synthetic.main.shipment_fragment.list_view
import kotlinx.android.synthetic.main.shipment_morph_fragment.*
import java.util.ArrayList

/**
 * Created by grinfeldra
 */

const val KEY_TYPE_SHIPMENT = "key_shipment"
const val ORDER_FOR_TRANSPORT = 0
const val ROUTES = 1
const val SELECTION_BOX = 2

class ShipmentMorphFragment : MvpAppCompatFragment(), ShipmentOwnerMorphView, IOnScannerEvent, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    @InjectPresenter
    lateinit var presenter: ShipmentOwnerMorphPresenter

    @ProvidePresenter
    fun providePresenter() = ShipmentOwnerMorphPresenter()

    private var scanner: AbstractScanner? = null
    private var progressOwner: ProgressOwner? = null
    private var type: Int? = null


    fun newInstance(type: Int) = ShipmentMorphFragment().apply {
        arguments = Bundle().apply {
            putInt(KEY_TYPE_SHIPMENT, type)
        }
    }

    override fun onFirstViewAttach() {
        type = arguments?.getInt(KEY_TYPE_SHIPMENT)!!
        presenter.loadShipment(type!!)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var title = ""
        when (arguments?.getInt(KEY_TYPE_SHIPMENT)) {
            ORDER_FOR_TRANSPORT -> title = getString(R.string.order_for_transport)
            ROUTES -> title = getString(R.string.routes)
            SELECTION_BOX -> title = getString(R.string.selection_box)
        }
        (activity as BaseFragmentActivity).setActionBarTitle(title)
        return inflater.inflate(R.layout.shipment_morph_fragment, container, false)
    }


    override fun completeLoadShipment(result: List<ShipmentOwner>) {
        val adapter = ArrayAdapter(context, android.R.layout.select_dialog_item,
                result)
        list_view.adapter = adapter
        list_view.onItemClickListener = this
        list_view.onItemSelectedListener = this
    }

    override fun completeLoadItemInfo(data: ArrayList<Shipment>, docNum: String, nameOwner: String) {
        val docType = arguments?.getInt(KEY_TYPE_SHIPMENT)
        (activity as BaseFragmentActivity).changeFragment(docType?.let {
            ShipmentEnterFragment().newInstance(data, docNum, nameOwner, it)
        })
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val shipment = parent?.getItemAtPosition(position) as ShipmentOwner
        presenter.loadItem(shipment)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val shipment = parent?.getItemAtPosition(position) as ShipmentOwner
        txt1.text = shipment.date
    }

    override fun onDataScanner(goodsCard: ScannerData?) {

    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

}