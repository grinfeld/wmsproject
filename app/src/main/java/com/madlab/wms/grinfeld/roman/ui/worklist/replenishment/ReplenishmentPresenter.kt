package com.madlab.wms.grinfeld.roman.ui.worklist.replenishment

import androidx.lifecycle.MutableLiveData
import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.Replenishment
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class ReplenishmentPresenter : BasePresenter<ReplenishmentView>() {

    var liveData = MutableLiveData<Int>()

    init {
        liveData.value = 0
    }

    fun setReserve(replenishment: Replenishment, docType: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_RESERVE_REPLENISHMENT(replenishment.num,
                replenishment.fromCell, replenishment.toCell, docType))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        if (it.contains("OK")) {
                            viewState.onCompleteReserve()
                        } else {
                            val list = Replenishment.parseAnswer(it)
                            viewState.onCompleteItemReserve(replenishment.fromCellTxt, replenishment.toCellTxt, list)
                        }
                    }
                })
    }


}