package com.madlab.wms.grinfeld.roman.ui.worklist.shipment

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
interface ShipmentView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun completeLoadItemInfo(data: ArrayList<Shipment>, docNum: String, nameOwner: String)

}