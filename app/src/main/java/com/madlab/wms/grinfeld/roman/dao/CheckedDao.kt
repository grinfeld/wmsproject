package com.madlab.wms.grinfeld.roman.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.Route

/**
 * Created by grinfeldra
 */
@Dao
interface CheckingDao {


    @Query("SELECT * FROM Route where `docNum` =:numRoute")
    fun getRoute(numRoute: String) : Route

    @Query("SELECT * FROM Route where `docNum` =:numRoute")
    fun getLiveRoute(numRoute: String) : LiveData<Route>

    @Insert
    fun insert(route: Route)

    @Update
    fun update(route: Route)

}