package com.madlab.wms.grinfeld.roman.ui.worklist;

import androidx.fragment.app.Fragment;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.entities.InventoryCell;
import com.madlab.wms.grinfeld.roman.entities.InventoryEUZ;
import com.madlab.wms.grinfeld.roman.entities.Replenishment;
import com.madlab.wms.grinfeld.roman.entities.Zone;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.BailorListFragment;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.InventoryListFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_select_euz.InventorySelectEUZFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.placement.PlacementFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.placement_transit_te.PlacementTransitFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.replenishment.ReplenishmentFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.SelectionFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.shipment.ShipmentFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;

import static com.madlab.wms.grinfeld.roman.ui.worklist.placement.PlacementFragmentKt.TYPE_OTHER_PLACEMENT;
import static com.madlab.wms.grinfeld.roman.ui.worklist.placement.PlacementFragmentKt.TYPE_RETURN_PLACEMENT;


/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class WorkListPresenter extends BasePresenter<WorkListView> {


    void startCommand(int position) {
        switch (position) {
            case 0://прием
                startCommand(Command.COM_BAILOR(Command.TYPE_ORDER), position);
                break;
            case 1://возврат
                startCommand(Command.COM_BAILOR(Command.TYPE_RETURN), position);
                break;
            case 2://размещение
                startCommand(Command.COM_GET_ACCESS_WORK(Command.TYPE_CELL), position);
                break;
            case 3://размещение и подпитка
                loadReplenishmentNum();
                break;
            case 4://отбор
                getViewState().openWorkFragment(new SelectionFragment());
                break;
            case 5://размещение транзитной ТЕ
                getViewState().openWorkFragment(new PlacementTransitFragment());
                break;
            case 6://отгрузка
                getViewState().openWorkFragment(new ShipmentFragment());
                break;
            case 7://инвентаризация ячейки
                startCommand(Command.COM_GET_ACCESS_WORK(Command.TYPE_INVENTORY), position);
                break;
            case 8://инвентаризация ЕУЗ
                loadInventoryEUZ();
                break;
            case 9://проверка
                getViewState().openCheckDialog();
                break;
            case 10://размещение возврата
                startCommand(Command.COM_GET_ACCESS_WORK(Command.TYPE_CELL), position);
                break;
            case 11://размещение брака
                startCommand(Command.COM_GET_ACCESS_WORK(Command.TYPE_CELL), position);
                break;
        }
    }

    private void startCommand(String command, int position) {
        mCompositeDisposable.add(new WMSClient().send(command)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().openWorkFragment(newInstanceFragmentOnWork(position, response));
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    void loadZoneInventory() {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_GET_LIST_ZONE())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        List<Zone> zones = Zone.Companion.parseAnswer(response);
                        getViewState().openZoneList(zones);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    private Fragment newInstanceFragmentOnWork(int position, String param) {
        switch (position) {
            case 0:
                return BailorListFragment.newInstance(param, Command.TYPE_ORDER);
            case 1:
                return BailorListFragment.newInstance(param, Command.TYPE_RETURN);
            case 2:
                String timestamp = param.split(";")[2];
                return new PlacementFragment().newInstance(timestamp, TYPE_OTHER_PLACEMENT);
            case 7:
                return InventoryListFragment.newInstance(new ArrayList<>(), Command.TYPE_CELL);
            case 10:
                String timestamp2 = param.split(";")[2];
                return new PlacementFragment().newInstance(timestamp2, TYPE_RETURN_PLACEMENT);
            case 11:
                String timestamp3 = param.split(";")[2];
                getViewState().openPlacementDefectiveDialog(timestamp3);
                return null;
            default:
                return null;
        }
    }


    private void loadInventoryEUZ() {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_GET_INVENTORY(Command.TYPE_EUZ))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        List<InventoryCell> listInventoryCell = InventoryCell.getListInventoryCell(response);
                        getViewState().onCompleteLoadInventoryEUZ(listInventoryCell);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    void loadInventoryListEUZFromZone(String zone) {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_LIST_EUZ_FROM_ZONE(zone))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        ArrayList<InventoryEUZ> inventoryEUZList = InventoryEUZ.Companion.parse(response);
                        getViewState().openWorkFragment(new InventorySelectEUZFragment().newInstance(inventoryEUZList));
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

    private void loadReplenishmentNum() {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_GET_REPLENISHMENT_NUM())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        String[] result = response.split(";");
                        String num = result[1];
                        String type = result[2];
                        loadReplenishmentType(num, type);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    private void loadReplenishmentType(String num, String type) {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_GET_REPLENISHMENT_TYPE(num, type))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        String[] result = response.split(";");
                        String mType = result[1];
                        loadReplenishmentGoods(num, mType);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

    private void loadReplenishmentGoods(String num, String type) {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_GET_REPLENISHMENT_GOODS(num, type))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        ArrayList<Replenishment> list = Replenishment.Companion.parseAnswer(response);
                        if (list.size() > 0) {
                            getViewState().openWorkFragment(new ReplenishmentFragment().newInstance(list, type));
                        } else {
                            getViewState().showError("Недостаточно данных");
                        }
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

}
