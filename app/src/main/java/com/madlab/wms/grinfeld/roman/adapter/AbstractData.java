package com.madlab.wms.grinfeld.roman.adapter;

/**
 * Created by GrinfeldRA
 */
public abstract class AbstractData {

    public abstract String getLeftText();
    public abstract String getRightText();

}
