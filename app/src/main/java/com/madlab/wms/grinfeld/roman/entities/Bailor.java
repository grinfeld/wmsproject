package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.wms.grinfeld.roman.adapter.AbstractData;

public class Bailor extends AbstractData implements Parcelable {

    private static final String TAG = Bailor.class.getSimpleName();

    private String code;
    private String bailor;
    private int f;
    private int s;
    private int t;

    public Bailor(String holder, String name, int f, int s, int t) {
        this.code = holder;
        this.bailor = name;
        this.f = f;
        this.s = s;
        this.t = t;
    }

    protected Bailor(Parcel in) {
        code = in.readString();
        bailor = in.readString();
        f = in.readInt();
        s = in.readInt();
        t = in.readInt();
    }

    public String getCode() {
        return code;
    }

    public String getBailor() {
        return bailor;
    }

    public int getF() {
        return f;
    }

    public int getS() {
        return s;
    }

    public int getT() {
        return t;
    }

    @Override
    public String toString() {
        return "Owner{" +
                "code='" + code + '\'' +
                ", bailor='" + bailor + '\'' +
                ", f=" + f +
                ", s=" + s +
                ", t=" + t +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(bailor);
        dest.writeInt(f);
        dest.writeInt(s);
        dest.writeInt(t);
    }

    public static final Creator<Bailor> CREATOR = new Creator<Bailor>() {
        @Override
        public Bailor createFromParcel(Parcel in) {
            return new Bailor(in);
        }

        @Override
        public Bailor[] newArray(int size) {
            return new Bailor[size];
        }
    };

    @Override
    public String getLeftText() {
        return code;
    }

    @Override
    public String getRightText() {
        return bailor;
    }

}
