package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.common.App;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.entities.ProductParam;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.entities.parser.GoodsInDocParser;
import com.madlab.wms.grinfeld.roman.utils.DateParser;

import io.reactivex.android.schedulers.AndroidSchedulers;


/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class OrderPresenter extends BasePresenter<OrderView> {

    void getParamOnPallet(String numDoc, String dateDoc, ScannerData scannerData) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_PARAM_ON_PALLET(numDoc, dateDoc, scannerData.getTe()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteLoadParamPallet(response, scannerData);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

    void loadParamFromScanCode(Order order, ScannerData scannerData, int type, int position) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_PARAM_ON_GOODS(0, order.getName(), DateParser.getDateToStringFromLongToRequest(order.getDate()), type, scannerData.getBarcode()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        ProductParam productParam = new ProductParam(response);
                        if (type == Command.TYPE_GET) {
                            productParam.setRequired(productParam.getRequired() - productParam.getAccepted());
                            productParam.setAccepted(0);
                        }
                        if (productParam.getUnit().equals("кг")) {
                            productParam.setKoefEi1(0);
                            productParam.setEi1("");
                        }
                        productParam.setKoef(1);
                        getViewState().onCompleteLoadParamGoods(productParam, scannerData, position);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    void deleteGoods(String numDoc, int position) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_DELETE_POSITION_IN_ORDER(numDoc, position))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteDeleteGoods();
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

    void getGoodsInOrder(int type, Order order) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_GOODS_IN_ORDER(type, order.getNumDoc(), order.getDateDoc()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteGetGoodsInOrder(GoodsInDocParser.getGoodsInDoc(response));
                    }

                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    void sentToPrintTE(int type, int count) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_SEND_TO_PRINT_TE(type, count))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                            if (response.contains("OK")) {
                                getViewState().onCompleteSentToPrintTE();
                            } else {
                                getViewState().showError(App.getAppContext().getString(R.string.print_fragment_error));
                            }
                        },
                        throwable -> getViewState().showError(throwable.getMessage())));
    }

}
