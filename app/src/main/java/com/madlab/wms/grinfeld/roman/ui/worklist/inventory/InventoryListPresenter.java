package com.madlab.wms.grinfeld.roman.ui.worklist.inventory;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.App;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.entities.InventoryCell;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;


/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class InventoryListPresenter extends BasePresenter<InventoryListView> {

    /**
     * @param type - [cell, te]
     */
    void loadInventory(int type) {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_GET_INVENTORY(type))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        List<InventoryCell> listInventoryCell = InventoryCell.getListInventoryCell(response);
                        getViewState().onCompleteLoadInventoryCell(listInventoryCell);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

    void loadDataFromCellBarcode(String barcode, int type) {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_ACCESS_INVENTORY_CELL(barcode, type))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        if (response.contains("NO")) {
                            getViewState().showDialog(barcode);
                        } else {
                            if (type == 1) {
                                String[] split = response.split(";");
                                String numberInventory = split[1];
                                String createDate = split[2];
                                getViewState().onCompleteLoadDataFromCellBarcode(numberInventory, createDate);
                            } else {
                                getViewState().showError(App.getAppContext().getString(R.string.doc_create));
                            }
                        }
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));

    }

    void loadProductInInventoryCell(String inventoryNum, String createDate, int typeInventory) {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_GET_PRODUCT_IN_INVENTORY(inventoryNum, createDate, typeInventory))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteLoadProductInInventoryCell(InventoryCellProduct.getList(response, typeInventory), createDate, inventoryNum);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }



}
