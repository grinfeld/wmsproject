package com.madlab.wms.grinfeld.roman.entities.shpment

import android.os.Parcelable
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import kotlinx.android.parcel.Parcelize

/**
 * Created by grinfeldra
 *
 *  ГКДАЛ41198;9.8.Тильзитер "Сырная тарелка" 45% фасованный;0,537;
 */
@Parcelize
class ShipmentGoods(var codeGoods: String, var nameGoods: String, var count: String) : AbstractData(),Parcelable {

    override fun getLeftText(): String {
        return codeGoods
    }

    override fun getRightText(): String {
        return count
    }

    companion object {
        fun parse(answer: String): ArrayList<ShipmentGoods> {
            val split = answer.split("|")
            val result = ArrayList<ShipmentGoods>()
            for (value in split) {
                val temp = value.split(";")
                val shipment = ShipmentGoods(temp[0], temp[1], temp[2])
                result.add(shipment)
            }
            return result
        }
    }

}