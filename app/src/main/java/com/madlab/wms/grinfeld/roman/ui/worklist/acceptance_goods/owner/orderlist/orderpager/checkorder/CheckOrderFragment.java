package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.adapter.order.OrderAdapter;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.entities.Product;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.send_to_print_euz.SendToPrintFragment;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.ArrayList;

/**
 * Created by GrinfeldRa
 */
public class CheckOrderFragment extends MvpAppCompatFragment implements CheckOrderView, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener, View.OnKeyListener {

    public static final int LOAD_FROM_FRAGMENT = 0;
    public static final int LOAD_FROM_VIEW_PAGER = 1;
    private static final String KEY_PARAM = "key_param";
    private static final String TYPE_LOAD = "type_load";
    private static final String TAG = "#CheckOrderFragment";
    private Context context;
    private TextView txt_remainder, txt_accepted, txt_expected, txt_name, txt_code, empty_view;
    private ProgressOwner progressOwner;
    private ListView listView;
    private Product product;
    private Order order;
    private View rootView;

    @InjectPresenter
    CheckOrderPresenter presenter;

    @ProvidePresenter
    CheckOrderPresenter providePresenter() {
        return new CheckOrderPresenter();
    }


    public static CheckOrderFragment newInstance(Order order, int typeLoad) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PARAM, order);
        args.putInt(TYPE_LOAD, typeLoad);
        CheckOrderFragment fragment = new CheckOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ProgressOwner) {
            progressOwner = (ProgressOwner) context;
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume" );
        if (getArguments() != null) {
            Order order = getArguments().getParcelable(KEY_PARAM);
            presenter.loadGoods(order);
        }
        listView.setOnKeyListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause" );
        listView.setOnKeyListener(null);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.check_order_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        rootView = view;
        txt_code = view.findViewById(R.id.txt_code);
        txt_name = view.findViewById(R.id.txt_name);
        txt_expected = view.findViewById(R.id.txt_expected);
        txt_accepted = view.findViewById(R.id.txt_accepted);
        txt_remainder = view.findViewById(R.id.txt_remainder);
        listView = view.findViewById(R.id.list_view);
        empty_view = view.findViewById(R.id.emptyElement);
        listView.setOnItemClickListener(this);
        listView.setOnItemSelectedListener(this);
        listView.setOnKeyListener(this);
        if (getArguments() != null) {
            order = getArguments().getParcelable(KEY_PARAM);
//            if (getArguments().getInt(TYPE_LOAD) == LOAD_FROM_FRAGMENT) {
//                presenter.loadGoods(order);
//            }
        }
    }

    @Override
    public void onCompleteLoadGoods(ArrayList<Product> data) {
        float weightLeft = 0.4f;
        float weightRight = 1;
        if (data.size() > 0){
            MyAdapter checkOrderAdapter = new MyAdapter<>(context, data);
            checkOrderAdapter.setWeight(weightLeft, weightRight);
            Utils.setParamFromHeader(rootView, weightLeft, weightRight, getString(R.string.code), getString(R.string.name));
            empty_view.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            listView.setAdapter(checkOrderAdapter);
            listView.setFocusable(true);
            listView.requestFocus();
        }else {
            listView.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onCompleteEnterBarcode() {
        Toast.makeText(context, getString(R.string.barcode_enter_complete), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        product = (Product) parent.getItemAtPosition(position);
        txt_code.setText(product.getCode());
        txt_name.setText(product.getName());
        txt_expected.setText(getString(R.string.expected, product.getBox()));
        txt_accepted.setText(getString(R.string.accepted, product.getThing()));
        txt_remainder.setText(getString(R.string.remained, product.getBox2()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        parent.requestFocusFromTouch();
        parent.setSelection(position);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_P:
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (product != null && order != null && getActivity() != null) {
                        ((BaseFragmentActivity) getActivity()).changeFragment(SendToPrintFragment.newInstance(order, product));
                    }
                }
                return true;
            case KeyEvent.KEYCODE_A:
                if (product != null) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                        builder2.setTitle(getString(R.string.enter_barcode));
                        Spanned spanned;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            spanned = Html.fromHtml("ЕУЗ(" + product.getCode() + "):</br>" + product.getName(), Html.FROM_HTML_MODE_COMPACT);
                        } else {
                            spanned = Html.fromHtml("ЕУЗ(" + product.getCode() + "):</br>" + product.getName());
                        }
                        builder2.setMessage(spanned);
                        final View customLayout = getLayoutInflater().inflate(R.layout.dialog_enter_barcode, null);
                        builder2.setView(customLayout);
                        builder2.setNegativeButton(getString(R.string.cancel), null);
                        builder2.setPositiveButton(getString(R.string.ok), (dialog1, which1) -> {
                            EditText editBarcode = customLayout.findViewById(R.id.editBarcode);
                            EditText editCoefficient = customLayout.findViewById(R.id.editCoefficient);
                            if (!editBarcode.getText().toString().isEmpty()) {
                                presenter.enterBarcode(product.getCode(), editBarcode.getText().toString(),
                                        Integer.parseInt(editCoefficient.getText().toString()));
                            }
                        });
                        AlertDialog dialog2 = builder2.create();
                        if (!dialog2.isShowing()) {
                            dialog2.show();
                        }
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
