package com.madlab.wms.grinfeld.roman.ui.worklist.replenishment

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.Replenishment
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
interface ReplenishmentView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteItemReserve(fromCellTxt: String, toCellTxt: String, list: ArrayList<Replenishment>)

    fun onCompleteReserve()


}