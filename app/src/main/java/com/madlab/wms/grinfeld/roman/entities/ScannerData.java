package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by GrinfeldRa
 */
public class ScannerData implements Parcelable {

    private String count;
    private Date production_date;
    private Date expiration_date;
    private String lootNumber;
    private String barcode;
    private String te;


    public ScannerData(String count, Date production_date, Date expiration_date, String lootNumber, String barcode, String te) {
        this.count = count;
        this.production_date = production_date;
        this.expiration_date = expiration_date;
        this.lootNumber = lootNumber;
        this.barcode = barcode;
        this.te = te;
    }

    public ScannerData(String barcode) {
        this.barcode = barcode;
    }

    private ScannerData(Parcel in) {
        count = in.readString();
        production_date = new Date(in.readLong());
        expiration_date = new Date(in.readLong());
        lootNumber = in.readString();
        barcode = in.readString();
        te = in.readString();
    }

    public static final Creator<ScannerData> CREATOR = new Creator<ScannerData>() {
        @Override
        public ScannerData createFromParcel(Parcel in) {
            return new ScannerData(in);
        }

        @Override
        public ScannerData[] newArray(int size) {
            return new ScannerData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(count);
        dest.writeLong(production_date == null ? 0 : production_date.getTime());
        dest.writeLong(expiration_date == null ? 0 : expiration_date.getTime());
        dest.writeString(lootNumber);
        dest.writeString(barcode);
        dest.writeString(te);
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setProduction_date(Date production_date) {
        this.production_date = production_date;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    public Date getProduction_date() {
        return production_date;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }

    public String getLootNumber() {
        return lootNumber;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getTe() {
        return te;
    }

    public void setTe(String te) {
        this.te = te;
    }
}
