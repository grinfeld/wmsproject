package com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.selection_enter_count

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.entities.selection.OrderItem
import kotlinx.android.synthetic.main.selection_boxes_enter_count_fragment.*
import kotlinx.android.synthetic.main.selection_boxes_enter_count_fragment.et_exist
import kotlinx.android.synthetic.main.selection_boxes_enter_count_fragment.et_need
import android.text.TextWatcher
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.selection.Order
import com.madlab.wms.grinfeld.roman.entities.selection.Weight
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.CLASSIC_SELECTION
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.KEY_TYPE_SELECTION
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlin.math.roundToInt


/**
 * Created by grinfeldra
 */

private const val KEY_ORDER_ITEM = "key_order_item"
private const val KEY_ORDER = "key_order"

class SelectionEnterCountFragment : MvpAppCompatFragment(), KeyEventListener, TextWatcher, IOnScannerEvent {

    private var countPack = 0
    lateinit var item: OrderItem
    lateinit var order: Order
    private var scanner: AbstractScanner? = null

    fun newInstance(orderItem: OrderItem, order: Order) = SelectionEnterCountFragment().apply {
        arguments = Bundle().apply {
            putParcelable(KEY_ORDER_ITEM, orderItem)
            putParcelable(KEY_ORDER, order)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (goodsCard != null && goodsCard.barcode != null && et_count_weight.isVisible) {
            val isWeight = order.listWeightProductBarcode.contains(goodsCard.barcode.substring(0, 2))
            if (isWeight) {
                et_count_weight.setText(Utils.toDouble(goodsCard.barcode.substring(7, 9)
                        .plus(".").plus(goodsCard.barcode.substring(9, 12))).toString())
                et_count_weight.isFocusableInTouchMode = true
                et_count_weight.requestFocus()
                et_count_weight.setSelection(et_count_weight.text.length)
            }
        }
    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.selection_boxes_enter_count_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        item = arguments?.getParcelable<OrderItem>(KEY_ORDER_ITEM)!!
        order = arguments?.getParcelable<Order>(KEY_ORDER)!!
        txt_code_goods.text = item.name
//        var need =
//        if (item.mIsWeightGoods && item.BoxNeed() > 0) {
//            if ((item.mHeadsCount / item.BoxNeed()) > 1)
//                need = String.format("%s%s (%sг=1кор)", item.mNado, item.mBaseMeash, item.mHeadsCount / item.BoxNeed())
//        }
        et_need.text = item.need.toEditable()
        et_exist.text = item.exist.toEditable()
        if (!item.mIsWeightGoods) {
            txt_message.visibility = View.VISIBLE
            txt_message.text = getString(R.string.enter, (item.mNado - item.mEst).toInt(), item.mBaseMeash)
            count_view.visibility = View.VISIBLE
            et_count.requestFocus()
        } else {
            weight_view.visibility = View.VISIBLE
            et_count_head.requestFocus()
            txt_count_weight_fact.text = getString(R.string.enter_weight_fact, item.mEst.toString())
            et_count_weight.addTextChangedListener(this)
            //Если количество головок = 0
            //то это псевдовесовой товар
            if (item.mHeadsCount == 0) {
                txt_message.visibility = View.VISIBLE
                txt_message.text = getString(R.string.need_message, item.need, item.mNado, item.mBaseMeash)
                txt_et_count_head_fact.visibility = View.GONE
                et_count_head.visibility = View.GONE
            } else {//Если количество головок != 0
                txt_et_count_head_fact.text = getString(R.string.enter_head_fact, item.mHeads.toString())
                et_count_head.addTextChangedListener(this)
            }
        }
    }


    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (KeyEvent.KEYCODE_ENTER == keyCode && event?.action == KeyEvent.ACTION_UP) {
            if (activity?.currentFocus?.id == R.id.et_count) {
                if (!TextUtils.isEmpty(et_count.text)) {
                    val add = et_count.text.toString().toDouble()
                    val newCount = item.mEst + add
                    if (newCount.compareTo(item.mNado) <= 0) {
                        item.mEst = newCount
                        item.mMissing = false
                        item.mTE = order.scanTe
                        App.getDatabase().orderSelectionDao().update(item)
                        showDialogPutToBox()
                    } else {
                        showDialog(getString(R.string.error_enter_count))
                    }
                }
            } else if (activity?.currentFocus?.id == R.id.et_count_head) {
                if (!TextUtils.isEmpty(et_count_head.text)) {
                    val headsMax = item.mHeadsCount + (item.mHeadsCount * 0.1f).roundToInt()
                    if (headsMax < item.mHeads + et_count_head.text.toString().toInt()) {
                        showDialog(getString(R.string.error_enter_head, headsMax.toString()))
                    } else {
                        item.mHeads += et_count_head.text.toString().toInt()
                        item.mTE = order.scanTe
                        et_count_head.text.clear()
                    }
                    return true
                } else {
                    App.getDatabase().orderSelectionDao().update(item)
                    showDialogPutToBox()
                }
            } else if (activity?.currentFocus?.id == R.id.et_count_weight) {
                if (!TextUtils.isEmpty(et_count_weight.text)) {
                    val add = et_count_weight.text.toString().toDouble()
                    val newCount = item.mEst + add
                    if (item.mHeadsCount == 0) {
                        val maxW = item.mNado + (item.mNado * (order.mPercentMaxWeight.toFloat() / 100))
                        if (newCount.compareTo(maxW) <= 0) {
                            countPack += 1
                            txt_total_count_weight.text = countPack.toString()
                            item.mEst = newCount
                            order.mWeights.add(Weight(item.mTask, item.code, add))
                            et_count_weight.text.clear()
                        } else {
                            showDialog(getString(R.string.error_enter_weight))
                        }
                    } else {
                        countPack += 1
                        txt_total_count_weight.text = countPack.toString()
                        item.mEst = newCount
                        order.mWeights.add(Weight(item.mTask, item.code, add))
                        et_count_weight.text.clear()
                    }
                    return true
                } else {
                    if (item.mHeadsCount != 0) {
                        if (item.mHeads == 0) {
                            showDialog(getString(R.string.enter_weight_and_not_enter_count_head))
                        } else {
                            item.mTE = order.scanTe
                            App.getDatabase().orderSelectionDao().update(item)
                            App.getDatabase().orderSelectionDao().update(order)
                            showDialogPutToBox()
                        }
                    } else {
                        item.mTE = order.scanTe
                        App.getDatabase().orderSelectionDao().update(item)
                        App.getDatabase().orderSelectionDao().update(order)
                        showDialogPutToBox()
                    }
                }
            }
        }
        return false
    }


    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        when (s.hashCode()) {
            et_count_head.text.hashCode() -> {
                if (!TextUtils.isEmpty(s)) {
                    txt_et_count_head_fact.text = getString(R.string.enter_head_fact, (s.toString().toInt() + item.mHeads).toString())
                } else {
                    txt_et_count_head_fact.text = getString(R.string.enter_head_fact, item.mHeads.toString())
                }
            }
            et_count_weight.text.hashCode() -> {
                if (!TextUtils.isEmpty(s)) {
                    txt_count_weight_fact.text = getString(R.string.enter_weight_fact, (s.toString().toDouble() + item.mEst).toString())
                } else {
                    txt_count_weight_fact.text = getString(R.string.enter_weight_fact, item.mEst.toString())
                }
            }
        }
    }

    private fun showDialogPutToBox() {
        if (arguments?.getByte(KEY_TYPE_SELECTION) == CLASSIC_SELECTION) {
            fragmentManager?.popBackStack()
        } else {
            val alertDialog = context?.let {
                AlertDialog.Builder(it)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.confirmation))
                        .setMessage(getString(R.string.put_to_box_message, item.mBoxNumber.toString()))
                        .setPositiveButton(getString(R.string.ok)) { _, _ -> fragmentManager?.popBackStack() }
            }
            alertDialog?.create()
            alertDialog?.show()
        }
    }

    private fun showDialog(message: String) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.ok), null)
        }
        alertDialog?.create()
        alertDialog?.show()
    }

    private fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)
}