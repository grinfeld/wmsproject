package com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.selection_check

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView

/**
 * Created by grinfeldra
 */
interface SelectionCheckView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteReserveSelectionOrder()

}