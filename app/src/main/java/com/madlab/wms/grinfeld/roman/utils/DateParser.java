package com.madlab.wms.grinfeld.roman.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateParser {

    private static final String TAG = DateParser.class.getSimpleName();

    public static long getDateLongFromString(String s) {
        int year = Integer.parseInt(s.substring(0, 4));
        int month = Integer.parseInt(s.substring(4, 6));
        int day = Integer.parseInt(s.substring(6, 8));
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime().getTime();
    }


    public static Date getDateFromString(String s) throws ParseException {
        SimpleDateFormat df2 = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
        return df2.parse(s);
    }

    public static String getDateFromLong(long l) {
        Date date = new Date(l);
        SimpleDateFormat df2 = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        return df2.format(date);
    }

    public static String getDateToStringFromLongToRequest(long l) {
        Date date = new Date(l);
        SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        return df2.format(date);
    }
}
