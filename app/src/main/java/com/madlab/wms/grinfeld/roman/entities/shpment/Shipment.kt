package com.madlab.wms.grinfeld.roman.entities.shpment

import android.os.Parcelable
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import kotlinx.android.parcel.Parcelize

/**
 * Created by grinfeldra
 *
 *
 * EU2280712;ЗГЗ   +2;332962310;АШАН-Челябинск. Труда 203;
 */
@Parcelize
class Shipment(var te: String, var zone: String, var numDoc: String, var nameOwner: String, var status: Boolean)
    : Parcelable, AbstractData() {


    override fun getLeftText(): String {
        return te
    }

    override fun getRightText(): String {
        return if (status) "Отгружена" else "Не отгружена"
    }

    companion object {
        fun parse(answer: String): ArrayList<Shipment> {
            val split = answer.split("|")
            val result = ArrayList<Shipment>()
            for (value in split) {
                val temp = value.split(";")
                val shipment = Shipment(temp[0], temp[1], temp[2], temp[3], false)
                result.add(shipment)
            }
            return result
        }
    }

}