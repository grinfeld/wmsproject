package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_series

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.InventoryCellProductViewModel
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.InventoryProductList.METHOD_ADD_OR_EDIT
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.InventoryProductList.METHOD_ADD_SERIES
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_editor_count.InventoryEditorCountFragment
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.inventory_list_product.*


/**
 * Created by grinfeldra
 */

private const val KEY = "key"

class InventorySeriesFragment : MvpAppCompatFragment(), AdapterView.OnItemSelectedListener, KeyEventListener, InventorySeriesView {


    @InjectPresenter
    lateinit var presenter: InventorySeriesPresenter

    @ProvidePresenter
    fun providePresenter() = InventorySeriesPresenter()

    private lateinit var inventoryCellProduct: InventoryCellProduct
    private var model: InventoryCellProductViewModel? = null

    fun newInstance(key: String) = InventorySeriesFragment().apply {
        arguments = Bundle().apply {
            putString(KEY, key)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
    }

    override fun onDetach() {
        super.onDetach()
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
    }

    override fun onResume() {
        super.onResume()
        list_view.isFocusable = true
        list_view.requestFocus()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(activity!!).get(InventoryCellProductViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.inventory_list_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val weightLeft = 1f
        val weightRight = 1f
        val listAdapter = MyAdapter<AbstractData>(context)
        model?.liveData?.observe(this, Observer {
            val code = arguments?.getString(KEY)
            val mutableList = it[code]
            mutableList?.forEach { product ->
                product.textForNewSeries = true
            }
            listAdapter.setData(it[code] as List<AbstractData>?)
        })
        Utils.setParamFromHeader(view, weightLeft, weightRight, getString(R.string.date), getString(R.string.count_lbl))
        listAdapter.setWeight(weightLeft, weightRight)
        list_view.adapter = listAdapter
        initListeners()
    }

    private fun initListeners() {
        list_view.onItemSelectedListener = this
        list_view.setOnKeyListener(this)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        inventoryCellProduct = parent?.getItemAtPosition(position) as InventoryCellProduct
        txt_date.text = getString(R.string.date_format, inventoryCellProduct.productionDate, inventoryCellProduct.expirationDate)
        inventoryCellProduct = parent.getItemAtPosition(position) as InventoryCellProduct
        txt_code.text = inventoryCellProduct.codeGoods
        txt_name.text = inventoryCellProduct.goodsName
        chb_is_edit.isChecked = inventoryCellProduct.isEdited
        presenter.initFieldMeasure(inventoryCellProduct)
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER && event?.action == KeyEvent.ACTION_UP) {
            startInventoryEditorCountFragment(inventoryCellProduct, METHOD_ADD_OR_EDIT)
            return true
        } else if (keyCode == KeyEvent.KEYCODE_A && event?.action == KeyEvent.ACTION_UP) {
            if (inventoryCellProduct.inventoryType == Command.TYPE_CELL) {
                showAddNewSessionDialog()
            }
            return true
        } else if (keyCode == KeyEvent.KEYCODE_DEL && event?.action == KeyEvent.ACTION_UP) {
            showDelDialog()
            return true
        } else if (keyCode == KeyEvent.KEYCODE_E && event?.action == KeyEvent.ACTION_UP) {
            inventoryCellProduct.changeSelected()
            presenter.initFieldMeasure(inventoryCellProduct)
            return true
        }
        return false
    }


    private fun showAddNewSessionDialog() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.add_new_series))
                    .setPositiveButton(getString(R.string.yes)) { _, _ -> startInventoryEditorCountFragment(inventoryCellProduct, METHOD_ADD_SERIES) }
                    .setNegativeButton(getString(R.string.no)) { dialog, _ -> dialog.dismiss() }
                    .create()
        }
        if (alertDialog?.isShowing == false) {
            alertDialog.show()
        }
    }

    private fun showDelDialog() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.del_series))
                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                        inventoryCellProduct.countDefective = 0.0
                        inventoryCellProduct.count = 0.0
                        inventoryCellProduct.countFact = 0.0
                        model?.addOrReplace(inventoryCellProduct)
                        presenter.initFieldMeasure(inventoryCellProduct)
                    }
                    .setNegativeButton(getString(R.string.no)) { dialog, _ -> dialog.dismiss() }
                    .create()
        }
        if (alertDialog?.isShowing == false) {
            alertDialog.show()
        }
    }

    private fun startInventoryEditorCountFragment(product: InventoryCellProduct?, method: Int) {
        if (activity != null && product != null) {
            if (method == METHOD_ADD_SERIES) {
                try {
                    val newSeries = product.cloneFromNewSeries()
                    (activity as MainActivity).changeFragment(InventoryEditorCountFragment().newInstance(newSeries, method))
                } catch (e: CloneNotSupportedException) {
                    e.printStackTrace()
                }

            } else {
                (activity as MainActivity).changeFragment(InventoryEditorCountFragment().newInstance(product, method))
            }
        }
    }

    override fun onInitFieldMeasure(countText: String, countDefective: String) {
        txt_count.text = getString(R.string.count_label, countText)
        txt_defective.text = getString(R.string.defective_label, countDefective)
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

}