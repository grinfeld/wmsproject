package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.Order;

import java.util.List;

/**
 * Created by GrinfeldRa
 */
public interface OrderListFragmentView extends BaseView {
    @StateStrategyType(SkipStrategy.class)
    void onCompleteGetNumberOrder(Order order);
    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadOrders(List<Order> orderList);
}
