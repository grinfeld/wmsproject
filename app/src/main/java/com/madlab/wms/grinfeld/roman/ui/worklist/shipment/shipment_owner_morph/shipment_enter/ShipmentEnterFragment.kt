package com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.shipment_enter

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentGoods
import com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.shipment_enter.shipment_goods.ShipmentGoodsFragment
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.shipment_enter_fragment.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by grinfeldra
 */
const val KEY_SHIPMENT = "key_shipment"
const val KEY_DOC_NUM = "doc_num"
const val KEY_DOC_NAME = "doc_name"
const val KEY_DOC_TYPE = "doc_type"

class ShipmentEnterFragment : MvpAppCompatFragment(), ShipmentEnterView, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener, KeyEventListener {

    override fun onComplete() {
        fragmentManager?.popBackStack()
        fragmentManager?.popBackStack()
    }

    @InjectPresenter
    lateinit var presenter: ShipmentEnterPresenter

    @ProvidePresenter
    fun providePresenter() = ShipmentEnterPresenter()

    private var progressOwner: ProgressOwner? = null

    private lateinit var selectedShipmentTE: String

    fun newInstance(shipment: ArrayList<Shipment>, docNum: String,
                    docName: String, docType: Int) = ShipmentEnterFragment().apply {
        arguments = Bundle().apply {
            putParcelableArrayList(KEY_SHIPMENT, shipment)
            putString(KEY_DOC_NUM, docNum)
            putString(KEY_DOC_NAME, docName)
            putInt(KEY_DOC_TYPE, docType)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.shipment_enter_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val data = arguments?.getParcelableArrayList<Shipment>(KEY_SHIPMENT)
        val myAdapter = MyAdapter<Shipment>(context, data)
        Utils.setParamFromHeader(view, "ТЕ", "Статус")
        list_view.adapter = myAdapter
        list_view.onItemSelectedListener = this
        list_view.onItemClickListener = this
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.shipment_title_count,
                "0", data?.size.toString()))
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val shipment = parent?.getItemAtPosition(position) as Shipment
        shipment.status = true
        val myAdapter = parent.adapter as MyAdapter<*>
        myAdapter.notifyDataSetChanged()
        val allItem = myAdapter.allItem as ArrayList<Shipment>
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.shipment_title_count,
                allItem.count { it.status }.toString(), allItem.size.toString()))
        txt4.text = shipment.rightText
        val isCompleteShipment = allItem.size == allItem.count { it.status }
        if (isCompleteShipment) {
            completeDialog(isCompleteShipment, allItem)
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val shipment = parent?.getItemAtPosition(position) as Shipment
        selectedShipmentTE = shipment.te
        txt1.text = shipment.nameOwner
        txt2.text = getString(R.string.te, shipment.te)
        txt3.text = getString(R.string.inventory_cell_num, shipment.zone)
        txt4.text = shipment.rightText
    }

    private fun completeDialog(isCompleteShipment: Boolean, allItem: List<Shipment>) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(if (isCompleteShipment) getString(R.string.complete_shipment) else getString(R.string.cancel_shipment))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        val docType = arguments?.getInt(KEY_DOC_TYPE)
                        val docName = arguments?.getString(KEY_DOC_NAME)
                        val docNum = arguments?.getString(KEY_DOC_NUM)
                        val value = String.format(Locale.US, "%s;%s;%s;", docType, docNum, docName)
                        if (isCompleteShipment) {
                            val param = ArrayList<String>()
                            param.add(value.plus("\n"))
                            for (shipment in allItem) {
                                param.add(String.format(Locale.US, "%s;%s;%s;\n", shipment.te, shipment.numDoc, if (shipment.status) 1 else 0))
                            }
                            presenter.completeShipment(param)
                        } else {
                            presenter.closeShipment(value)
                        }

                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    val allItem = (list_view.adapter as MyAdapter<*>).allItem as List<Shipment>
                    val isCompleteShipment = allItem.size == allItem.count { it.status }
                    completeDialog(isCompleteShipment, allItem)
                }
                true
            }
            KeyEvent.KEYCODE_I -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    if (::selectedShipmentTE.isInitialized) {
                        val docNum = arguments?.getString(KEY_DOC_NUM)
                        presenter.getGoodsFromShipment(selectedShipmentTE, docNum!!)
                    }
                }
                true
            }
            KeyEvent.KEYCODE_P -> {
                if (event?.action == KeyEvent.ACTION_UP) {
                    val alertDialog = context?.let {
                        AlertDialog.Builder(it)
                                .setMessage(getString(R.string.print_te))
                                .setPositiveButton(getString(R.string.ok)) { _, _ ->
                                    val allItem = (list_view.adapter as MyAdapter<*>).allItem as List<Shipment>
                                    val param = ArrayList<String>()
                                    val docNum = arguments?.getString(KEY_DOC_NUM)
                                    param.add(String.format(Locale.US, "%s;%s;0;\n", docNum, docNum))
                                    for (shipment in allItem) {
                                        param.add(String.format(Locale.US, "%s;%s;%s;\n", shipment.numDoc, shipment.te, shipment.leftText))
                                    }
                                    presenter.printShipment(param)
                                }
                                .setNegativeButton(getString(R.string.cancel), null)
                                .create()
                    }
                    if (!alertDialog?.isShowing!!) {
                        alertDialog.show()
                    }
                }
                true
            }
            else -> false
        }
    }

    override fun onCompleteGetGoodsFromShipment(data: java.util.ArrayList<ShipmentGoods>, te: String) {
        (context as BaseFragmentActivity).changeFragment(ShipmentGoodsFragment().newInstance(data, te))
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }
}