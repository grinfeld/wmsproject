package com.madlab.wms.grinfeld.roman.common;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.github.edwnmrtnz.BarcodeScannerHelper;
import com.madlab.wms.grinfeld.roman.dao.AppDatabase;

import androidx.multidex.MultiDexApplication;
import androidx.room.Room;

import io.fabric.sdk.android.Fabric;

/**
 * Created by GrinfeldRa
 */
public class App extends MultiDexApplication {

    private static Context context;
    private static AppDatabase database;

    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        App.context = getApplicationContext();
        BarcodeScannerHelper.newInstance(this);
        database = Room.databaseBuilder(this, AppDatabase.class, "wmsDatabase")
                .allowMainThreadQueries()
                .build();
    }


    public static AppDatabase getDatabase() {
        return database;
    }

    public static Context getAppContext() {
        return App.context;
    }


}
