package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes.checking_validation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.EuzBoxesRoute
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.checking_boxes_validation_fragment.*

/**
 * Created by grinfeldra
 */

const val KEY_DATA = "data"
class CheckingValidationFragment : MvpAppCompatFragment(), AdapterView.OnItemSelectedListener {

    fun newInstance(list: List<EuzBoxesRoute>) = CheckingValidationFragment().apply {
        arguments = Bundle().apply {
            putParcelableArrayList(KEY_DATA, ArrayList(list))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.checking_boxes_validation_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val list = arguments?.getParcelableArrayList<EuzBoxesRoute>(KEY_DATA)!!
        val adapter = MyAdapter<EuzBoxesRoute>(context, list)
        list_view.adapter = adapter
        Utils.setParamFromHeader(view,"Код","№ Короба")
        list_view.onItemSelectedListener = this
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val euzBoxesRoute = parent?.getItemAtPosition(position) as EuzBoxesRoute
        txt_name.text = euzBoxesRoute.nameGoods
    }

}