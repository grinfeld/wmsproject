package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner;
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner;
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent;
import com.madlab.wms.grinfeld.roman.ui.MainActivity;
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_editor_count.InventoryEditorCountFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_series.InventorySeriesFragment;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;


/**
 * Created by GrinfeldRa
 */
public class InventoryProductList extends MvpAppCompatFragment implements IOnScannerEvent,
        AdapterView.OnItemSelectedListener, View.OnClickListener, View.OnKeyListener, KeyEventListener, InventoryProductView {

    public static final int METHOD_ADD_OR_EDIT = 0;
    public static final int METHOD_ADD_SERIES = 1;

    private static final String KEY_LIST_PRODUCTS = "list_products";
    private static final String KEY_TYPE_INVENTORY = "type_inventory";
    private static final String KEY_NUMBER_INVENTORY = "number_inventory";
    private static final String KEY_CREATE_DATE = "create_date";
    private MyAdapter listAdapter;
    private Context context;
    private TextView txt_code, txt_name, txt_date, txt_count, txt_defective;
    private CheckBox chb_is_edit;
    private InventoryCellProduct inventoryCellProduct;
    private ListView listView;
    private ProgressOwner progressOwner;
    private AbstractScanner scanner;
    private InventoryCellProductViewModel model;
    private int typeInventory;

    @InjectPresenter
    InventoryProductPresenter presenter;

    @ProvidePresenter
    InventoryProductPresenter providePresenter() {
        return new InventoryProductPresenter();
    }

    public static InventoryProductList newInstance(ArrayList<InventoryCellProduct> list,
                                                   String number_inventory, String create_date,
                                                   int typeInventory) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_LIST_PRODUCTS, list);
        args.putString(KEY_CREATE_DATE, create_date);
        args.putString(KEY_NUMBER_INVENTORY, number_inventory);
        args.putInt(KEY_TYPE_INVENTORY, typeInventory);
        InventoryProductList fragment = new InventoryProductList();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).addKeyEventHandler(this);
        }
        if (context instanceof ProgressOwner) {
            progressOwner = (ProgressOwner) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).removeKeyEventHandler(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (listView != null) {
            listView.setFocusable(true);
            listView.requestFocus();
        }
        scanner = new FactoryScanner().getScanner(context, this);
        scanner.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            model = ViewModelProviders.of(getActivity()).get(InventoryCellProductViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.inventory_list_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = view.findViewById(R.id.list_view);
        txt_code = view.findViewById(R.id.txt_code);
        txt_name = view.findViewById(R.id.txt_name);
        txt_date = view.findViewById(R.id.txt_date);
        chb_is_edit = view.findViewById(R.id.chb_is_edit);
        txt_count = view.findViewById(R.id.txt_count);
        txt_defective = view.findViewById(R.id.txt_defective);
        FloatingActionButton floatingActionButton = view.findViewById(R.id.fb_next);
        floatingActionButton.setOnClickListener(this);
        if (getArguments() != null) {
            typeInventory = getArguments().getInt(KEY_TYPE_INVENTORY);
            ArrayList<InventoryCellProduct> inventoryCellProducts1 = getArguments().getParcelableArrayList(KEY_LIST_PRODUCTS);
            if (inventoryCellProducts1 != null) {
                if (model.getLiveData().getValue() == null || model.getLiveData().getValue().size() == 0) {
                    for (InventoryCellProduct product : inventoryCellProducts1) {
                        model.addOrNewSeries(product);
                    }
                }
            }
            if (getActivity() != null) {
                if (typeInventory == Command.TYPE_CELL && inventoryCellProducts1 != null && inventoryCellProducts1.size() > 0) {
                    ((BaseFragmentActivity) getActivity()).setActionBarTitle(inventoryCellProducts1.get(0).getNumberCell());
                }
            }
            float weightLeft = 0.5f, weightRight = 1;
            listAdapter = new MyAdapter<>(context);
            model.getLiveData().observe(getViewLifecycleOwner(), inventoryCellProducts -> {
                List<InventoryCellProduct> data = new ArrayList<>();
                for (Map.Entry<String, List<InventoryCellProduct>> entry : inventoryCellProducts.entrySet()) {
                    List<InventoryCellProduct> value = entry.getValue();
                    InventoryCellProduct product = value.get(0);
                    product.textForNewSeries = false;
                    data.add(product);
                }
                listAdapter.setData(data);
            });
            Utils.setParamFromHeader(view, weightLeft, weightRight, typeInventory == Command.TYPE_CELL
                            ? getString(R.string.code) : getString(R.string.cell),
                    typeInventory == Command.TYPE_CELL ? getString(R.string.name) : getString(R.string.count_lbl));
            listAdapter.setWeight(weightLeft, weightRight);
            listView.setAdapter(listAdapter);
            listView.setOnItemSelectedListener(this);
            listView.setOnKeyListener(this);
        }
    }


    @Override
    public void onDataScanner(ScannerData goodsCard) {
        presenter.getBailorProductFromBarcode(goodsCard.getBarcode(), model);
    }

    @Override
    public void onStatusUpdateScanner(String scanStatus) {

    }

    @Override
    public void onErrorScanner(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        inventoryCellProduct = (InventoryCellProduct) parent.getItemAtPosition(position);
        txt_code.setText(inventoryCellProduct.getCodeGoods());
        txt_name.setText(inventoryCellProduct.getGoodsName());
        int size = 0;
        Map<String, List<InventoryCellProduct>> value = model.getLiveData().getValue();
        if (value != null) {
            String key = Command.TYPE_CELL == typeInventory ? inventoryCellProduct.getCodeGoods() : inventoryCellProduct.getAdressCell();
            List<InventoryCellProduct> inventoryCellProducts = value.get(key);
            if (inventoryCellProducts != null) {
                size = inventoryCellProducts.size();
            }
        }
        txt_date.setText(size > 1 ? getString(R.string.series_count, size) :
                getString(R.string.date_format, inventoryCellProduct.getProductionDate(), inventoryCellProduct.getExpirationDate()));
        chb_is_edit.setChecked(inventoryCellProduct.isEdited());
        presenter.initFieldMeasure(inventoryCellProduct, model);
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fb_next) {
            startInventoryEditorCountFragment(inventoryCellProduct);
        }
    }

    private void startInventoryEditorCountFragment(InventoryCellProduct product) {
        if (getActivity() != null && product != null) {
            String key = Command.TYPE_CELL == typeInventory ? product.getCodeGoods() : product.getAdressCell();
            List<InventoryCellProduct> listInventorySeries = getListInventorySeries(key);
            if (listInventorySeries != null && listInventorySeries.size() > 1) {
                ((MainActivity) getActivity()).changeFragment(new InventorySeriesFragment().newInstance(key));
            } else {
                ((MainActivity) getActivity()).changeFragment(new InventoryEditorCountFragment().newInstance(product, METHOD_ADD_OR_EDIT));
            }
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
            startInventoryEditorCountFragment(inventoryCellProduct);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            showCompleteCellInventoryDialog();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_A && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (typeInventory == Command.TYPE_CELL) {
                showAddNewSessionDialog();
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
            showDelDialog();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_E && event.getAction() == KeyEvent.ACTION_DOWN) {
            inventoryCellProduct.changeSelected();
            presenter.initFieldMeasure(inventoryCellProduct, model);
            return true;
        }
        return false;
    }


    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }

    @Override
    public void onCompleteReserveCellInventory() {
        if (getActivity() != null) {
            model.clearData();
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onCompleteGetProduct(InventoryCellProduct product) {
        startInventoryEditorCountFragment(product);
    }

    @Override
    public void onInitFieldMeasure(String textCount, String textCountDefective) {
        txt_count.setText(getString(R.string.count_label, textCount));
        txt_defective.setText(getString(R.string.defective_label, textCountDefective));
    }

    @Override
    public void onScannerExistProduct(List<InventoryCellProduct> listInventorySeries) {
        if (listInventorySeries != null)
            startInventoryEditorCountFragment(listInventorySeries.get(0));
    }

    @Override
    public void onPause() {
        super.onPause();
        scanner.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        scanner.onStop();
    }

    private List<InventoryCellProduct> getListInventorySeries(String codeGoods) {
        Map<String, List<InventoryCellProduct>> value = model.getLiveData().getValue();
        if (value != null) {
            List<InventoryCellProduct> inventoryCellProducts = value.get(codeGoods);
            if (inventoryCellProducts != null) {
                return inventoryCellProducts;
            }
        }
        return null;
    }

    private void showDelDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.confirmation))
                .setMessage(getString(R.string.del_goods))
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                    String key = inventoryCellProduct.getInventoryType() == Command.TYPE_CELL ?
                            inventoryCellProduct.getCodeGoods() : inventoryCellProduct.getAdressCell();
                    model.clearProductAndAllSeries(key);
                    presenter.initFieldMeasure(inventoryCellProduct, model);
                })
                .setNegativeButton(getString(R.string.no), (dialog, which) ->
                        dialog.dismiss())
                .create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    private void showAddNewSessionDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.confirmation))
                .setMessage(getString(R.string.add_new_series))
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                            try {
                                InventoryCellProduct newSeries = inventoryCellProduct.cloneFromNewSeries();
                                if (getActivity() != null)
                                    ((MainActivity) getActivity()).changeFragment(new InventoryEditorCountFragment().newInstance(newSeries, METHOD_ADD_SERIES));
                            } catch (CloneNotSupportedException e) {
                                e.printStackTrace();
                            }
                        }
                )
                .setNegativeButton(getString(R.string.no), (dialog, which) -> {
                    dialog.dismiss();
                })
                .create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    private void showCompleteCellInventoryDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.confirmation))
                .setMessage(getString(R.string.close_inventory))
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                            if (getArguments() != null) {
                                presenter.reserveCellInventory(getArguments().getString(KEY_NUMBER_INVENTORY),
                                        getArguments().getString(KEY_CREATE_DATE), model.getLiveData().getValue(),
                                        getArguments().getInt(KEY_TYPE_INVENTORY));
                            }
                        }
                )
                .setNeutralButton(getString(R.string.cancel), null)
                .setNegativeButton(getString(R.string.no), (dialog, which) -> {
                    if (getActivity() != null) {
                        model.clearData();
                        getActivity().onBackPressed();
                    }
                })
                .create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }


}
