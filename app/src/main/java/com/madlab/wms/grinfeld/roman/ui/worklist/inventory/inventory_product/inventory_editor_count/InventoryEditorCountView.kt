package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_editor_count

import android.view.View
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView

/**
 * Created by GrinfeldRa
 */
interface InventoryEditorCountView : BaseView{
    @StateStrategyType(SkipStrategy::class)
    fun onDeleteCountProduct(v: View)
    @StateStrategyType(SkipStrategy::class)
    fun onInitFieldsMeasure(accepted: String, acceptedDefective: String)
}