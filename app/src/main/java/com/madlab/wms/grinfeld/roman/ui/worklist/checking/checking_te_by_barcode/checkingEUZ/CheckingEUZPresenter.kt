package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode.checkingEUZ

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.EUZRoute
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.Route
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class CheckingEUZPresenter : BasePresenter<CheckingEUZView>() {

    fun getEuzFromRoute(numRoute: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_EUZ_FROM_ROUTE(numRoute))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val euzRoutes = EUZRoute.parse(it)
                        val dbRoute: Route? = App.getDatabase().checkingDao().getRoute(numRoute)
                        val iterator = euzRoutes.iterator()
                        while (iterator.hasNext()) {
                            val next = iterator.next()
                            val find = dbRoute?.euzRoutes?.find { eR -> eR == next }
                            if (find != null) {
                                iterator.remove()
                            }
                        }
                        if (euzRoutes.size > 0) {
                            viewState.onCompleteReload(euzRoutes)
                        } else {
                            viewState.showError("Новых ЕУЗ не найдено")
                        }

                    }
                })
    }

    fun countChange(countBox: Int) {
        viewState.onCountChange(countBox)
    }

}