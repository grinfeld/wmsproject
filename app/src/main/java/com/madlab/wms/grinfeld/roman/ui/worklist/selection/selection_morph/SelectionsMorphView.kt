package com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView

/**
 * Created by grinfeldra
 */
interface SelectionsMorphView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onFirstViewAttach()

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteReserveSelectionOrder()

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteCheckTe(te: String)

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteSendPart()
}