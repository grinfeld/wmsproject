package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.send_to_print_euz;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.entities.Product;

/**
 * Created by GrinfeldRA
 */
public class SendToPrintFragment extends MvpAppCompatFragment implements KeyEventListener, SendToPrintView {

    private static final String ORDER_KEY = "order";
    private static final String PRODUCT_KEY = "product";
    private EditText et_coefficient,
            et_count_sticker;
    private Order order;
    private Product product;
    private Context context;
    private ProgressOwner progressOwner;

    @InjectPresenter
    SendToPrintPresenter presenter;

    @ProvidePresenter
    SendToPrintPresenter providePresenter() {
        return new SendToPrintPresenter();
    }

    public static SendToPrintFragment newInstance(Order order, Product product) {
        Bundle args = new Bundle();
        args.putParcelable(ORDER_KEY, order);
        args.putParcelable(PRODUCT_KEY, product);
        SendToPrintFragment fragment = new SendToPrintFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).addKeyEventHandler(this);
        }
        if (context instanceof ProgressOwner) {
            progressOwner = ((ProgressOwner) context);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            ((BaseFragmentActivity) getActivity()).setActionBarTitle(getString(R.string.print_fragment_label));
        }
        return inflater.inflate(R.layout.send_to_print_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TextView txt_code = view.findViewById(R.id.txt_code);
        TextView txt_name = view.findViewById(R.id.txt_name);
        et_coefficient = view.findViewById(R.id.et_coefficient);
        et_count_sticker = view.findViewById(R.id.et_count_sticker);
        if (getArguments() != null) {
            order = getArguments().getParcelable(ORDER_KEY);
            product = getArguments().getParcelable(PRODUCT_KEY);
            txt_code.setText(getString(R.string.print_fragment_code, order.getName()));
            txt_name.setText(product.getName());
        }
    }


    @Override
    public void onDetach() {
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).removeKeyEventHandler(this);
        }
        super.onDetach();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (KeyEvent.ACTION_DOWN == event.getAction() && KeyEvent.KEYCODE_ENTER == event.getKeyCode() && isNotEmptyFields()) {
            presenter.sentToPrint(order.getShortName(),
                    Integer.parseInt(et_coefficient.getText().toString()),
                    Integer.parseInt(et_count_sticker.getText().toString()));
            return true;
        }
        return false;
    }


    private boolean isNotEmptyFields() {
        return !TextUtils.isEmpty(et_coefficient.getText()) &&
                !TextUtils.isEmpty(et_count_sticker.getText());
    }

    @Override
    public void onCompleteSentToPrint() {
        Toast.makeText(context, getString(R.string.print_fragment_complete), Toast.LENGTH_SHORT).show();
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }
}
