package com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.madlab.wms.grinfeld.roman.entities.converter.DataConverter

/**
 * Created by grinfeldra
 */
@Entity
data class Route (@PrimaryKey(autoGenerate = true) var id: Long?,
            var docNum: String, var name: String) {

    @TypeConverters(DataConverter::class)
    var euzRoutes = java.util.ArrayList<EUZRoute>()

    constructor() : this(null, "", "")

    override fun toString(): String {
        return name
    }


    companion object {
        fun parseAnswer(answer: String): ArrayList<Route> {
            val split = answer.split("|")
            val result = ArrayList<Route>()
            for (value in split) {
                val temp = value.split(";")
                val route = Route()
                route.docNum = temp[0]
                route.name = temp[1]
                result.add(route)
            }
            return result
        }
    }

}
