package com.madlab.wms.grinfeld.roman.entities.shpment

import com.madlab.wms.grinfeld.roman.utils.Utils

/**
 * Created by grinfeldra
 */
class ShipmentOwner(var type: Int, var code: String, var name: String, var date: String) {

    override fun toString(): String {
        return name
    }

    companion object{
        fun parse(answer: String):List<ShipmentOwner>{
            val split = answer.split("|")
            val result = ArrayList<ShipmentOwner>()
            for (value in split) {
                val temp = value.split(";")
                val shipment = ShipmentOwner(Utils.toInt(temp[0]), temp[1], temp[2], temp[3])
                result.add(shipment)
            }
            return result
        }
    }

}