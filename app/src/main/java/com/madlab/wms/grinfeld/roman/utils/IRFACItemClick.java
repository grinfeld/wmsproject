package com.madlab.wms.grinfeld.roman.utils;

import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;

/**
 * Created by grinfeldra
 */
public interface IRFACItemClick {

    void onClickRFA(int position, RFACLabelItem item);

}
