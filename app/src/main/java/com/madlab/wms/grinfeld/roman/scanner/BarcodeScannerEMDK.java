package com.madlab.wms.grinfeld.roman.scanner;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;


public class BarcodeScannerEMDK extends AbstractScanner implements EMDKManager.EMDKListener, Scanner.StatusListener, Scanner.DataListener {
    // Declare variables to call interface class
    private static final int I_ON_DATA = 0;
    private static final int I_ON_STATUS = 1;
    private static final int I_ON_ERROR = 2;

    private static EMDKManager emdkManager = null;  // Declare variable to store EMDKManager object
    private static BarcodeManager barcodeManager; // Declare variable to store BarcodeManager object
    private static Scanner scanner = null; //Declare variable to hold scanner device to scann

    private static IOnScannerEvent mUIactivity = null; // Declare scanner event listener for UI activity
    private static Handler mScanHandler; // Handler to call listener
    private static IOnScannerEventRunnable mEventRunnable; // Runnable will be called on mScanHandler
    private static BarcodeScannerEMDK mBarcodeScanner; // Declare variable to store BarcodeScanner object

    private final static String TAG = "[Z] BarcodeScanner - "; //TAG for logging

    // Entry point - passing context of UI activity
    // Constructor context of BarcodeScanner
    public static BarcodeScannerEMDK getInstance(Context context, IOnScannerEvent iOnScannerEvent) {
        if (mBarcodeScanner == null) {
            mBarcodeScanner = new BarcodeScannerEMDK(context); // call BarcodeScanner() and returns BarcodeScanner object()
            mUIactivity = iOnScannerEvent;
            //i.e.: com.BarcodeClassSample.BarcodeScanner@7594390
        }
        return mBarcodeScanner;
    }


    @Override
    public void onResume() {
        registerUIobject(mUIactivity);
    }

    @Override
    public void onPause() {
        unregisterUIobject();
    }

    @Override
    public void onStop() {
        deInitScanner();
        releaseEmdk();
    }


    //get EMDKManager with context of UI activity
    //i.e.: com.BarcodeClassSample.Main1Activity@c7b924
    private BarcodeScannerEMDK(Context context) {
        // The EMDKManager object will be created and returned in the callback.
        EMDKResults results = EMDKManager.getEMDKManager(context, this);
        // Check the return status of getEMDKManager
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            System.out.println(TAG + "EMDKManager Request Failed");
        }
        // create Handler to move data from Scanner object to an object on the UI thread
        // i.e.: Handler (android.os.Handler) {beeae42}
        mScanHandler = new Handler(Looper.getMainLooper());
        //create runnable object
        //i.e.: com.BarcodeClassSample.BarcodeScanner$IOnScannerEventRunnable@6c47e53
        mEventRunnable = new IOnScannerEventRunnable();
    }

    //Method to release the EMDK manager
    public static void releaseEmdk() {
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
        mBarcodeScanner = null;
    }

    private void registerUIobject(IOnScannerEvent UIactivity) {
        mUIactivity = UIactivity;
    }

    private void unregisterUIobject() {
        mUIactivity = null;
    }

    //This method is called automatically whenever the EMDKmanager is initialised
    @Override
    public void onOpened(EMDKManager emdkManager) {
        BarcodeScannerEMDK.emdkManager = emdkManager;
        //Method calls to initialize and set decoder parameters of scanner
        initializeScanner();
        setScannerParameters();
    }

    //Called when the barcode is scanned
    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if (scanDataCollection != null
                && scanDataCollection.getResult() == ScannerResults.SUCCESS) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            if (scanData != null && scanData.size() > 0) {
                final ScanDataCollection.ScanData data = scanData.get(0);
                callIOnScannerEvent(I_ON_DATA, data.getData(), null);
            }
        }
    }

    //Called when the status of the scanner is changed
    //i.e.: com.symbol.emdk.barcode.StatusData@6803045
    @Override
    public void onStatus(StatusData statusData) {
        String statusStr = "";
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE: //Scanner is IDLE - this is when to request a read
                statusStr = "Scanner enabled and idle";
                try {
                    if (scanner.isEnabled() && !scanner.isReadPending()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    }
                } catch (ScannerException e) {
                    System.out.println(TAG + "onStatus() - ScannerException " + e.getMessage());
                    e.printStackTrace();
                    statusStr = e.getMessage();
                }
                break;
            case SCANNING: //Scanner is SCANNING
                statusStr = "Scanner beam is on, aim at the barcode";
                break;
            case WAITING: //Scanner is waiting for trigger press
                statusStr = "Waiting for trigger, press to scan barcode";
                break;
            case DISABLED: //Scanner is disabled
                statusStr = "Scanner is not enabled";
                break;
            case ERROR: //Error occurred
                statusStr = "Error occurred during scanning";
                break;
        }
        //Return result to populate UI thread
        callIOnScannerEvent(I_ON_STATUS, null, statusStr);
    }

    //Clean up objects created by EMDK manager, if EMDK was closed abruptly.
    @Override
    public void onClosed() {
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
        mBarcodeScanner = null;
    }

    //Method to initialize, addAll listeners and enable scanner
    private void initializeScanner() {
        try {
            //Get instance of the Barcode Manager object
            barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
            //Using the default scanner device to scan barcodes
            scanner = barcodeManager.getDevice(BarcodeManager.DeviceIdentifier.DEFAULT);
            //Add data and status listeners
            scanner.addDataListener(this);
            scanner.addStatusListener(this);
            scanner.triggerType = Scanner.TriggerType.HARD; //The trigger type is set to HARD by default
            scanner.enable(); //Enable the scanner
        } catch (ScannerException e) {
            System.out.println(TAG + "initializeScanner() - ScannerException " + e.getMessage());
            e.printStackTrace();
        } catch (Exception ex) {
            System.out.println(TAG + "initializeScanner() - Exception " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    //Method to set decoder parameters in the ScannerConfig object
    private void setScannerParameters() {
        try {
            ScannerConfig config = scanner.getConfig();
            config.decoderParams.aztec.enabled = true;
            config.decoderParams.australianPostal.enabled = true;
            config.decoderParams.canadianPostal.enabled = true;
            config.decoderParams.chinese2of5.enabled = true;
            config.decoderParams.codaBar.enabled = true;
            config.decoderParams.code11.enabled = true;
            config.decoderParams.code128.enabled = true;
            config.decoderParams.code39.enabled = true;
            config.decoderParams.code93.enabled = true;
            config.decoderParams.compositeAB.enabled = true;
            config.decoderParams.compositeC.enabled = true;
            config.decoderParams.d2of5.enabled = true;
            config.decoderParams.dataMatrix.enabled = true;
            config.decoderParams.dutchPostal.enabled = true;
            config.decoderParams.ean13.enabled = true;
            config.decoderParams.ean8.enabled = true;
            config.decoderParams.gs1Databar.enabled = true;
            config.decoderParams.gs1DatabarLim.enabled = true;
            config.decoderParams.gs1DatabarExp.enabled = true;
            config.decoderParams.i2of5.enabled = true;
            config.decoderParams.japanesePostal.enabled = true;
            config.decoderParams.korean3of5.enabled = true;
            config.decoderParams.matrix2of5.enabled = true;
            config.decoderParams.maxiCode.enabled = true;
            config.decoderParams.microPDF.enabled = true;
            config.decoderParams.microQR.enabled = true;
            config.decoderParams.msi.enabled = true;
            config.decoderParams.pdf417.enabled = true;
            config.decoderParams.qrCode.enabled = true;
            config.decoderParams.signature.enabled = true;
            config.decoderParams.tlc39.enabled = true;
            config.decoderParams.triOptic39.enabled = true;
            config.decoderParams.ukPostal.enabled = true;
            config.decoderParams.upca.enabled = true;
            config.decoderParams.upce0.enabled = true;
            config.decoderParams.upce1.enabled = true;
            config.decoderParams.us4StateFics.enabled = true;
            config.decoderParams.us4State.enabled = true;
            config.decoderParams.usPlanet.enabled = true;
            config.decoderParams.usPostNet.enabled = true;
            config.decoderParams.webCode.enabled = true;
            config.decoderParams.mailMark.enabled = true;
            config.decoderParams.hanXin.enabled = true;
            scanner.setConfig(config);
        } catch (ScannerException e) {
            System.out.println(TAG + "setScannerParameters() - ScannerException " + e.getMessage());
            e.printStackTrace();
            callIOnScannerEvent(I_ON_ERROR, null, "setScannerParameters() - ScannerException " + e.getMessage());
        } catch (RuntimeException e) {
            e.printStackTrace();
            System.out.println(TAG + "setScannerParameters() - RuntimeException " + e.getMessage());
            callIOnScannerEvent(I_ON_ERROR, null, "setScannerParameters() - RuntimeException " + e.getMessage());
        }
    }

    //Method to de-initialize scanner
    public static void deInitScanner() {
        if (scanner != null) {
            try {
                if (scanner.isReadPending()) {
                    scanner.cancelRead();
                }
                scanner.disable();
                scanner.removeDataListener(mBarcodeScanner);
                scanner.removeStatusListener(mBarcodeScanner);
                scanner.release();
            } catch (ScannerException e) {
                System.out.println(TAG + "deInitScanner() - ScannerException " + e.getMessage());
                e.printStackTrace();
            }
            scanner = null;
        }

        //Release instance of barcodeManager object
        if (barcodeManager != null) {
            barcodeManager = null;
        }
    }

    private void callIOnScannerEvent(int interfaceId, String data, String status) {
        if (mUIactivity != null) {
            mEventRunnable.setDetails(interfaceId, data, status);
            mScanHandler.post(mEventRunnable);
        }
    }

    private static class IOnScannerEventRunnable implements Runnable {
        private int mInterfaceId = 0;
        private String mBarcodeData = "";
        private String mBarcodeStatus = "";

        public void setDetails(int id, String data, String statusStr) {
            mInterfaceId = id;
            mBarcodeData = data;
            mBarcodeStatus = statusStr;
        }

        @Override
        public void run() {
            if (mUIactivity != null) {
                switch (mInterfaceId) {
                    case I_ON_DATA:
                        mUIactivity.onDataScanner(BarcodeParseMetaData.parseMetadata(mBarcodeData));
                        break;
                    case I_ON_STATUS:
                        mUIactivity.onStatusUpdateScanner(mBarcodeStatus);
                        break;
                    case I_ON_ERROR:
                        mUIactivity.onErrorScanner(mBarcodeStatus);
                        break;
                }
            }
        }
    }
}
