package com.madlab.wms.grinfeld.roman.ui.worklist.placement

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell
import com.madlab.wms.grinfeld.roman.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class PlacementPresenter : BasePresenter<PlacementView>() {

    fun checkTE(te: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_CHECK_PLACEMENT_TE(te))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val split = it.split(";")
                        val cellTxt = split[1]
                        val cellNum = split[2]
                        viewState.onCompleteCheckTE(te, cellTxt, cellNum)
                    }
                })
    }

    fun placementCell(te: String, cell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_PLACEMENT_CELL(te, cell))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.showError("Успешно размещено")
                        viewState.onCompletePlacementCell()
                    }
                })
    }

    fun startPlacementCell(cell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_BLOCK_PLACEMENT_CELL(cell))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val cellTxt = it.split(";")[1]
                        viewState.onCompleteStartPlacementCell(cell, cellTxt)
                    }
                })
    }

    fun getPlacementCell(cell: String, timestamp: String, cellTxt: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_PLACEMENT_CELL(cell))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val placementCell = PlacementCell.parseAnswer(it, cell, timestamp, cellTxt)
                        viewState.onCompleteGetPlacementCell(placementCell)
                    }
                })
    }

    fun getInfoEuz(placementCell: PlacementCell, cell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_INFO_EUZ(cell, placementCell.code))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val split = it.split(";")
                        val owner = split[9]
                        val koef = Utils.toDouble(split[16])
                        val ei2 = split[17]
                        placementCell.owner = owner
                        placementCell.koef = koef
                        placementCell.ei2 = ei2
                        getCellEmpty(placementCell)
                    }
                })
    }

    fun getCellEmpty(placementCell: PlacementCell) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_CELL_EMPTY(placementCell.code, placementCell.owner, ""))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val cellEmptyList = ArrayList<String>()
                        it.split("|").forEach { it1 ->
                            val split = it1.split(";")
                            cellEmptyList.add(split[1])
                        }
                        viewState.onCompleteLoadDataAfterOnItemClick(placementCell, cellEmptyList)
                    }
                })
    }

    fun getInfoEuz(code: String, cell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_INFO_EUZ_FROM_CODE(cell, code))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val code = it.split(";")[0]
                        viewState.onCompleteGetInfoEuz(code)
                    }
                })
    }


}