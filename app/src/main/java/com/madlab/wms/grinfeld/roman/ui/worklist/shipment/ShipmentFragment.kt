package com.madlab.wms.grinfeld.roman.ui.worklist.shipment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.ShipmentMorphFragment
import com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.shipment_enter.ShipmentEnterFragment
import kotlinx.android.synthetic.main.shipment_fragment.*
import java.util.ArrayList

/**
 * Created by grinfeldra
 */


class ShipmentFragment : MvpAppCompatFragment(), ShipmentView, IOnScannerEvent, AdapterView.OnItemClickListener {

    @InjectPresenter
    lateinit var presenter: ShipmentPresenter

    @ProvidePresenter
    fun providePresenter() = ShipmentPresenter()

    private var scanner: AbstractScanner? = null
    private var progressOwner: ProgressOwner? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.shipment_title))
        return inflater.inflate(R.layout.shipment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ArrayAdapter(context, android.R.layout.select_dialog_item,
                resources.getStringArray(R.array.type_shipment))
        list_view.adapter = adapter
        list_view.onItemClickListener = this
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        (activity as BaseFragmentActivity).changeFragment(ShipmentMorphFragment().newInstance(position))
    }

    override fun completeLoadItemInfo(data: ArrayList<Shipment>, docNum: String, nameOwner: String) {
        (activity as BaseFragmentActivity).changeFragment(ShipmentEnterFragment().newInstance(data, docNum, nameOwner, 0))
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (goodsCard?.barcode != null) {
            presenter.loadItem(goodsCard.barcode)
        } else {
            showError(getString(R.string.error_route_barcode))
        }
    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

}