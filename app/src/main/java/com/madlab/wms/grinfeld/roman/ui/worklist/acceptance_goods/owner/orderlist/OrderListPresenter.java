package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist;

import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.entities.parser.OrderParser;
import com.madlab.wms.grinfeld.roman.utils.DateParser;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by GrinfeldRa
 */
@InjectViewState
public class OrderListPresenter extends BasePresenter<OrderListFragmentView> {


    void loadOrders(String code, int type) {
        mCompositeDisposable.add(new WMSClient().send(Command.COM_ORDERS(type, code))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteLoadOrders(OrderParser.getList(response, code));
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }


    void getNumberOrder(int type, Order order) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_GET_NUMBER_ORDER(type, order.getShortName(), order.getName(),
                        DateParser.getDateToStringFromLongToRequest(order.getDate())))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        String[] split = response.split(";");
                        order.setNumDoc(split[1]);
                        order.setDateDoc(split[2]);
                        getViewState().onCompleteGetNumberOrder(order);
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

}
