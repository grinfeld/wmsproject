package com.madlab.wms.grinfeld.roman.client;

import android.net.Uri;
import android.os.Environment;

import com.hierynomus.msdtyp.AccessMask;
import com.hierynomus.msfscc.fileinformation.FileStandardInformation;
import com.hierynomus.mssmb2.SMB2CreateDisposition;
import com.hierynomus.smbj.auth.AuthenticationContext;
import com.hierynomus.smbj.connection.Connection;
import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.share.DiskShare;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.EnumSet;

import io.reactivex.Observable;

/**
 * Created by GrinfeldRa
 */
public class SMBClient {

    private static final String FILE_VERSION = "version.txt";
    private static final String FILE_APK = "app-release.apk";
    private static final String HOST_NAME = "10.0.150.227";
    private static final String USER_NAME = "MtradeBot";
    private static final String PASSWORD = "tobedartm12";
    private static final String SHARE_NAME = "RT";
    public static final String ACTUAL_VERSION = "actual_version";

    public Observable<String> updateApk = Observable.create(o -> {
        Utils.isOnline();
        com.hierynomus.smbj.SMBClient client = new com.hierynomus.smbj.SMBClient();
        try (Connection connection = client.connect(HOST_NAME)) {
            AuthenticationContext ac = new AuthenticationContext(USER_NAME, PASSWORD.toCharArray(), "");
            Session session = connection.authenticate(ac);
            try (DiskShare share = (DiskShare) session.connectShare(SHARE_NAME)) {
                com.hierynomus.smbj.share.File versionFile =
                        share.openFile(FILE_VERSION, EnumSet.of(AccessMask.GENERIC_ALL), null, null, SMB2CreateDisposition.FILE_OPEN, null);
                String versionApp;
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(versionFile.getInputStream()))) {
                    versionApp = bufferedReader.readLine();
                }
                if (!versionApp.equals(Utils.getVersionName())) {
                    com.hierynomus.smbj.share.File apkRemoteFile =
                            share.openFile(FILE_APK, EnumSet.of(AccessMask.GENERIC_ALL), null, null, SMB2CreateDisposition.FILE_OPEN, null);
                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root + "/wms/");
                    if (!myDir.exists()) {
                        myDir.mkdir();
                    }
                    File dest = new File(myDir, apkRemoteFile.getFileName());
                    long offset = 0;
                    long remaining = apkRemoteFile.getFileInformation(FileStandardInformation.class).getEndOfFile();
                    int maxReadSize = share.getTreeConnect().getSession().getConnection().getNegotiatedProtocol().getMaxReadSize();
                    byte[] buffer = new byte[maxReadSize];
                    try (FileOutputStream out = new FileOutputStream(dest)) {
                        while (remaining > 0) {
                            int amount = remaining > buffer.length ? buffer.length : (int) remaining;
                            int amountRead = apkRemoteFile.read(buffer, offset, 0, amount);
                            if (amountRead == -1) {
                                remaining = 0;
                            } else {
                                out.write(buffer, 0, amountRead);
                                remaining -= amountRead;
                                offset += amountRead;
                            }
                        }
                    }
                    o.onNext(Uri.fromFile(dest).toString());
                } else {
                    o.onNext(ACTUAL_VERSION);
                }
                o.onComplete();
            }
        } catch (Exception e) {
            o.onError(e);
        }
    });

}
