package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.Route

/**
 * Created by grinfeldra
 */
interface RoutesView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteGetEuzFromRoute(routeNum: String)

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteLoadListRoute(routes: List<Route>)
}