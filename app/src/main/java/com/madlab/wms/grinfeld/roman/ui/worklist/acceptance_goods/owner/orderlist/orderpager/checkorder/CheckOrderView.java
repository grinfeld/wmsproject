package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.Product;

import java.util.ArrayList;

/**
 * Created by GrinfeldRa
 */
interface CheckOrderView extends BaseView {
    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadGoods(ArrayList<Product> data);
    @StateStrategyType(SkipStrategy.class)
    void onCompleteEnterBarcode();

}
