package com.madlab.wms.grinfeld.roman.ui.worklist;

import androidx.fragment.app.Fragment;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.InventoryCell;
import com.madlab.wms.grinfeld.roman.entities.Zone;

import java.util.List;

/**
 * Created by GrinfeldRa
 */
public interface WorkListView extends BaseView {
    @StateStrategyType(SkipStrategy.class)
    void openWorkFragment(Fragment fragment);

    @StateStrategyType(SkipStrategy.class)
    void openPlacementDefectiveDialog(String timestamp);

    @StateStrategyType(SkipStrategy.class)
    void openZoneList(List<Zone> zones);

    @StateStrategyType(SkipStrategy.class)
    void onCompleteLoadInventoryEUZ(List<InventoryCell> listInventoryCell);

    @StateStrategyType(SkipStrategy.class)
    void openCheckDialog();

}
