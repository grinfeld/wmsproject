package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_te_by_barcode.checkingEUZ

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.dao.CheckingDao
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.EUZRoute
import com.madlab.wms.grinfeld.roman.entities.checked.checked_te_barcode.Route
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.cheking_euz_fragment.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by grinfeldra
 */
const val KEY_NUM_ROUTE = "key_num_route"

class CheckingEUZFragment : MvpAppCompatFragment(), CheckingEUZView, AdapterView.OnItemSelectedListener, KeyEventListener, IOnScannerEvent {

    @InjectPresenter
    lateinit var presenter: CheckingEUZPresenter

    @ProvidePresenter
    fun providePresenter() = CheckingEUZPresenter()

    private lateinit var liveRoute: LiveData<Route>
    private lateinit var dao: CheckingDao
    private lateinit var scanner: AbstractScanner
    private var numRoute: String? = null
    private var adapter: MyAdapter<EUZRoute>? = null
    private lateinit var progressOwner: ProgressOwner
    private val a = 0.4f
    private val b = 0.6f
    private lateinit var alertDialog: AlertDialog


    fun newInstance(numRoute: String) = CheckingEUZFragment().apply {
        arguments = Bundle().apply {
            putString(KEY_NUM_ROUTE, numRoute)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner.onResume()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        numRoute = arguments?.getString(KEY_NUM_ROUTE)
        dao = App.getDatabase().checkingDao()
        liveRoute = dao.getLiveRoute(numRoute!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.list_euz))
        return inflater.inflate(R.layout.cheking_euz_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = MyAdapter(context)
        adapter?.setWeight(a, b)
        list_view.adapter = adapter
        val euzRoutes = dao.getRoute(numRoute!!).euzRoutes
        adapter?.setData(euzRoutes)
        liveRoute.observe(viewLifecycleOwner, Observer { it ->
            if (it.euzRoutes.size == 0) {
                val alertDialog = context?.let {
                    AlertDialog.Builder(it)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle(getString(R.string.confirmation))
                            .setMessage(getString(R.string.check_complete))
                            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                                fragmentManager?.popBackStack()
                            }
                            .create()
                }
                if (alertDialog?.isShowing == false) {
                    alertDialog.show()
                }
            }
        })
        Utils.setParamFromHeader(view, a, b, getString(R.string.euz_lbl),
                getString(R.string.date).plus(" | ").plus(getString(R.string.count_lbl)))
        list_view.onItemSelectedListener = this
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event?.action == KeyEvent.ACTION_UP) {
            val alertDialog = context?.let {
                AlertDialog.Builder(it)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.confirmation))
                        .setMessage(getString(R.string.close_check))
                        .setPositiveButton(getString(R.string.yes)) { _, _ ->
                            fragmentManager?.popBackStack()
                        }
                        .setNegativeButton(getString(R.string.no), null)
                        .create()
            }
            if (alertDialog?.isShowing == false) {
                alertDialog.show()
            }
            return true
        } else if (keyCode == KeyEvent.KEYCODE_O && event?.action == KeyEvent.ACTION_UP) {
            val alertDialog = context?.let {
                AlertDialog.Builder(it)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.confirmation))
                        .setMessage(getString(R.string.reload_euz))
                        .setPositiveButton(getString(R.string.yes)) { _, _ ->
                            numRoute?.let { it1 -> presenter.getEuzFromRoute(it1) }
                        }
                        .setNegativeButton(getString(R.string.no), null)
                        .create()
            }
            if (alertDialog?.isShowing == false) {
                alertDialog.show()
            }
            return true
        }
        return false
    }

    override fun onCountChange(countBox: Int) {
        txt_count.text = getString(R.string.count_label, countBox.toString().plus("Кор."))
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (::alertDialog.isInitialized && alertDialog.isShowing) {
            alertDialog.dismiss()
        }
        val productionDate = goodsCard?.production_date
        val te = goodsCard?.te
        if (productionDate != null) {
            val value = adapter?.allItem
            val euzRoute = value?.find { euzRoute -> goodsCard.barcode == euzRoute.barcode }
            if (euzRoute != null) {
                val format = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
                val productionDateStr = format.format(productionDate)
                if (productionDateStr == euzRoute.date) {
                    val currentIndex = value.indexOf(euzRoute)
                    list_view.requestFocusFromTouch()
                    list_view.setSelection(currentIndex)
                    val route = dao.getRoute(numRoute!!)
                    euzRoute.countBox--
                    if (euzRoute.countBox == 0) {
                        route.euzRoutes.removeAt(currentIndex)
                    } else {
                        route.euzRoutes[currentIndex] = euzRoute
                        presenter.countChange(euzRoute.countBox)
                    }
                    adapter?.setData(route.euzRoutes)
                    dao.update(route)
                } else {
                    Toast.makeText(context, getString(R.string.error_date_euz), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context, getString(R.string.euz_not_found_from_route), Toast.LENGTH_SHORT).show()
            }
        } else if (te != null) {
            val value = adapter?.allItem
            val euzRoutes = value?.filter { euzRoute -> te == euzRoute.te }
            if (euzRoutes != null && euzRoutes.isNotEmpty()) {
                showEUZFromTE(euzRoutes, te)
            } else {
                Toast.makeText(context, getString(R.string.error_te), Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(context, getString(R.string.error_meta_128barcode), Toast.LENGTH_SHORT).show()
        }
    }


    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val euzRoute = parent?.getItemAtPosition(position) as EUZRoute
        txt_euz.text = euzRoute.codeGoods
        txt_te.text = euzRoute.te
        txt_count.text = getString(R.string.count_label, euzRoute.countBox.toString().plus("Кор."))
    }

    override fun onPause() {
        super.onPause()
        scanner.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner.onStop()
    }

    override fun onDetach() {
        super.onDetach()
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
    }

    override fun onCompleteReload(euzRoutes: ArrayList<EUZRoute>) {
        numRoute?.let {
            adapter?.addData(euzRoutes)
            val route = dao.getRoute(it)
            route.euzRoutes.addAll(euzRoutes)
            dao.update(route)
        }
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }


    private fun showEUZFromTE(euzRoutes: List<EUZRoute>, mTENumber: String) {
        val adapter = MyAdapter(context, euzRoutes)
        val rootView = layoutInflater.inflate(R.layout.dialog_listview, null)
        rootView.findViewById<TextView>(R.id.tv_message).text =
                getString(R.string.euz_from_te, mTENumber)
        val listView = rootView.findViewById<ListView>(R.id.list_view)
        listView.adapter = adapter
        Utils.setParamFromHeader(view, a, b, getString(R.string.euz_lbl),
                getString(R.string.date).plus(" | ").plus(getString(R.string.count_lbl)))
        alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setTitle(getString(R.string.list_euz))
                    .setView(rootView)
                    .create()
        }!!
        listView.onItemClickListener = null
        if (!alertDialog.isShowing) {
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }


}