package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes.checking_boxes.CheckingBoxesFragment
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import kotlinx.android.synthetic.main.checking_action_fragment.*

/**
 * Created by grinfeldra
 */
class CheckingActionFragment : MvpAppCompatFragment(), AdapterView.OnItemClickListener, IOnScannerEvent, CheckingActionView {

    @InjectPresenter
    lateinit var presenter: CheckingActionPresenter

    @ProvidePresenter
    fun providePresenter() = CheckingActionPresenter()

    private var scanner: AbstractScanner? = null
    private var progressOwner: ProgressOwner? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.selection_check_title))
        return inflater.inflate(R.layout.checking_action_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = ArrayAdapter<String>(context, android.R.layout.select_dialog_item,
                resources.getStringArray(R.array.actions_checking_boxes))
        list_view.adapter = adapter
        list_view.onItemClickListener = this
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        presenter.getData()
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        val barcode = goodsCard?.barcode?.trim()
        if (barcode != null && barcode.contains("M") && barcode.contains("L")) {
            val routeBarcode = barcode?.substring(barcode.indexOf("M") + 1,
                    barcode.indexOf("L"))
            presenter.getDataByBarcode(routeBarcode)
        } else {
            showError(getString(R.string.error_route_barcode))
        }
    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

    override fun onCompleteGetData(numDoc: String) {
        (activity as MainActivity).changeFragment(CheckingBoxesFragment().newInstance(numDoc))
    }

}