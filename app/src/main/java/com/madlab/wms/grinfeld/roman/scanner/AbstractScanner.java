package com.madlab.wms.grinfeld.roman.scanner;

/**
 * Created by GrinfeldRa
 */
public abstract class AbstractScanner {

    public abstract void onResume();
    public abstract void onPause();
    public abstract void onStop();

}
