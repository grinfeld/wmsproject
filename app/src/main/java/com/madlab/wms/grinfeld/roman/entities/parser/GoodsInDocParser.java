package com.madlab.wms.grinfeld.roman.entities.parser;

import com.madlab.wms.grinfeld.roman.entities.GoodsInDoc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRa
 */
public class GoodsInDocParser {

    //1;ГКДАЛ78627;Грудной отруб бескостный из мраморной говядины, зам. (Brisket Deckle-Off, Boneless 120);16;;1;0;1;12/12/2019;11/12/2020;;16 кг;332900200;РЦ Возврат;
    public static List<GoodsInDoc> getGoodsInDoc(String response) {
        List<GoodsInDoc> result = new ArrayList<>();
        if (response.equals("OK;")){
            return result;
        }
        String[] array = response.split("[|]");
        for (String s : array){
            String[] temp = s.split(";");
            int position = Integer.parseInt(temp[0]);
            String code = temp[1];
            String name = temp[2];
            String count = temp[3];
            String te = temp[4];
            String prodDate = temp[8];
            String shelfLife = temp[9];
            String countStr = temp[11];
            result.add(new GoodsInDoc(position, code, name, prodDate, shelfLife, count, countStr, te));
        }
        return result;
    }

}
