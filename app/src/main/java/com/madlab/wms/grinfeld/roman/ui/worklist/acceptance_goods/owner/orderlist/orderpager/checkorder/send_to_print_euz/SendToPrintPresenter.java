package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.send_to_print_euz;


import com.arellomobile.mvp.InjectViewState;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.common.BasePresenter;
import com.madlab.wms.grinfeld.roman.client.WMSClient;

import io.reactivex.android.schedulers.AndroidSchedulers;


/**
 * Created by GrinfeldRA
 */
@InjectViewState
public class SendToPrintPresenter extends BasePresenter<SendToPrintView> {

    void sentToPrint(String shotName, int coefficient, int count) {
        mCompositeDisposable.add(new WMSClient()
                .send(Command.COM_SEND_TO_PRINT_EUZ(shotName, coefficient, count))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doFinally(getViewState()::hideProgress)
                .subscribe(response -> {
                    if (isSuccess(response)) {
                        getViewState().onCompleteSentToPrint();
                    }
                }, throwable -> getViewState().showError(throwable.getMessage())));
    }

}
