package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.wms.grinfeld.roman.adapter.AbstractData;

/**
 * Created by GrinfeldRa
 */
public class GoodsInDoc extends AbstractData implements Parcelable {


    // 2 = 2;ГКДАЛ67143;Огурцы  10200мл  рез. кружочками  Eggerstorfer /Германия;10;EU0197322;1;0;1;12/03/2019;12/06/2019;;10 Шт. ;332900400;РЦ Приемка;
    // 1 = 1;ГКДАЛ57166; Оливки ЗЕЛЕНЫЕ б/к  300мл/12 SUNFEEL/Испания; 77; EU0197322; 1;0;1;12/03/2019;12/06/2019;;6 Кор. 5 Шт.;332900400;РЦ Приемка;
    private int position;
    private String code;
    private String name;
    private String prodDate;
    private String shelfLife;
    private String count;
    private String countStr;
    private String te;


    public GoodsInDoc(int position, String code, String name, String prodDate, String shelfLife, String count, String countStr, String te) {
        this.position = position;
        this.code = code;
        this.name = name;
        this.prodDate = prodDate;
        this.shelfLife = shelfLife;
        this.count = count;
        this.countStr = countStr;
        this.te = te;
    }

    private GoodsInDoc(Parcel in) {
        position = in.readInt();
        code = in.readString();
        name = in.readString();
        prodDate = in.readString();
        shelfLife = in.readString();
        count = in.readString();
        countStr = in.readString();
        te = in.readString();
    }

    public static final Creator<GoodsInDoc> CREATOR = new Creator<GoodsInDoc>() {
        @Override
        public GoodsInDoc createFromParcel(Parcel in) {
            return new GoodsInDoc(in);
        }

        @Override
        public GoodsInDoc[] newArray(int size) {
            return new GoodsInDoc[size];
        }
    };

    public int getPosition() {
        return position;
    }


    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getShelfLife() {
        return shelfLife;
    }

    public String getCount() {
        return count;
    }

    public String getTe() {
        return te;
    }

    public String getCountStr() {
        return countStr;
    }

    public String getProdDate() {
        return prodDate;
    }

    @Override
    public String getLeftText() {
        return getCode();
    }

    @Override
    public String getRightText() {
        return getName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(position);
        dest.writeString(code);
        dest.writeString(name);
        dest.writeString(prodDate);
        dest.writeString(shelfLife);
        dest.writeString(count);
        dest.writeString(countStr);
        dest.writeString(te);
    }
}
