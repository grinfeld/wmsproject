package com.madlab.wms.grinfeld.roman.entities

import com.madlab.wms.grinfeld.roman.adapter.AbstractData

/**
 * Created by grinfeldra
 */
class Zone(var code: String, var name: String) : AbstractData() {
    override fun getLeftText(): String {
        return code
    }

    override fun getRightText(): String {
        return name
    }

    companion object {
        fun parseAnswer(answer: String): List<Zone> {
            val result = ArrayList<Zone>()
            for (temp in answer.split("|")) {
                val split = temp.split(";")
                result.add(Zone(split[0], split[1]))
            }
            return result
        }
    }
}