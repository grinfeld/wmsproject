package com.madlab.wms.grinfeld.roman.entities.selection;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.madlab.wms.grinfeld.roman.common.App;
import com.madlab.wms.grinfeld.roman.dao.OrderSelectionDao;
import com.madlab.wms.grinfeld.roman.entities.converter.DataConverter;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by grinfeldra
 * <p>
 * 1 = 9064400000013082015;1;ГКДАЛ;ГК "Далимо";0;0;0;0;1;1;;2;0;1;Магазины34 (резерв);1;70;70;70;0;
 * 2 = ГКДАЛ_26565459_3.5О&20150812#;
 */
@Entity
public class Order implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    public int id;
    public String mID;         //0 Номер автоперемещения
    public byte mType;         //1 Тип отбора груп
    public String mStoreName;  //2
    public String mOwner;      //3
    public byte mScanTE;       //4
    public byte mScanTEA;      //5
    public byte mAutoK;        //6
    public byte mScanCell;     //7
    public byte mEnterK;       //8
    public byte mScanZGZ;      //9
    public String addressCell;      //10
    public short mRowsCount;  //11
    public byte mA;          //12 Не используется
    public byte mOdnaTE;       //13
    public String mRoute;      //14
    public boolean mScanTEPosleOtbora;//15
    public byte mPercent;          //16
    public byte mPercentMaxWeight; //17
    public byte mPercentMaxWeightHeads;     //18
    /// <summary>
    /// Признак отбора по поддонам
    /// </summary>
    public boolean mSeparate;     //19
    public String mTaskNumber; //20
    public String mTaskNumberAllString;
    public int currentPosition = 0;
    public String scanTe;

    @TypeConverters(DataConverter.class)
    public List<Weight> mWeights = new ArrayList<>();

    @TypeConverters(DataConverter.class)
    public List<SelectionBarcode> mBarcode;

    @TypeConverters(DataConverter.class)
    public List<String> listWeightProductBarcode;

    @TypeConverters(DataConverter.class)
    public List<PalletPrinted> palletPrintedList;


    public Order() {
        mWeights = new ArrayList<>();
        mBarcode = new ArrayList<>();
        listWeightProductBarcode = new ArrayList<>();
        palletPrintedList = new ArrayList<>();
    }


    protected Order(Parcel in) {
        id = in.readInt();
        mID = in.readString();
        mType = in.readByte();
        mStoreName = in.readString();
        mOwner = in.readString();
        mScanTE = in.readByte();
        mScanTEA = in.readByte();
        mAutoK = in.readByte();
        mScanCell = in.readByte();
        mEnterK = in.readByte();
        mScanZGZ = in.readByte();
        addressCell = in.readString();
        mRowsCount = (short) in.readInt();
        mA = in.readByte();
        mOdnaTE = in.readByte();
        mRoute = in.readString();
        mScanTEPosleOtbora = in.readByte() != 0;
        mPercent = in.readByte();
        mPercentMaxWeight = in.readByte();
        mPercentMaxWeightHeads = in.readByte();
        mSeparate = in.readByte() != 0;
        mTaskNumber = in.readString();
        currentPosition = in.readInt();
        scanTe = in.readString();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    public static void parse(String data, String selectionBarcode, String weightProductBarcode) {
        String[] allRows = data.split("[|]");
        String[] selectionBarcodeRows = selectionBarcode.split("[|]");
        String[] weightProductBarcodeRows = weightProductBarcode.split(";");
        Order result = parseHeader(allRows[0]);
        if (result != null) {
            result.mTaskNumber = allRows[1].split("&")[0];
            result.mTaskNumberAllString = allRows[1];
            for (String row : selectionBarcodeRows) {
                String[] val = row.split(";");
                if (val.length >= 3) {
                    result.mBarcode.add(new SelectionBarcode(val[0], val[1], Utils.toDouble(val[2])));
                }
            }
            result.listWeightProductBarcode.addAll(Arrays.asList(weightProductBarcodeRows).subList(1, weightProductBarcodeRows.length));
            OrderSelectionDao orderSelectionDao = App.getDatabase().orderSelectionDao();
            orderSelectionDao.insert(result);
            String mTaskNUM = allRows[1].split("&")[0];
            int id = orderSelectionDao.getOrder(mTaskNUM).id;
            short itemsCounter = 0;
            byte mTENumber = -1;
            for (int i = 2; i < allRows.length; i++) {
                String[] row = allRows[i].split(";");
                OrderItem orderItem = OrderItem.parse(row);
                if (orderItem != null) {
                    orderItem.Index = itemsCounter;
                    orderItem.orderId = id;
                    if (mTENumber != orderItem.mTENumber) {
                        result.palletPrintedList.add(new PalletPrinted(orderItem.mTENumber, false));
                        mTENumber = orderItem.mTENumber;
                    }
                    orderSelectionDao.insert(orderItem);
                }
                itemsCounter++;
            }
            result.id = id;
            orderSelectionDao.update(result);
        }
    }

    public boolean isComplete(int id, byte mPercentMaxWeight) {
        boolean res = true;
        List<OrderItem> allOrders = App.getDatabase().orderSelectionDao().getAllOrders(id);
        for (OrderItem item : allOrders) {
            double nado = item.mNado;
            double est = item.mEst;
            if (item.mIsWeightGoods) {
                est += est * ((double) mPercentMaxWeight / 100);
            }
            Double v = nado - est;
            if (est == 0.0 || v.compareTo(0.0) > 0) {
                res = false;
                break;
            }
        }
        return res;
    }

    public boolean isCompletePlacing(int id) {
        boolean res = true;
        List<OrderItem> allOrders = App.getDatabase().orderSelectionDao().getAllOrders(id);
        for (OrderItem item : allOrders) {
            if (item.mMissing) continue;
            Double mEst = item.mEst;
            if (item.mPlacedPieces == 0 || mEst.compareTo(item.mPlacedPieces) > 0) {
                res = false;
                break;
            }
        }
        return res;
    }


    public boolean isCompletePlacingItem(OrderItem item) {
        Double mEst = item.mEst;
        if (item.mMissing) {
            return true;
        } else return item.mPlacedPieces != 0 && mEst.compareTo(item.mPlacedPieces) <= 0;
    }


    private static Order parseHeader(String rowHeader) {
        String[] values = rowHeader.split(";");
        Order result = new Order();
        for (int i = 0; i < values.length; i++) {
            String currentItem = values[i];
            switch (i) {
                case 0:
                    result.mID = currentItem;
                    break;
                case 1:
                    result.mType = Byte.parseByte(currentItem);
                    break;
                case 2:
                    result.mStoreName = currentItem;
                    break;
                case 3:
                    result.mOwner = currentItem;
                    break;
                case 4:
                    result.mScanTE = Byte.parseByte(currentItem);
                    break;
                case 5:
                    result.mScanTEA = Byte.parseByte(currentItem);
                    break;
                case 6:
                    result.mAutoK = Byte.parseByte(currentItem);
                    break;
                case 7:
                    result.mScanCell = Byte.parseByte(currentItem);
                    break;
                case 8:
                    result.mEnterK = Byte.parseByte(currentItem);
                    break;
                case 9:
                    result.mScanZGZ = Byte.parseByte(currentItem);
                    break;
                case 10:
                    result.addressCell = currentItem;
                    break;
                case 11:
                    result.mRowsCount = (short) Utils.toInt(currentItem);
                    break;
                case 12:
                    result.mA = Byte.parseByte(currentItem);
                    break;
                case 13:
                    result.mOdnaTE = Byte.parseByte(currentItem);
                    break;
                case 14:
                    result.mRoute = currentItem;
                    break;
                case 15:
                    result.mScanTEPosleOtbora = currentItem.equals("1");
                    break;
                case 16:
                    result.mPercent = Byte.parseByte(currentItem);
                    break;
                case 17:
                    result.mPercentMaxWeight = Byte.parseByte(currentItem);
                    break;
                case 18:
                    result.mPercentMaxWeightHeads = Byte.parseByte(currentItem);
                    break;
                case 19://Признак набора по поддонам
                    break;
                case 20://mTaskNumber Номер задания
                    break;
                default:
                    break;
            }
        }
        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(mID);
        dest.writeByte(mType);
        dest.writeString(mStoreName);
        dest.writeString(mOwner);
        dest.writeByte(mScanTE);
        dest.writeByte(mScanTEA);
        dest.writeByte(mAutoK);
        dest.writeByte(mScanCell);
        dest.writeByte(mEnterK);
        dest.writeByte(mScanZGZ);
        dest.writeString(addressCell);
        dest.writeInt((int) mRowsCount);
        dest.writeByte(mA);
        dest.writeByte(mOdnaTE);
        dest.writeString(mRoute);
        dest.writeByte((byte) (mScanTEPosleOtbora ? 1 : 0));
        dest.writeByte(mPercent);
        dest.writeByte(mPercentMaxWeight);
        dest.writeByte(mPercentMaxWeightHeads);
        dest.writeByte((byte) (mSeparate ? 1 : 0));
        dest.writeString(mTaskNumber);
        dest.writeInt(currentPosition);
        dest.writeString(scanTe);
    }
}
