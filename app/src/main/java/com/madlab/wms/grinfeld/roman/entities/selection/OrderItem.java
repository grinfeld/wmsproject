package com.madlab.wms.grinfeld.roman.entities.selection;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.madlab.wms.grinfeld.roman.adapter.AbstractData;
import com.madlab.wms.grinfeld.roman.utils.Utils;



/**
 * Created by grinfeldra
 * <p>
 * 3 = ;ГКДАЛ39004;Чечил копченый 100г/18шт (Имэкс);331602410;3-16-024-1;2;0;Шт.;Уп.;18;;0;;0;;0;0;0;0;0;Голина ИП (Ракита);0,1;;20150720;20151018;0;
 * 4 = ;ГКДАЛ39003;Чечил 130г/18шт (Имэкс);331602920;3-16-029-2;2;0;Шт.;Уп.;18;;0;;0;;0;0;0;0;0;Голина ИП (Ракита);0,18;;20150719;20150917;0;
 */
@Entity
public class OrderItem extends AbstractData implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public int Index;
    public int orderId;
    public String mTask;           // 0
    public String code;
    public String name;
    public String mCell;           // 3
    public String mCellAddress;    // 4
    public double mNado;           // 5
    public boolean mPodpitka;         // 6
    public String mBaseMeash;      // 7
    public String mPackMeash;      // 8
    /// <summary>
    /// 9 Количество в упаковке
    /// </summary>
    public double mKoef;
    public String mGU2;            // 10
    public short mKoef2;          // 11
    public String mGU3;            // 12
    public short mKoef3;          // 13
    public String mGU4;            // 14
    public short mKoef4;          // 15
    public boolean mIsWeightGoods;    // 16
    /// <summary>
    /// 17 Средний вес головки
    /// </summary>
    public double mHeadsWeight;
    public byte mA;                // 18 - Неизвестно
    /// <summary>
    /// 19 - надо голов
    /// </summary>
    public int mHeadsCount;
    public String mClient;         // 20
    /// <summary>
    /// 21 вес коробки
    /// </summary>
    public float mBoxWeight;
    /// <summary>
    /// 22 Головок в коробе
    /// </summary>
    public short mHeadsInBox;

    public String mBestBeforeStart;  // 23

    public String mBestBeforeEnd;    // 24

    /// <summary>
    /// 25 - Набрано штук
    /// </summary>
    public double mEst;
    /// <summary>
    /// 25 - Набрано голов
    /// </summary>
    public int mHeads;
    /// <summary>
    /// 26 - ШК поддона в формате EU*******
    /// </summary>
    public String mTE;
    /// <summary>
    /// Номер текущего поддона (для набора по коробам)
    /// </summary>
    public byte mTENumber;
    /// <summary>
    /// 27 - Размещено штук
    /// </summary>
    public double mPlacedPieces;
    /// <summary>
    /// 28 - Размещено голов
    /// </summary>
    public int mPlacedHeads;
    public String mZGZCell;        // 29 - Ячейка ЗГЗ
    public boolean mMissing;        // 30 - Нет на остатках

    /// <summary>
    /// Номер короба
    /// </summary>
    public int mBoxNumber;
    /// <summary>
    /// Название тары
    /// </summary>
    public String mBoxName;
    /// <summary>
    /// Флаг сборного/отдельного короба. 0 - отдельный, 1 - сборный
    /// </summary>
    public boolean mIsBoxSeparate;


    public OrderItem() {
    }


    protected OrderItem(Parcel in) {
        Index = in.readInt();
        mTask = in.readString();
        code = in.readString();
        name = in.readString();
        mCell = in.readString();
        mCellAddress = in.readString();
        mNado = in.readDouble();
        mPodpitka = in.readByte() != 0;
        mBaseMeash = in.readString();
        mPackMeash = in.readString();
        mKoef = in.readDouble();
        mGU2 = in.readString();
        mKoef2 = (short) in.readInt();
        mGU3 = in.readString();
        mKoef3 = (short) in.readInt();
        mGU4 = in.readString();
        mKoef4 = (short) in.readInt();
        mIsWeightGoods = in.readByte() != 0;
        mHeadsWeight = in.readDouble();
        mA = in.readByte();
        mHeadsCount = in.readInt();
        mClient = in.readString();
        mBoxWeight = in.readFloat();
        mHeadsInBox = (short) in.readInt();
        mBestBeforeStart = in.readString();
        mBestBeforeEnd = in.readString();
        mEst = in.readDouble();
        mHeads = in.readInt();
        mTE = in.readString();
        mTENumber = in.readByte();
        mPlacedPieces = in.readDouble();
        mPlacedHeads = in.readInt();
        mZGZCell = in.readString();
        mMissing = in.readByte() != 0;
        mBoxNumber = in.readInt();
        mBoxName = in.readString();
        mIsBoxSeparate = in.readByte() != 0;
    }

    public static final Creator<OrderItem> CREATOR = new Creator<OrderItem>() {
        @Override
        public OrderItem createFromParcel(Parcel in) {
            return new OrderItem(in);
        }

        @Override
        public OrderItem[] newArray(int size) {
            return new OrderItem[size];
        }
    };

    static OrderItem parse(String[] data) {
        try {
            OrderItem result = new OrderItem();
            for (int i = 0; i < data.length; i++) {
                String currentItem = data[i];
                if (currentItem == null) continue;
                switch (i) {
                    case 0:
                        result.mTask = currentItem;
                        break;
                    case 1:
                        result.code = currentItem;
                        break;
                    case 2:
                        result.name = currentItem;
                        break;
                    case 3:
                        result.mCell = currentItem;
                        break;
                    case 4:
                        result.mCellAddress = currentItem;
                        break;
                    case 5:
                        result.mNado = Utils.toDouble(currentItem);
                        break;
                    case 6:
                        result.mPodpitka = Utils.toInt(currentItem) == 1;
                        break;
                    case 7:
                        result.mBaseMeash = currentItem;
                        break;
                    case 8:
                        result.mPackMeash = currentItem;
                        break;
                    case 9:
                        result.mKoef = Utils.toDouble(currentItem);
                        break;
                    case 10:
                        result.mGU2 = currentItem;
                        break;
                    case 11:
                        result.mKoef2 = (short) Utils.toInt(currentItem);
                        break;
                    case 12:
                        result.mGU3 = currentItem;
                        break;
                    case 13:
                        result.mKoef3 = (short) Utils.toInt(currentItem);
                        break;
                    case 14:
                        result.mGU4 = currentItem;
                        break;
                    case 15:
                        result.mKoef4 = (short) Utils.toInt(currentItem);
                        break;
                    case 16:
                        result.mIsWeightGoods = Utils.toInt(currentItem) == 1;
                        break;
                    case 17:
                        result.mHeadsWeight = Utils.toDouble(currentItem);
                        break;
                    case 18:
                        result.mA = Byte.parseByte(currentItem);
                        break;
                    case 19:
                        result.mHeadsCount = Utils.toInt(currentItem);
                        break;
                    case 20:
                        result.mClient = currentItem;
                        break;
                    case 21:
                        result.mBoxWeight = Utils.toFloat(currentItem);
                        break;
                    case 22:
                        result.mHeadsInBox = (short) Utils.toInt(currentItem);
                        break;
                    case 23:
                        result.mBestBeforeStart = currentItem;
                        break;
                    case 24:
                        result.mBestBeforeEnd = currentItem;
                        break;
                    case 25: //Признак отбора по паллетам, пропускаем

                        break;
                    case 26://Номер короба
                        result.mBoxNumber = Utils.toInt(currentItem);
                        break;
                    case 27://Название тары
                        result.mBoxName = currentItem;
                        break;
                    case 28://Флаг сборный/отдельный короб
                        result.mIsBoxSeparate = currentItem.equals("0");
                        break;
                    case 29:// Номер поддона
                        result.mTENumber = Byte.parseByte(currentItem);
                        break;
                    default:
                        break;
                }


            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getExist() {
        if (mIsWeightGoods) {
            //Если весовой, в килограммах
            if (mHeadsCount > 0) {
                return String.format("%s%s (%sг.)", mEst, mBaseMeash, mHeads);
            } else {
                //Если весовой, в штуках
                return String.format("%s%s", mEst, mBaseMeash);
            }
        } else {
            //Если есть коробки
            if (mKoef > 0) {
                int box = (int) Math.floor(mEst / mKoef);
                int pieces = (int) (mEst - (box * mKoef));
                return String.format("%s%s %s%s", box, mPackMeash, pieces, mBaseMeash);
            } else {
                return String.format("%s%s", mEst, mBaseMeash);
            }
        }
    }

    public int BoxNeed() {
        return mHeadsInBox == 0 ? 0 : mHeadsCount / mHeadsInBox;
    }

    public String getNeed() {

        if (mIsWeightGoods) {
            //Если весовой, в килограммах
            if (mHeadsCount > 0) {
                int boxNeed = BoxNeed();
                if (boxNeed > 0) {
                    int rest = mHeadsCount - (mHeadsInBox * boxNeed);
                    if (rest > 0)
                        return String.format("%s%s (%sг=%sкор%sг)", mNado, mBaseMeash, mHeadsCount, boxNeed, rest);
                    else
                        return String.format("%s%s (%sг=%sкор)", mNado, mBaseMeash, mHeadsCount, boxNeed);
                } else {
                    return String.format("%s%s (%sг)", mNado, mBaseMeash, mHeadsCount);
                }
            } else //Если весовой, в штуках
            {
                return String.format("%s%s", mNado, mBaseMeash);
            }
        } else //Если штучный{
            //Если есть коробки
            if (mKoef > 0) {
                int box = (int) Math.floor(mNado / mKoef);
                int pieces = (int) (mNado - (box * mKoef));
                return String.format("%s%s %s%s", box, mPackMeash, pieces, mBaseMeash);
            } else {
                return String.format("%s%s", mNado, mBaseMeash);
            }
    }


    @Override
    public String getLeftText() {
        return name;
    }

    @Override
    public String getRightText() {
        return (int) mNado + " | " + (int) mEst;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Index);
        dest.writeString(mTask);
        dest.writeString(code);
        dest.writeString(name);
        dest.writeString(mCell);
        dest.writeString(mCellAddress);
        dest.writeDouble(mNado);
        dest.writeByte((byte) (mPodpitka ? 1 : 0));
        dest.writeString(mBaseMeash);
        dest.writeString(mPackMeash);
        dest.writeDouble(mKoef);
        dest.writeString(mGU2);
        dest.writeInt((int) mKoef2);
        dest.writeString(mGU3);
        dest.writeInt((int) mKoef3);
        dest.writeString(mGU4);
        dest.writeInt((int) mKoef4);
        dest.writeByte((byte) (mIsWeightGoods ? 1 : 0));
        dest.writeDouble(mHeadsWeight);
        dest.writeByte(mA);
        dest.writeInt(mHeadsCount);
        dest.writeString(mClient);
        dest.writeFloat(mBoxWeight);
        dest.writeInt((int) mHeadsInBox);
        dest.writeString(mBestBeforeStart);
        dest.writeString(mBestBeforeEnd);
        dest.writeDouble(mEst);
        dest.writeInt(mHeads);
        dest.writeString(mTE);
        dest.writeByte(mTENumber);
        dest.writeDouble(mPlacedPieces);
        dest.writeInt(mPlacedHeads);
        dest.writeString(mZGZCell);
        dest.writeByte((byte) (mMissing ? 1 : 0));
        dest.writeInt(mBoxNumber);
        dest.writeString(mBoxName);
        dest.writeByte((byte) (mIsBoxSeparate ? 1 : 0));
    }
}


