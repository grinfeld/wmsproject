package com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.shipment_enter

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentGoods
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
@InjectViewState
class ShipmentEnterPresenter : BasePresenter<ShipmentEnterView>() {

    fun getGoodsFromShipment(te: String, docNum: String) {
        add(WMSClient().send(Command.COM_GET_GOODS_FROM_SHIPMENT(te, docNum))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val data = ShipmentGoods.parse(it)
                        viewState.onCompleteGetGoodsFromShipment(data, te)
                    }
                })
    }

    fun printShipment(list: List<String>) {
        add(WMSClient().sendArray(Command.COM_PRINT_SHIPMENT(), list)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.showError(it)
                    }
                })
    }

    fun completeShipment(param: ArrayList<String>) {
        add(WMSClient().sendArray(Command.COM_COMPLETE_SHIPMENT(), param)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.showError("Успешно отгружено")
                        viewState.onComplete()
                    }
                })
    }

    fun closeShipment(value: String) {
        add(WMSClient().send(Command.COM_CLOSE_SHIPMENT(value))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.onComplete()
                    }
                })
    }
}