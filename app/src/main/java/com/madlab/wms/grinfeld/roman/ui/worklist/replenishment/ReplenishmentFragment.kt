package com.madlab.wms.grinfeld.roman.ui.worklist.replenishment

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.Replenishment
import kotlinx.android.synthetic.main.replenishment_fragment.*

/**
 * Created by grinfeldra
 */
const val KEY_LIST = "key_list"
const val KEY_DOC_TYPE = "key_doc_type"

class ReplenishmentFragment : MvpAppCompatFragment(), ReplenishmentView, KeyEventListener {

    @InjectPresenter
    lateinit var presenter: ReplenishmentPresenter

    @ProvidePresenter
    fun providePresenter() = ReplenishmentPresenter()

    private lateinit var progressOwner: ProgressOwner
    private lateinit var list: ArrayList<Replenishment>

    fun newInstance(list: ArrayList<Replenishment>, docType: String) = ReplenishmentFragment().apply {
        arguments = Bundle().apply {
            putParcelableArrayList(KEY_LIST, list)
            putString(KEY_DOC_TYPE, docType)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        list = arguments?.getParcelableArrayList<Replenishment>(KEY_LIST) as ArrayList<Replenishment>
        val num = list[0].num
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.replenishment_title, num))
        return inflater.inflate(R.layout.replenishment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.liveData.observe(viewLifecycleOwner, Observer {
            val replenishment = list[it]
            et_from_cell.setText(replenishment.fromCellTxt)
            et_to_cell.setText(replenishment.toCellTxt)
            et_name.setText(replenishment.nameGoods)
            val count = replenishment.count.plus(" ").plus(replenishment.baseEi)
            if (replenishment.otherEiTxt.isNotEmpty()) {
                count.plus("(").plus(replenishment.otherEiTxt).plus(")")
            }
            txt_count.text = getString(R.string.count, count)
        })
    }


    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            if (event?.action == KeyEvent.ACTION_UP) {
                val currentPos = presenter.liveData.value
                if (currentPos != null && currentPos < list.size - 1) {
                    presenter.liveData.value = currentPos + 1
                }
            }
            true
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            if (event?.action == KeyEvent.ACTION_UP) {
                val currentPos = presenter.liveData.value
                if (currentPos != null && currentPos > 0) {
                    presenter.liveData.value = currentPos - 1
                }
            }
            true
        } else if (keyCode == KeyEvent.KEYCODE_Y) {
            if (event?.action == KeyEvent.ACTION_UP) {
                val currentPos = presenter.liveData.value!!
                val replenishment = list[currentPos]
                val docType = arguments?.getString(KEY_DOC_TYPE)
                if (docType != null) {
                    val alertDialog = context?.let {
                        AlertDialog.Builder(it)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle(getString(R.string.confirmation))
                                .setMessage(getString(R.string.complete_dialog_replenish))
                                .setPositiveButton(getString(R.string.yes)) { _, _ ->
                                    presenter.setReserve(replenishment, docType)
                                }
                                .setNegativeButton(getString(R.string.no), null)
                                .create()
                    }
                    if (!alertDialog?.isShowing!!) {
                        alertDialog.show()
                    }
                }
            }
            true
        } else {
            false
        }
    }

    override fun onCompleteItemReserve(fromCellTxt: String, toCellTxt: String, list: java.util.ArrayList<Replenishment>) {
        this.list = list
        presenter.liveData.value = 0
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage(getString(R.string.replenishment_complete_item, fromCellTxt, toCellTxt))
                    .setNegativeButton(getString(R.string.ok), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onCompleteReserve() {
        showError(getString(R.string.replenishment_complete))
        fragmentManager?.popBackStack()
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }
}