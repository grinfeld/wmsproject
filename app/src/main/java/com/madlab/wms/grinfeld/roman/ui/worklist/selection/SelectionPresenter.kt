package com.madlab.wms.grinfeld.roman.ui.worklist.selection

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.entities.selection.MyPrinter
import com.madlab.wms.grinfeld.roman.entities.selection.Order
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.BOXES_SELECTION
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.CLASSIC_SELECTION
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.TE_SELECTION
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit


/**
 * Created by grinfeldra
 */
@InjectViewState
class SelectionPresenter : BasePresenter<SelectionView>() {

    fun newSelection() {
        mCompositeDisposable.add(WMSClient().send(Command.COM_NEW_SELECTION())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .repeatWhen { it.delay(5, TimeUnit.SECONDS, AndroidSchedulers.mainThread()) }
                .takeUntil { !it.contains("OK;Q;") }
                .subscribe({
                    if (it.contains("ERROR")) {
                        viewState.hideProgress()
                        viewState.showError(it.split(";")[1])
                    } else if (!it.contains("OK;Q;")) {
                        if (it.contains("OK;N;")) {
                            viewState.hideProgress()
                            viewState.onNotAvailable()
                        } else {
                            val data = it.split(";")
                            getBarcode(it, data[0], data[1])
                        }
                    }
                }, {
                    it.printStackTrace()
                    viewState.showError(it.message)
                    viewState.hideProgress()
                })

        )
    }

    fun getBarcode(orderData: String, orderID: String, orderType: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_BARCODE_FROM_SELECTION(orderID, orderType))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.isEmpty()) {
                        viewState.onErrorGetBarcode(orderData, orderID, orderType)
                    } else {
                        getWeightProductBarcode(orderData, it)
                    }
                })
    }

    fun getWeightProductBarcode(orderData: String, selectionBarcode: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_WEIGHT_GOODS_BARCODE())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    try {
                        val dao = App.getDatabase().orderSelectionDao()
                        val typeOrder = orderData.split("|")[2].split(";")[25].toByte()
                        val mTaskNumber = orderData.split("|")[1].split("&")[0]
                        val order = dao.getOrder(mTaskNumber)
                        if (order == null) {
                            Order.parse(orderData, selectionBarcode, it)
                        } else {
                            order.mEnterK = orderData.split("|")[0].split(";")[8].toByte()
                            dao.update(order)
                        }
                        when (typeOrder) {
                            CLASSIC_SELECTION -> viewState.onSelectionStart(mTaskNumber, typeOrder)
                            TE_SELECTION -> viewState.onSelectionStart(mTaskNumber, typeOrder)
                            BOXES_SELECTION -> getListPrinters(mTaskNumber, typeOrder)
                        }
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        viewState.hideProgress()
                        viewState.showError("Недостаточно параметров")
                    }
                })
    }

    fun getListPrinters(mTaskNumber: String, typeOrder: Byte) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_LIST_PRINTERS())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .subscribe {
                    MyPrinter.parseResponse(it)
                    viewState.hideProgress()
                    viewState.onCompleteGetListPrinters(mTaskNumber, typeOrder)
                })
    }

    fun sendToPrint(host: String, mID: String, mTE: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_SEND_TO_PRINT_SELECTION(host, mID, mTE))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.hideProgress()
                        viewState.onCompleteSendToPrint()
                    }
                })
    }


}