package com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.parcel.Parcelize

/**
 * Created by grinfeldra
 *
 * ;;ГКДАЛ50314;Шеба Плежер говядина/кролик 24х85г;3;0;Шт.;0;0;0;0;Кор.;24;;;;;50;
 */
@Parcelize
@Entity
class EuzBoxesRoute(@PrimaryKey(autoGenerate = true) var id: Long?,
                    var codeGoods: String,
                    var nameGoods: String,
                    var count: Double,
                    var baseEi: String,
                    var numBoxes: Int,
                    var enterCount: Double,
                    var checked: Boolean) : Parcelable, AbstractData() {

    override fun getLeftText(): String {
        return codeGoods
    }

    override fun getRightText(): String {
        return numBoxes.toString()
    }

    constructor() : this(null, "", "", 0.0, "", 0, 0.0, false)

    companion object {
        fun parseAnswer(listData: List<String>): ArrayList<EuzBoxesRoute> {
            val result = ArrayList<EuzBoxesRoute>()
            for ((id, temp) in listData.withIndex()) {
                val split = temp.split(";")
                val euzBoxesRoute = EuzBoxesRoute()
                euzBoxesRoute.id = id.toLong()
                euzBoxesRoute.codeGoods = split[2]
                euzBoxesRoute.nameGoods = split[3]
                euzBoxesRoute.count = Utils.toDouble(split[4])
                euzBoxesRoute.baseEi = split[6]
                euzBoxesRoute.numBoxes = Utils.toInt(split[17])
                result.add(euzBoxesRoute)
            }
            return result
        }
    }


}