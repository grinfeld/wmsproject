package com.madlab.wms.grinfeld.roman.scanner;

import com.madlab.wms.grinfeld.roman.entities.ScannerData;

import java.util.Date;
import se.injoin.gs1utils.ApplicationIdentifier;
import se.injoin.gs1utils.ElementStrings;

/**
 * Created by GrinfeldRa
 */
class BarcodeParseMetaData {

    private static final String TAG = "#ScannerParseMetaData";

    static ScannerData parseMetadata(String barcode){
        ElementStrings.ParseResult result = ElementStrings.parse(barcode);
        String weight = String.valueOf(result.getDecimal(ApplicationIdentifier.ITEM_NET_WEIGHT_KG));
        Date production_date = result.getDate(ApplicationIdentifier.PRODUCTION_DATE);
        Date expiration_date = result.getDate(ApplicationIdentifier.EXPIRATION_DATE);
        String lootNumber = result.getString(ApplicationIdentifier.BATCH_OR_LOT_NUMBER);
        String barCode = result.getString(ApplicationIdentifier.GTIN);
        try {
            Integer.parseInt(weight);
        }catch (NumberFormatException e){
            weight = null;
        }
        String barcodePrefix = barcode.substring(0, 2);
        if (barcodePrefix.equals("EU") || barcodePrefix.equals("FI") || barcodePrefix.equals("US")) {
            return new ScannerData(weight, production_date, expiration_date, lootNumber, null, barcode);
        }else {
            return new ScannerData(weight, production_date, expiration_date, lootNumber, barCode == null ? barcode : barCode, null);
        }
    }

}
