package com.madlab.wms.grinfeld.roman.entities.selection;

/**
 * Created by grinfeldra
 */
public class Box {

    private String description;
    private int count;
    private int id;

    public Box(int id, String description, int count) {

        this.description = description;
        this.count = count;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }
}
