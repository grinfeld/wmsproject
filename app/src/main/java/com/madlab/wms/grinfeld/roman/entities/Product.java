package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.wms.grinfeld.roman.adapter.AbstractData;
import com.madlab.wms.grinfeld.roman.utils.DataParser;

public class Product extends AbstractData implements Parcelable {

    private String code;
    private String name;
    private String box;
    private String thing;
    private String box2;
    private String thing2;
    private String box3;
    private String thing3;
    private String unit;

    private int accepted;

    public Product(String code, String name, String countBox, String countThing, String countBox2, String countThing2, String countBox3, String countThing3, String unit) {
        this.code = code;
        this.name = name;
        this.box = countBox;
        this.thing = countThing;
        this.box2 = countBox2;
        this.thing2 = countThing2;
        this.box3 = countBox3;
        this.thing3 = countThing3;
        this.unit = unit;
        accepted = 0;
    }

    private Product(Parcel in) {
        code = in.readString();
        name = in.readString();
        box = in.readString();
        thing = in.readString();
        box2 = in.readString();
        thing2 = in.readString();
        box3 = in.readString();
        thing3 = in.readString();
        unit = in.readString();
        accepted = in.readInt();
    }

    public int getAccepted() {
        return accepted;
    }

    public void setAccepted(int accepted) {
        this.accepted = accepted;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getBox() {
        return box;
    }

    public int getCountBox() {
        return DataParser.getCount(box);
    }

    public String getThing() {
        return thing;
    }

    public int getCountThing() {
        return DataParser.getCount(thing);
    }

    public String getBox2() {
        return box2;
    }

    public int getCountBox2() {
        return DataParser.getCount(box2);
    }

    public String getThing2() {
        return thing2;
    }

    public int getCountThing2() {
        return DataParser.getCount(thing2);
    }

    public String getBox3() {
        return box3;
    }

    public int getCountBox3() {
        return DataParser.getCount(box3);
    }

    public String getThing3() {
        return thing3;
    }

    public int getCountThing3() {
        return DataParser.getCount(thing3);
    }

    public String getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return "Product{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", box='" + box + '\'' +
                ", thing='" + thing + '\'' +
                ", box2='" + box2 + '\'' +
                ", thing2='" + thing2 + '\'' +
                ", thing3='" + thing3 + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
        dest.writeString(box);
        dest.writeString(thing);
        dest.writeString(box2);
        dest.writeString(thing2);
        dest.writeString(box3);
        dest.writeString(thing3);
        dest.writeString(unit);
        dest.writeInt(accepted);
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public String getLeftText() {
        return getCode();
    }

    @Override
    public String getRightText() {
        return getName();
    }
}
