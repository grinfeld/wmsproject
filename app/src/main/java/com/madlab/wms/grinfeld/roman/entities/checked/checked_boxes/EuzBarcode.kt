package com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by grinfeldra
 *
 * ГКДАЛ62615;4623720637934;1;
 *
 */
@Entity
data class EuzBarcode(@PrimaryKey(autoGenerate = true)
                      var id: Long?, var codeGoods: String,
                      var barcode: String) {

    constructor() : this(null, "", "")

    companion object {
        fun parseAnswer(answer: String): ArrayList<EuzBarcode> {
            val result = ArrayList<EuzBarcode>()
            val split = answer.split("|")
            for (temp in split) {
                val data = temp.split(";")
                val euzBarcode = EuzBarcode()
                euzBarcode.codeGoods = data[0]
                euzBarcode.barcode = data[1]
                result.add(euzBarcode)
            }
            return result
        }
    }

}