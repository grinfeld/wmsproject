package com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentOwner
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
interface ShipmentOwnerMorphView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onFirstViewAttach()

    @StateStrategyType(AddToEndStrategy::class)
    fun completeLoadShipment(result: List<ShipmentOwner>)

    @StateStrategyType(SkipStrategy::class)
    fun completeLoadItemInfo(data: ArrayList<Shipment>, docNum: String, nameOwner: String)
}