package com.madlab.wms.grinfeld.roman.ui.worklist.checking.checking_boxes

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.App
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.dao.AppDatabase
import com.madlab.wms.grinfeld.roman.entities.checked.checked_boxes.RouteBoxes
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class CheckingActionPresenter : BasePresenter<CheckingActionView>() {

    fun getData() {
        add(WMSClient().send(Command.COM_GET_DATA_FROM_CHECKING_BOXES())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .subscribe {
                    if (it.contains("ERROR")) {
                        viewState.hideProgress()
                        viewState.showError(it.split(";")[1])
                    } else {
                        val split = it.split(";")
                        getDataStep2(it, split[0], split[1])
                    }
                })
    }

    fun getDataByBarcode(routeBarcode: String) {
        add(WMSClient().send(Command.COM_GET_DATA_FROM_CHECKING_BOXES_BY_BARCODE(routeBarcode))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .subscribe {
                    if (it.contains("ERROR")) {
                        viewState.hideProgress()
                        viewState.showError(it.split(";")[1])
                    } else {
                        val split = it.split(";")
                        getDataStep2(it, split[0], split[1])
                    }
                })
    }

    private fun getDataStep2(answer: String, docNum: String, date: String) {
        add(WMSClient().send(Command.COM_GET_DATA_FROM_CHECKING_BOXES_STEP2(docNum, date))
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        val checkingBoxesDao = App.getDatabase().checkingBoxesDao()
                        val route: RouteBoxes? = checkingBoxesDao.getRoute(docNum)
                        if (route == null) {
                            val routeBoxes = RouteBoxes.parseAnswer(answer, it)
                            checkingBoxesDao.insert(routeBoxes)
                        }
                        viewState.onCompleteGetData(docNum)
                    }
                })
    }
}