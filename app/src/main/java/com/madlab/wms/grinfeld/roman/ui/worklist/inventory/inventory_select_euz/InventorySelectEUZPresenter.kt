package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_select_euz

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class InventorySelectEUZPresenter : BasePresenter<InventorySelectEUZView>() {


    fun loadInventoryNum(code: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_INVENTORY_NUM(code))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally { viewState.hideProgress() }
                .subscribe({ response ->
                    if (isSuccess(response)) {
                        if (response.contains("NO")) {
                            viewState.showError(response.split(";")[1])
                        } else {
                            val split = response.split(";")
                            val numInventory = split[1]
                            val createDate = split[2]
                            loadProductInInventoryEUZ(numInventory, createDate)
                        }
                    }
                }, { throwable -> viewState.showError(throwable.message) }))
    }

    fun loadProductFromBarcode(barcode: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_PRODUCT_FROM_BARCODE(barcode))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally { viewState.hideProgress() }
                .subscribe({ response ->
                    if (isSuccess(response)) {
                        val codeGoods = response.split(";")[0]
                        loadInventoryNum(codeGoods)
                    }
                }, { throwable -> viewState.showError(throwable.message) }))
    }

    private fun loadProductInInventoryEUZ(inventoryNum: String, createDate: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_GET_PRODUCT_IN_INVENTORY(inventoryNum, createDate, Command.TYPE_EUZ))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally { viewState.hideProgress() }
                .subscribe({ response ->
                    if (isSuccess(response)) {
                        viewState.onCompleteLoadProductInInventoryEUZ(InventoryCellProduct.getList(response, Command.TYPE_EUZ), createDate, inventoryNum)
                    }
                }, { throwable -> viewState.showError(throwable.message) }))
    }


}