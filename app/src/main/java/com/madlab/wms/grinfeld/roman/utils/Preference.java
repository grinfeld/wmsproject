package com.madlab.wms.grinfeld.roman.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.common.App;

/**
 * Created by GrinfeldRa
 */
public class Preference {

    private static Context context = App.getAppContext();

    public static int getPort() {
        if (getString(context, context.getString(R.string.pref_port_key)).equals(""))
            return 0;
        else
            return Integer.parseInt(getString(context, context.getString(R.string.pref_port_key)));
    }

    public static String getServer() {
        return getString(context, context.getString(R.string.pref_server_key));
    }

    public static String getLoginCode() {
        return getString(context, context.getString(R.string.pref_login_code_key));
    }

    private static String getString(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, "");
    }

    public static void setLoginCode(String text) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(context.getString(R.string.pref_login_code_key), text);
        edit.apply();
    }

    public static int getOrderConditionMode() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(context.getString(R.string.pref_condition_mode), 0);
    }

    public static void setOrderConditionMode(int mode) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(context.getString(R.string.pref_condition_mode), mode);
        edit.apply();
    }
}
