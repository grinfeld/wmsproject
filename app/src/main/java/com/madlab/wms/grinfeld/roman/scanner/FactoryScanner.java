package com.madlab.wms.grinfeld.roman.scanner;

import android.content.Context;

/**
 * Created by GrinfeldRa
 */
public class FactoryScanner {

    public AbstractScanner getScanner(Context context, IOnScannerEvent iOnScannerEvent){
        try {
            Class.forName("com.symbol.emdk.EMDKManager");
            return BarcodeScannerEMDK.getInstance(context, iOnScannerEvent);
        } catch (ClassNotFoundException e) {
            return new BarcodeScannerBluetooth(context, iOnScannerEvent);
        }
    }

}
