package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.CheckOrderFragment;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.OrderFragment;

import static com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.CheckOrderFragment.LOAD_FROM_VIEW_PAGER;

/**
 * Created by GrinfeldRa
 */
public class OrderPagerFragment extends MvpAppCompatFragment implements KeyEventListener, OrderPagerView {

    private static final String KEY_DATA = "key";
    private static final String KEY_TYPE = "key_type";
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Context context;
    private Order order;
    private int orderType = 0;

    @InjectPresenter
    OrderPagerPresenter presenter;


    @ProvidePresenter
    OrderPagerPresenter providePresenter() {
        return new OrderPagerPresenter();
    }

    public static OrderPagerFragment newInstance(Order order, int type) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_DATA, order);
        args.putInt(KEY_TYPE, type);
        OrderPagerFragment fragment = new OrderPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).addKeyEventHandler(this);
        }
    }

    @Override
    public void onDetach() {
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).removeKeyEventHandler(this);
        }
        super.onDetach();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.order_pager_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager = view.findViewById(R.id.viewpager);
        tabLayout = view.findViewById(R.id.tab);
        if (getArguments() != null) {
            order = getArguments().getParcelable(KEY_DATA);
            orderType = getArguments().getInt(KEY_TYPE);
            if (getActivity() != null) {
                ((BaseFragmentActivity) getActivity()).setActionBarTitle(getString(R.string.acceptance, order.getNumDoc()));
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(OrderFragment.newInstance(order, orderType), "Приемка");
        viewPagerAdapter.addFragment(CheckOrderFragment.newInstance(order, LOAD_FROM_VIEW_PAGER), "Проверка");
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            presenter.loadGoods(order);
            return true;
        }
        return false;
    }


    @Override
    public void onCloseOrder(String s) {
        if (getActivity() != null) {
            new android.app.AlertDialog.Builder(context)
                    .setMessage(s)
                    .setPositiveButton("OK", (dialog, which) -> {
                        if (getActivity() != null) {
                            getActivity().onBackPressed();
                        }
                    }).show();
        }
    }

    @Override
    public void isAllGoodsAcceptedInOrder(boolean b) {
        if (b) {
            new AlertDialog.Builder(context)
                    .setMessage(getString(R.string.all_good_accepted))
                    .setPositiveButton("OK", (dialog, which) ->
                            showCompleteOrderDialog()).show();
        } else {
            showCompleteOrderDialog();
        }
    }

    @Override
    public void onCompleteCheckOrder(String message, Order order, int isClose) {
        if (message.isEmpty()) {
            presenter.closeOrder(order, isClose, orderType);
        } else {
            AlertDialog alertDialog1 = new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.close_order))
                    .setPositiveButton(getString(R.string.ok), (dialog1, which1) -> presenter.closeOrder(order, 0, orderType))
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create();
            if (!alertDialog1.isShowing()) {
                alertDialog1.show();
            }
        }
    }


    private void showCompleteOrderDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.confirmation))
                .setMessage(getString(R.string.close_order_fully, order.getName()))
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> presenter.checkOrder(order, 1, orderType))
                .setNeutralButton(getString(R.string.cancel), null)
                .setNegativeButton(getString(R.string.no), (dialog, which) -> {
                    AlertDialog alertDialog1 = new AlertDialog.Builder(context)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle(getString(R.string.confirmation))
                            .setMessage(getString(R.string.close_order))
                            .setPositiveButton(getString(R.string.ok), (dialog1, which1) -> presenter.checkOrder(order, 0, orderType))
                            .setNegativeButton(getString(R.string.cancel), null)
                            .create();
                    if (!alertDialog1.isShowing()) {
                        alertDialog1.show();
                    }
                })
                .create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
