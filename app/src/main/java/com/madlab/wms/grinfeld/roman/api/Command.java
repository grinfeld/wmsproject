package com.madlab.wms.grinfeld.roman.api;

import com.madlab.wms.grinfeld.roman.common.App;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;
import com.madlab.wms.grinfeld.roman.entities.shpment.Shipment;
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentOwner;
import com.madlab.wms.grinfeld.roman.entities.selection.Order;
import com.madlab.wms.grinfeld.roman.entities.selection.OrderItem;
import com.madlab.wms.grinfeld.roman.entities.selection.Weight;
import com.madlab.wms.grinfeld.roman.utils.Preference;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.ShipmentOwnerMorphFragmentKt.SELECTION_BOX;

/**
 * Created by GrinfeldRa
 */
public class Command {

    public static final int TYPE_GET = 0;
    public static final int TYPE_EDIT = 1;
    public static final int TYPE_CONDITION = 0;
    public static final int TYPE_DEFECTIVE = 1;
    public static final int TYPE_INVENTORY = 812;
    public static final int TYPE_CELL = 3;
    public static final int TYPE_EUZ = 1;
    public static final int TYPE_ORDER = 0;
    public static final int TYPE_RETURN = 1;

    public static String COM_AUTHORIZATION() {
        return String.format(Locale.getDefault(), "111;%s", Preference.getLoginCode());
    }

    public static String COM_BAILOR(int mode) {
        return String.format(Locale.getDefault(), "33;%d;%s", mode, Preference.getLoginCode());
    }

    public static String COM_ORDERS(int type, String code) {
        return String.format(Locale.getDefault(), "02;%s;%s;;%s", type, code, Preference.getLoginCode());
    }

    public static String COM_PRODUCTS(String codeOrder, String date) {
        return String.format(Locale.getDefault(), "31;%s;%s;%s", codeOrder, date, Preference.getLoginCode());
    }

    public static String COM_GET_PARAM_ON_GOODS(int typeLoad, String codeOrder, String date, int type, String scanCode) {
        return String.format(Locale.getDefault(), "08A;%s;%s;%s;%d;%s;", typeLoad, codeOrder, date, type, scanCode);
    }


    public static String COM_GET_PARAM_ON_PALLET(String numDoc, String dateDoc, String scanCode) {
        return String.format(Locale.getDefault(), "08B;%s;%s;%s;", numDoc, dateDoc, scanCode);
    }

    public static String COM_GET_NUMBER_ORDER(int type, String shortName, String name, String date) {
        return String.format(Locale.getDefault(), "04;0;%s;%s;%s;%s;", type, shortName, name, date);
    }


    public static String COM_GET_GOODS_IN_ORDER(int type, String numDoc, String dateDoc) {
        return String.format(Locale.getDefault(), "07;%s;%s;%s;%s;;;", type, numDoc, dateDoc, Preference.getOrderConditionMode());
    }


    public static String COM_RESERVE(int typeOrder, String numDoc, String dateDoc, String euz, String count, String te, String cellNumber, String prodDate, String expirationDate, String lootNumber, int type) {
        return String.format(Locale.getDefault(), "09;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;0;", typeOrder, numDoc, dateDoc, Preference.getOrderConditionMode(), euz,
                count, te, cellNumber, prodDate, expirationDate, lootNumber, type);
    }


    public static String COM_CHECK_ORDER_DOC(String numDoc, String dateDoc, int orderType, int isClose) {
        return String.format(Locale.getDefault(), "11C;%s;%s;%s;%d;", numDoc, dateDoc, orderType, isClose);
    }

    public static String COM_CLOSE_ORDER_DOC(String numDoc, String dateDoc, int orderType, int isClose) {
        return String.format(Locale.getDefault(), "11;%s;%s;%s;%d;", numDoc, dateDoc, orderType, isClose);
    }

    public static String COM_SEND_TO_PRINT_EUZ(String shortName, int coefficient, int count) {
        return String.format(Locale.getDefault(), "6;%s;%s;%s;", shortName, coefficient, count);
    }

    public static String COM_SEND_TO_PRINT_TE(int type, int count) {
        return String.format(Locale.getDefault(), "5;%s;%s;", type, count);
    }

    public static String COM_ENTER_BARCODE(String productCode, String barcode, int coefficient) {
        return String.format(Locale.getDefault(), "20;%s;%s;%s;", productCode, barcode, coefficient);
    }

    public static String COM_DELETE_POSITION_IN_ORDER(String numDoc, int position) {
        return String.format(Locale.getDefault(), "10;%s;%d;", numDoc, position);
    }

    public static String COM_GET_ACCESS_WORK(int typeWork) {
        return String.format(Locale.getDefault(), "1;%s;%s;%s", Preference.getLoginCode(), typeWork, Utils.getVersionName());
    }

    public static String COM_GET_INVENTORY(int type) {
        return String.format(Locale.getDefault(), "88;1;%s;;", type);
    }

    public static String COM_GET_PRODUCT_IN_INVENTORY(String inventoryNum, String createDate, int type) {
        return String.format(Locale.getDefault(), "85;%s;%s;%s;", inventoryNum, createDate, type);
    }

    public static String COM_ACCESS_INVENTORY_CELL(String barcode, int type) {
        return String.format(Locale.getDefault(), "84;%s;%s;", barcode, type);
    }

    public static String COM_GET_PRODUCT_FROM_BARCODE(String barcode) {
        return String.format(Locale.getDefault(), "36;%s;", barcode);
    }

    public static String COM_RESERVE_INVENTORY_CELL() {
        return "86;";
    }

    public static List<String> PARAM_RESERVE_INVENTORY_CELL(String numberDoc, String date,
                                                            ArrayList<InventoryCellProduct> inventoryCellProducts,
                                                            int typeInventory) {
        List<String> listParam = new ArrayList<>();
        numberDoc = numberDoc.replaceAll("\\s", "");
        listParam.add(String.format(Locale.getDefault(), "%s;%s;%s;%s;", numberDoc, date, typeInventory, inventoryCellProducts.size()));
        for (InventoryCellProduct product : inventoryCellProducts) {
            int position = product.getPosition();
            int isEdit = product.isEdited() ? 1 : 0;
            String bailorName = product.getBailorName();
            String codeGoods = product.getCodeGoods();
            double countFact = product.getCountFact();
            double countDefective = product.getCountDefective();
            String codeSeries = product.getCodeSeries();
            String productionDate = product.getProductionDate();
            String expirationDate = product.getExpirationDate();
            String party = product.getParty();
            listParam.add(String.format(Locale.getDefault(), "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;;",
                    position, isEdit, bailorName, codeGoods, countFact, countDefective, codeSeries, productionDate, expirationDate, party));
        }
        return listParam;
    }


    public static String COM_GET_BARCODE_FROM_SELECTION(String orderID, String orderType) {
        return String.format(Locale.getDefault(), "54;%s;%s;", orderID, orderType);
    }

    public static String COM_GET_WEIGHT_GOODS_BARCODE() {
        return "19;";
    }

    public static String COM_GET_LIST_PRINTERS() {
        return "202;";
    }

    public static String COM_NEW_SELECTION() {
        return "50;";
    }


    public static String COM_SEND_TO_PRINT_SELECTION(String addressPrinter, String mID, String mType) {
        return String.format(Locale.getDefault(), "202P;%s;%s;%s;", addressPrinter, mID, mType);
    }

    public static String COM_RESERVE_SELECTION_BOXES() {
        return "53;";
    }


    public static List<String> COM_RESERVE_SELECTION_BOXES_PARAM(Order order) {
        List<OrderItem> allOrders = App.getDatabase().orderSelectionDao().getAllOrders(order.id);
        int placedInZGZ = 0;
        int cntEntered = 0;
        Double diff;
        int diffH;
        StringBuilder zgz = new StringBuilder();
        StringBuilder tablePart = new StringBuilder();
        for (OrderItem item : allOrders) {
            diff = item.mEst - item.mNado;
            diffH = item.mHeads - item.mPlacedHeads;
            if (diff.compareTo(0.0) != 0 || diffH != 0) {
                ++cntEntered;
                tablePart.append(String.format(Locale.US, "%s;%s;%.3f;%s;%s;%s;%s;%s;%s;\n",
                        item.mTask, item.code, diff, diffH, item.mCell,
                        item.mHeadsCount > 0 ? 1 : 0,
                        item.mBestBeforeStart, item.mBestBeforeEnd,
                        item.mBoxNumber));
            }
            if (item.mZGZCell != null && !item.mZGZCell.isEmpty()) {
                ++placedInZGZ;
                zgz.append(String.format(Locale.getDefault(), "%s;%s;\n", item.mTE, item.mZGZCell));
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(Locale.getDefault(), "%s;%s;%s;%s%s;%s;\n", Preference.getLoginCode(),
                cntEntered, placedInZGZ, order.mTaskNumberAllString, order.mWeights.size(), Utils.getVersionName()));
        sb.append(tablePart);
        sb.append(zgz);
        if (order.mWeights.size() > 0) {
            for (Weight weight : order.mWeights) {
                sb.append(String.format(Locale.US, "%s;%s;%.3f;\n", weight.task, weight.code, weight.amount));
            }
        }
        String tmp = sb.deleteCharAt(sb.lastIndexOf("\n")).toString();
        return Arrays.asList(tmp.split("\n"));
    }

    public static String COM_CHECK_TE(String te) {
        return String.format(Locale.getDefault(), "12F;%s;", te);
    }

    public static String COM_PLACEMENT_ZGS() {
        return "62F;";
    }

    public static String COM_CHECK_PLACEMENT_TE(String te) {
        return String.format(Locale.getDefault(), "12;%s;", te);
    }

    public static String COM_PLACEMENT_CELL(String te, String cell) {
        return String.format(Locale.getDefault(), "13;%s;%s;", te, cell);
    }

    //14F; - список подпиток

    public static String COM_BLOCK_PLACEMENT_CELL(String cell) {
        return String.format(Locale.getDefault(), "32;%s;", cell);
    }


    public static String COM_UNBLOCK_PLACEMENT_CELL(String cell) {
        return String.format(Locale.getDefault(), "44;%s;", cell);
    }

    public static String COM_GET_PLACEMENT_CELL(String cell) {
        return String.format(Locale.getDefault(), "40;%s;", cell);
    }

    //42;330100000;ГКДАЛ58647;;
    public static String COM_GET_INFO_EUZ(String cell, String code) {
        return String.format(Locale.getDefault(), "42;%s;%s;;", cell, code);
    }

    //42;330100000;;14601075349372;1;0;
    public static String COM_GET_INFO_EUZ_FROM_CODE(String cell, String code) {
        return String.format(Locale.getDefault(), "42;%s;;%s;1;0;", cell, code);
    }

    //45;ОТТРН75487;ОТТРН;;
    public static String COM_GET_CELL_EMPTY(String code, String owner, String codeSeries) {
        return String.format(Locale.getDefault(), "45;%s;%s;%s;", code, owner, codeSeries);
    }

    //43;111000150;331007810;ОТТРН75487;;360;19.11.2019 08:20:33;ОТТРН;1; Назаровара Карина
    public static String COM_RESERVE_PLACEMENT(String cellFrom, String cellTo, String codeTE, String count, String timeStamp, String owner) {
        return String.format(Locale.getDefault(), "43;%s;%s;%s;;%s;%s;%s;1;", cellFrom, cellTo, codeTE, count, timeStamp, owner);
    }

    public static String COM_RESERVE_PLACEMENT_TRANSIT_TE(String te, String cell) {
        return String.format(Locale.getDefault(), "62T;%s;%s;", te, cell);
    }

    public static String COM_RESERVE_PLACEMENT_DEFECTIVE(String cellFrom, String cellTo, String codeTE, String count, String timeStamp, String owner, int type) {
        return String.format(Locale.getDefault(), "43b;%s;%s;%s;;%s;%s;%s;%s;", cellFrom, cellTo, codeTE, count, timeStamp, owner, type);
    }

    public static String COM_GET_LIST_ZONE() {
        return "89;";
    }

    public static String COM_LIST_EUZ_FROM_ZONE(String zone) {
        return String.format(Locale.getDefault(), "90;%s;", zone);
    }

    public static String COM_GET_INVENTORY_NUM(String code) {
        return String.format(Locale.getDefault(), "91;%s;2;1;", code);
    }

    public static String COM_GET_REPLENISHMENT_NUM() {
        return "18F;";
    }

    public static String COM_GET_REPLENISHMENT_TYPE(String num, String type) {
        return String.format(Locale.getDefault(), "15F;%s;%s;", num, type);
    }

    public static String COM_GET_REPLENISHMENT_GOODS(String num, String type) {
        return String.format(Locale.getDefault(), "16F;%s;%s;", num, type);
    }

    public static String COM_RESERVE_REPLENISHMENT(String num, String fromCell, String toCell, String docType) {
        return String.format(Locale.getDefault(), "17F;%s;%s;%s;%s;", num, fromCell, toCell, docType);
    }

    public static String COM_GET_LIST_ROUTE() {
        return "96T;";
    }


    public static String COM_GET_EUZ_FROM_ROUTE(String num) {
        return String.format(Locale.getDefault(), "96S;%s;", num);
    }

    public static String COM_GET_SHIPMENT(int type) {
        return String.format(Locale.getDefault(), "70;%s;", type);
    }


    public static String COM_GET_ITEM_SHIPMENT(int type, String code, String date) {
        return String.format(Locale.getDefault(), "71;%s;%s;%s;", type, code, date);
    }


    public static String COM_GET_ITEM_INFO_SHIPMENT(String type, String code, String nameOwner) {
        return String.format(Locale.getDefault(), "72;%s;%s;%s;", type, code, nameOwner);
    }

    public static String COM_PRINT_SHIPMENT() {
        return "75P;";
    }

    public static String COM_GET_GOODS_FROM_SHIPMENT(String te, String codeDoc) {
        return String.format(Locale.getDefault(), "72F;%s;%s;", te, codeDoc);
    }

    public static String COM_CLOSE_SHIPMENT(String value) {
        return String.format(Locale.getDefault(), "74;%s;",value);
    }

    public static String COM_COMPLETE_SHIPMENT() {
        return "75F;";
    }


    public static String COM_GET_DATA_FROM_CHECKING_BOXES_BY_BARCODE(String barcode) {
        return String.format(Locale.getDefault(), "96;0;%s;;", barcode);
    }

    public static String COM_GET_DATA_FROM_CHECKING_BOXES() {
        return "96;2;;;";
    }

    public static String COM_GET_DATA_FROM_CHECKING_BOXES_STEP2(String docNum, String date) {
        return String.format(Locale.getDefault(), "93;%s;%s;", docNum, date);
    }

    public static String RESERVE_CHECKING_BOXES() {
        return "92;";
    }

}
