package com.madlab.wms.grinfeld.roman.entities.parser;

import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.utils.DateParser;

import java.util.ArrayList;

public class OrderParser {

    public static ArrayList<Order> getList(String dataFromServer, String code) {
        ArrayList<Order> list = new ArrayList<>();
        if (!dataFromServer.contains("OK")) {
            String[] rows = dataFromServer.split("[|]");
            for (String row : rows) {
                String[] values = row.split(";");
                Order order = new Order(values[0],
                        DateParser.getDateLongFromString(values[1]),
                        values[2],
                        values[4],
                        values[5],
                        Integer.parseInt(values[6]),
                        Double.parseDouble(values[7].replace(',', '.')),
                        Double.parseDouble(values[8].replace(',', '.')),
                        code);
                list.add(order);
            }

        }
        return list;
    }
}
