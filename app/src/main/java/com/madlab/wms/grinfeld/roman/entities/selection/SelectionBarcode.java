package com.madlab.wms.grinfeld.roman.entities.selection;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by grinfeldra
 */
@Entity
public class SelectionBarcode {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String code;
    public String bacrode;
    public double quant;

    public SelectionBarcode(String code, String bacrode, double quant) {
        this.code = code;
        this.bacrode = bacrode;
        this.quant = quant;
    }
}
