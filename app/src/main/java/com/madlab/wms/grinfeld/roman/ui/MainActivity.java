package com.madlab.wms.grinfeld.roman.ui;

import androidx.fragment.app.Fragment;

import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.ui.authorization.AuthorizationFragment;

/**
 * Created by GrinfeldRa
 */
public class MainActivity extends BaseFragmentActivity{

    @Override
    protected Fragment getFragment() {
        return AuthorizationFragment.newInstance();
    }

}
