package com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.shipment_enter.shipment_goods

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentGoods
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.shipment_goods_fragment.*

/**
 * Created by grinfeldra
 */

const val KEY_SHIPMENT_GOODS = "key_shipment_goods"
const val KEY_TE = "key_te"

class ShipmentGoodsFragment : MvpAppCompatFragment(), AdapterView.OnItemSelectedListener {

    fun newInstance(shipment: ArrayList<ShipmentGoods>, te: String) = ShipmentGoodsFragment().apply {
        arguments = Bundle().apply {
            putParcelableArrayList(KEY_SHIPMENT_GOODS, shipment)
            putString(KEY_TE, te)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.shipment_goods_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val te = arguments?.getString(KEY_TE)
        (activity as BaseFragmentActivity).setActionBarTitle(te)
        val data = arguments?.getParcelableArrayList<ShipmentGoods>(KEY_SHIPMENT_GOODS)
        val myAdapter = MyAdapter<ShipmentGoods>(context, data)
        Utils.setParamFromHeader(view,"Код", "Кол-во")
        list_view.adapter = myAdapter
        list_view.onItemSelectedListener = this
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val shipmentGoods = parent?.getItemAtPosition(position) as ShipmentGoods
        txt1.text = shipmentGoods.nameGoods
    }

}