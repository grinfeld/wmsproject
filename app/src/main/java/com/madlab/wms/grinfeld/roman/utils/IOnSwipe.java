package com.madlab.wms.grinfeld.roman.utils;

/**
 * Created by grinfeldra
 */
public interface IOnSwipe {

    public void onSwipeRight();
    public void onSwipeLeft();

}
