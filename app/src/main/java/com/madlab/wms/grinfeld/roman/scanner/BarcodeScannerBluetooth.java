package com.madlab.wms.grinfeld.roman.scanner;

import android.content.Context;
import android.text.TextUtils;

import com.github.edwnmrtnz.BarcodeDataListener;
import com.github.edwnmrtnz.BarcodeScannerConnectionListener;
import com.github.edwnmrtnz.BarcodeScannerHelper;


/**
 * Created by GrinfeldRa
 */
public class BarcodeScannerBluetooth extends AbstractScanner implements BarcodeScannerConnectionListener, BarcodeDataListener {

    private static final String TAG = "#BScannerBluetooth";
    private Context context;
    private IOnScannerEvent iOnScannerEvent;

    public BarcodeScannerBluetooth(Context context, IOnScannerEvent iOnScannerEvent) {
        this.context = context;
        this.iOnScannerEvent = iOnScannerEvent;
    }

    @Override
    public void onResume() {
        BarcodeScannerHelper.getInstance().startBarcodeBrodcastReceiver(context);
        BarcodeScannerHelper.getInstance().setOnBarcodeDataListener(this);
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!BarcodeScannerHelper.getInstance().isConnected()) {
                    iOnScannerEvent.onStatusUpdateScanner("Connecting to barcode scanner...");
                    BarcodeScannerHelper.getInstance().connectScanner(BarcodeScannerBluetooth.this);
                } else {
                    iOnScannerEvent.onStatusUpdateScanner("You are already connected");
                }
            }
        }.start();
    }

    @Override
    public void onPause() {
        BarcodeScannerHelper.getInstance().stopBarcodeBroadcastReceiver(context);
    }

    @Override
    public void onStop() {

    }


    @Override
    public void onDisconnected() {
        iOnScannerEvent.onStatusUpdateScanner("Connection failed!");
    }

    @Override
    public void onConnected() {
        iOnScannerEvent.onStatusUpdateScanner("Connection success!");
    }

    @Override
    public void onDataReceived(String data) {
        if (!TextUtils.isEmpty(data.trim())) {
            iOnScannerEvent.onDataScanner(BarcodeParseMetaData.parseMetadata(data));
        }
    }
}
