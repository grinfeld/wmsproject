package com.madlab.wms.grinfeld.roman.entities

import android.os.Parcelable
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.parcel.Parcelize


/**
 * Created by grinfeldra
 *
 * 4 = ГКДАЛ80347;Китекет ягн. в соусе 28х85г;6048;Шт.;216 Кор. ;
 * 3 = 35246;2;3-08-023-5;330802350;3-08-000-0;330800000;1;
 * --------------------------------------------------------------------
 * 2 = ГКДАЛ67986;Китекет пауч курица в желе 28х85г;6048;Шт.;216 Кор. ;
 * 1 = 35246;1;3-08-004-3;330800430;3-08-000-0;330800000;1;
 */
@Parcelize
class Replenishment(var num: String, var position: Int, var fromCellTxt: String,
                    var fromCell: String, var toCellTxt: String, var toCell: String,
                    var countGoodsFromCell: Int, var codeGoods: String, var nameGoods: String,
                    var count: String, var baseEi: String, var otherEiTxt: String) : Parcelable {

    companion object {
        fun parseAnswer(answer: String): ArrayList<Replenishment> {
            val split = answer.split("|")
            val result = ArrayList<Replenishment>()
            for (i in 0..split.size - 2 step 2) {
                val strCell = split[i].split(";")
                val strGoods = split[i + 1].split(";")
                val num: String = strCell[0]
                val position: Int = Utils.toInt(strCell[1])
                val fromCellTxt: String = strCell[2]
                val fromCell: String = strCell[3]
                val toCellTxt: String = strCell[4]
                val toCell: String = strCell[5]
                val countGoodsFromCell: Int = Utils.toInt(strCell[6])
                val codeGoods: String = strGoods[0]
                val nameGoods: String = strGoods[1]
                val count: String = strGoods[2]
                val baseEi: String = strGoods[3]
                val otherEiTxt: String = strGoods[4]
                result.add(Replenishment(num, position, fromCellTxt, fromCell, toCellTxt, toCell,
                        countGoodsFromCell, codeGoods, nameGoods, count, baseEi, otherEiTxt))
            }
            return result
        }
    }
}