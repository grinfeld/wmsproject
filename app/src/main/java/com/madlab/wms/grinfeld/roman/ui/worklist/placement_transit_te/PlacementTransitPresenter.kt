package com.madlab.wms.grinfeld.roman.ui.worklist.placement_transit_te

import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.client.WMSClient
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.ui.worklist.placement_defective.PlacementDefectiveView
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by grinfeldra
 */
@InjectViewState
class PlacementTransitPresenter : BasePresenter<PlacementTransitView>() {

    fun reservePlacementTransit(te: String, cell: String) {
        mCompositeDisposable.add(WMSClient().send(Command.COM_RESERVE_PLACEMENT_TRANSIT_TE(te, cell))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showProgress() }
                .doFinally(viewState::hideProgress)
                .subscribe {
                    if (isSuccess(it)) {
                        viewState.onCompleteReservePlacementTransit()
                    }
                })
    }


}