package com.madlab.wms.grinfeld.roman.ui.worklist.placement_transit_te

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView

/**
 * Created by grinfeldra
 */
interface PlacementTransitView : BaseView {

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteReservePlacementTransit()

}