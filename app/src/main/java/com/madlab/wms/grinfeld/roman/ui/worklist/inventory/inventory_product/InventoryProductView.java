package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct;

import java.util.List;

/**
 * Created by GrinfeldRa
 */
interface InventoryProductView extends BaseView {

    @StateStrategyType(SkipStrategy.class)
    void onCompleteReserveCellInventory();
    @StateStrategyType(SkipStrategy.class)
    void onCompleteGetProduct(InventoryCellProduct product);
    @StateStrategyType(SkipStrategy.class)
    void onInitFieldMeasure(String textCount, String textCountDefective);
    @StateStrategyType(SkipStrategy.class)
    void onScannerExistProduct(List<InventoryCellProduct> listInventorySeries);
}
