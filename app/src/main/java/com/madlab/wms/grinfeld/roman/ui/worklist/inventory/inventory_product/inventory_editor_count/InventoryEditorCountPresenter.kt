package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.inventory_editor_count

import android.view.View
import com.arellomobile.mvp.InjectViewState
import com.madlab.wms.grinfeld.roman.common.BasePresenter
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct
import java.text.NumberFormat

/**
 * Created by GrinfeldRa
 */
@InjectViewState
class InventoryEditorCountPresenter : BasePresenter<InventoryEditorCountView>() {

    fun onDeleteCountProduct(v: View) {
        viewState.onDeleteCountProduct(v)
    }

    fun onInitFieldsMeasure(product: InventoryCellProduct?, quantum: Double, total_count: Double, total_count_defective: Double) {
        var rest = 0.0
        val accepted: Double
        val acceptedDefective: Double
        if (total_count != 0.0) {
            if (quantum != 1.0) {
                rest = total_count % quantum
            }
            accepted = (total_count - rest) / quantum + product!!.countFact
        } else {
            accepted = product!!.countFact
        }
        if (total_count_defective != 0.0) {
            if (quantum != 1.0) {
                rest = total_count_defective % quantum
            }
            acceptedDefective = (total_count_defective - rest) / quantum + product.countDefective
        } else {
            acceptedDefective = product.countDefective
        }
        viewState.onInitFieldsMeasure(prepareMessage(accepted * quantum, product), prepareMessage(acceptedDefective * quantum, product))
    }

    private fun prepareMessage(v: Double, product: InventoryCellProduct): String {
        var result = ""
        val numberFormat = NumberFormat.getInstance()
        if (product.measureQuant != 0.0) {
            val restEi1Required = v % product.measureQuant
            val strRestEi1 = if (restEi1Required == 0.0) "" else numberFormat.format(restEi1Required) + " " + product.measureInitName
            result = numberFormat.format((v - restEi1Required) / product.measureQuant) + product.measureMoreName + " " + strRestEi1 + " ("
        }
        val endMessage = if (result.isEmpty()) "" else ")"
        result += numberFormat.format(if (v < 0) 0 else v) + product.measureInitName + endMessage
        return result
    }

}