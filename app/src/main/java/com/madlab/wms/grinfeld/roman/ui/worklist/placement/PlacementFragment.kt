package com.madlab.wms.grinfeld.roman.ui.worklist.placement

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.placement.enter_count.PlacementEnterCountFragment
import com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.PREFIX_CELL
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.placement_fragment.*
import kotlinx.android.synthetic.main.placement_fragment.list_view
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
const val KEY_TIMESTAMP = "timestamp"
const val KEY_TYPE_PLACEMENT = "type_placement"
const val TYPE_OTHER_PLACEMENT = 0
const val TYPE_RETURN_PLACEMENT = 1

class PlacementFragment : MvpAppCompatFragment(), PlacementView, IOnScannerEvent, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    private var progressOwner: ProgressOwner? = null
    private var scanner: AbstractScanner? = null
    private lateinit var te: String
    private lateinit var cell: String
    private var isCompleteCheckTE = false
    private var typePlacement = 0

    @InjectPresenter
    lateinit var presenter: PlacementPresenter

    @ProvidePresenter
    fun providePresenter() = PlacementPresenter()


    fun newInstance(timestamp: String, type: Int) = PlacementFragment().apply {
        arguments = Bundle().apply {
            putString(KEY_TIMESTAMP, timestamp)
            putInt(KEY_TYPE_PLACEMENT, type)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner?.onResume()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.placement_title))
        return inflater.inflate(R.layout.placement_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        typePlacement = arguments?.getInt(KEY_TYPE_PLACEMENT)!!
        when (typePlacement) {
            TYPE_OTHER_PLACEMENT -> txt_info.text = getString(R.string.scan_te)
            TYPE_RETURN_PLACEMENT -> txt_info.text = getString(R.string.placement_return)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val placementCell = parent?.getItemAtPosition(position) as PlacementCell
        txt_name.text = getString(R.string.footer_1, placementCell.code, placementCell.name)
        txt_rest.text = getString(R.string.footer_2, placementCell.otherEiTxt)
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val placementCell = parent?.getItemAtPosition(position) as PlacementCell
        presenter.getInfoEuz(placementCell, cell)
    }


    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner?.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner?.setProgressState(false)
    }

    override fun onCompleteLoadDataAfterOnItemClick(placementCell: PlacementCell,
                                                    cellEmptyList: ArrayList<String>) {
        (activity as MainActivity).changeFragment(PlacementEnterCountFragment().newInstance(placementCell, cellEmptyList))
    }

    override fun onCompleteGetPlacementCell(placementCell: List<PlacementCell>) {
        txt_info.visibility = View.GONE
        list_place_view.visibility = View.VISIBLE
        val adapter = MyAdapter(context, placementCell)
        list_view.adapter = adapter
        list_view.onItemClickListener = this
        list_view.onItemSelectedListener = this
        val weightLeft = 0.6f
        val weightRight = 1f
        adapter.setWeight(weightLeft, weightRight)
        Utils.setParamFromHeader(view, weightLeft, weightRight, getString(R.string.code), getString(R.string.name))
    }


    override fun onCompleteStartPlacementCell(cell: String, cellTxt: String) {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.start_placement_cell, cellTxt))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        arguments?.getString(KEY_TIMESTAMP)?.let { it1 -> presenter.getPlacementCell(cell, it1, cellTxt) }
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }

    override fun onCompleteCheckTE(te: String, cellTxt: String, cell: String) {
        txt_info.text = getString(R.string.placement_te, te, cellTxt)
        this.te = te
        this.cell = cell
        isCompleteCheckTE = true
    }

    override fun onCompletePlacementCell() {
        txt_info.text = getString(R.string.scan_te)
        isCompleteCheckTE = false
    }

    override fun onCompleteGetInfoEuz(code: String) {
        val myAdapter = list_view.adapter as MyAdapter<*>
        val listItem = myAdapter.allItem as List<PlacementCell>
        val placementCell = listItem.find { it.code == code }
        placementCell?.let { presenter.getInfoEuz(it, cell) }
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (typePlacement == TYPE_RETURN_PLACEMENT) {
            if (list_place_view.isVisible) {
                if (goodsCard?.barcode != null && goodsCard.barcode.substring(0, 2) != PREFIX_CELL) {
                    presenter.getInfoEuz(goodsCard.barcode, cell)
                } else {
                    showError(getString(R.string.error_barcode))
                }
            } else {
                if (goodsCard?.barcode != null && goodsCard.barcode.substring(0, 2) == PREFIX_CELL) {
                    cell = goodsCard.barcode.substring(2)
                    presenter.startPlacementCell(cell)
                } else {
                    showError(getString(R.string.error_barcode_cell))
                }
            }
        } else {
            if (isCompleteCheckTE) {
                if (goodsCard?.barcode != null && goodsCard.barcode.substring(0, 2) == PREFIX_CELL) {
                    val cell = goodsCard.barcode.substring(2)
                    presenter.placementCell(te, cell)
                } else {
                    showError(getString(R.string.error_barcode_cell))
                }
            } else {
                if (goodsCard?.barcode == null) {
                    if (goodsCard?.te != null)
                        presenter.checkTE(goodsCard.te)
                } else {
                    if (goodsCard.barcode.substring(0, 2) == PREFIX_CELL) {
                        cell = goodsCard.barcode.substring(2)
                        presenter.startPlacementCell(cell)
                    } else {
                        showError(getString(R.string.error_barcode_te))
                    }
                }
            }
        }
    }


    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun onPause() {
        super.onPause()
        scanner?.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner?.onStop()
    }

}
