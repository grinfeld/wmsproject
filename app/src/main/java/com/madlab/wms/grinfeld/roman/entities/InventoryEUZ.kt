package com.madlab.wms.grinfeld.roman.entities

import android.os.Parcelable
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import kotlinx.android.parcel.Parcelize

/**
 * Created by grinfeldra
 */
@Parcelize
class InventoryEUZ(var code: String, var name: String, var status: String,
                   var owner: String, var warehouse: String) : AbstractData(), Parcelable {

    override fun getLeftText(): String {
        return name
    }

    override fun getRightText(): String {
        return status
    }

    companion object {
        fun parse(answer: String): ArrayList<InventoryEUZ> {
            val result = ArrayList<InventoryEUZ>()
            for (data in answer.split("|")) {
                val split = data.split(";")
                val code = split[0]
                val name = split[1]
                val status = split[2]
                val owner = split[3]
                val warehouse = split[4]
                result.add(InventoryEUZ(code, name, status, owner, warehouse))
            }
            return result
        }
    }

}