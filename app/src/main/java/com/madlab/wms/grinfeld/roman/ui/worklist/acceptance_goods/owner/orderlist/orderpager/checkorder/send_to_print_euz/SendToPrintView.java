package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.checkorder.send_to_print_euz;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;

/**
 * Created by GrinfeldRA
 */
public interface SendToPrintView extends BaseView {
    @StateStrategyType(SkipStrategy.class)
    void onCompleteSentToPrint();
}
