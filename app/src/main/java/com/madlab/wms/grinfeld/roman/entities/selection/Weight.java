package com.madlab.wms.grinfeld.roman.entities.selection;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by grinfeldra
 */
@Entity
public class Weight {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String task;
    public String code;
    public double amount;

    public Weight(String task, String code, double amount) {
        this.task = task;
        this.code = code;
        this.amount = amount;
    }
}
