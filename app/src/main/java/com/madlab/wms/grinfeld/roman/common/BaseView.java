package com.madlab.wms.grinfeld.roman.common;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by GrinfeldRa
 */
public interface BaseView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void showError(String error);

    @StateStrategyType(AddToEndStrategy.class)
    void showProgress();

    @StateStrategyType(AddToEndStrategy.class)
    void hideProgress();

}
