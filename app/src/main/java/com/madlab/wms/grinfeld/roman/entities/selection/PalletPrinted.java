package com.madlab.wms.grinfeld.roman.entities.selection;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by grinfeldra
 */
@Entity
public class PalletPrinted {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public byte mTENumber;
    public boolean isPrinted;

    public PalletPrinted(byte mTENumber, boolean isPrinted) {
        this.mTENumber = mTENumber;
        this.isPrinted = isPrinted;
    }


    public byte getmTENumber() {
        return mTENumber;
    }

    public boolean isPrinted() {
        return isPrinted;
    }

    public void setmTENumber(byte mTENumber) {
        this.mTENumber = mTENumber;
    }

    public void setPrinted(boolean printed) {
        isPrinted = printed;
    }

    @NonNull
    @Override
    public String toString() {
        return "Поддон №" + mTENumber;
    }
}
