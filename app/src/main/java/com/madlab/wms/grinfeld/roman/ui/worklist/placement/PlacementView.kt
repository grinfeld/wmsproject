package com.madlab.wms.grinfeld.roman.ui.worklist.placement

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.placement.PlacementCell
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
interface PlacementView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onCompleteCheckTE(te: String, cellTxt: String, cell: String)

    @StateStrategyType(SkipStrategy::class)
    fun onCompletePlacementCell()

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteStartPlacementCell(cell: String, cellTxt: String)

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteGetPlacementCell(placementCell: List<PlacementCell>)

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteLoadDataAfterOnItemClick(placementCell: PlacementCell, cellEmptyList: ArrayList<String>)

    @StateStrategyType(SkipStrategy::class)
    fun onCompleteGetInfoEuz(code: String)

}