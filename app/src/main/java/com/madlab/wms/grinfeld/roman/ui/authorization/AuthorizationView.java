package com.madlab.wms.grinfeld.roman.ui.authorization;

import android.net.Uri;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;

/**
 * Created by GrinfeldRa
 */
public interface AuthorizationView extends BaseView {
    @StateStrategyType(SkipStrategy.class)
    void openWorkListFragment(String works);
    @StateStrategyType(SkipStrategy.class)
    void onUpdateApk(Uri uri);
    @StateStrategyType(SkipStrategy.class)
    void onNoUpdateRequired();
}
