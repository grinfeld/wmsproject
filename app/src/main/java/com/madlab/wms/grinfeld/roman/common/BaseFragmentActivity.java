package com.madlab.wms.grinfeld.roman.common;

import android.app.ActivityManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.utils.IRFACItemClick;
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.utils.Utils;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by GrinfeldRa
 */
public abstract class BaseFragmentActivity extends AppCompatActivity implements ProgressOwner, RapidFloatingActionContentLabelList.OnRapidFloatingActionContentLabelListListener {

    private final CopyOnWriteArrayList<KeyEventListener> keyEventHandlerList = new CopyOnWriteArrayList<>();

    private TextView tvTitle;
    private RapidFloatingActionLayout rfaLayout;
    private RapidFloatingActionButton rfaBtn;
    private RapidFloatingActionHelper rfabHelper;
    private RapidFloatingActionContentLabelList rfaContent;
    private IRFACItemClick irfacItemClick;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);

        rfaContent = new RapidFloatingActionContentLabelList(this);
        rfaContent.setOnRapidFloatingActionContentLabelListListener(this);
        rfaLayout = findViewById(R.id.activity_main_rfal);
        rfaBtn = findViewById(R.id.activity_main_rfab);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
            tvTitle = viewActionBar.findViewById(R.id.title);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.NO_GRAVITY);
            actionBar.setCustomView(viewActionBar, params);
        }
        if (savedInstanceState == null) {
            changeFragment(getFragment());
        }
    }

    public void setFloatingMenu(List<RFACLabelItem> items, IRFACItemClick irfacItemClick) {
        this.irfacItemClick = irfacItemClick;
        rfaBtn.setVisibility(View.VISIBLE);
        rfaContent.setItems(items);
        rfabHelper = new RapidFloatingActionHelper(
                this,
                rfaLayout,
                rfaBtn,
                rfaContent
        ).build();
    }


    public void setActionBarTitle(String title) {
        this.tvTitle.setText(title);
    }

    protected abstract Fragment getFragment();

    public void changeFragment(Fragment fragment) {
        setInvisibleFloating();
        boolean addToBackStack = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer) != null;
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment);

        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        Utils.closeKeyboard(this);
        transaction.commitAllowingStateLoss();
    }

    public void setInvisibleFloating() {
        rfaBtn.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof IRFACItemClick)) {
            setInvisibleFloating();
        }

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            Utils.closeKeyboard(this);
        }
    }


    @Override
    public void setProgressState(boolean refreshing) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment != null) {
            ProgressBar progressBar = Objects.requireNonNull(fragment.getActivity()).findViewById(R.id.progress_horizontal);
            if (progressBar != null)
                progressBar.setVisibility(refreshing ? View.VISIBLE : View.INVISIBLE);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        for (KeyEventListener handler : keyEventHandlerList) {
            if (handleKeyEvent(handler, event)) {
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void addKeyEventHandler(@NonNull KeyEventListener handler) {
        keyEventHandlerList.add(handler);
    }

    public void removeKeyEventHandler(@NonNull KeyEventListener handler) {
        keyEventHandlerList.remove(handler);
    }

    private boolean handleKeyEvent(@Nullable KeyEventListener listener, KeyEvent event) {
        return listener != null
                && listener.isVisible()
                && (event.getAction() == KeyEvent.ACTION_DOWN || event.getAction() == KeyEvent.ACTION_UP)
                && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getKeyCode() == KeyEvent.KEYCODE_BACK
                || event.getKeyCode() == KeyEvent.KEYCODE_DEL || event.getKeyCode() == KeyEvent.KEYCODE_E
                || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT
                || event.getKeyCode() == KeyEvent.KEYCODE_B || event.getKeyCode() == KeyEvent.KEYCODE_K
                || event.getKeyCode() == KeyEvent.KEYCODE_O || event.getKeyCode() == KeyEvent.KEYCODE_P
                || event.getKeyCode() == KeyEvent.KEYCODE_T || event.getKeyCode() == KeyEvent.KEYCODE_X
                || event.getKeyCode() == KeyEvent.KEYCODE_ESCAPE || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP
                || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_A
                || event.getKeyCode() == KeyEvent.KEYCODE_H || event.getKeyCode() == KeyEvent.KEYCODE_Y
                || event.getKeyCode() == KeyEvent.KEYCODE_I)
                && listener.onKey(listener.getView(), event.getKeyCode(), event);
    }


    @Override
    public void onRFACItemLabelClick(int position, RFACLabelItem item) {
        irfacItemClick.onClickRFA(position, item);
        rfabHelper.toggleContent();
    }

    @Override
    public void onRFACItemIconClick(int position, RFACLabelItem item) {
        irfacItemClick.onClickRFA(position, item);
        rfabHelper.toggleContent();
    }
}
