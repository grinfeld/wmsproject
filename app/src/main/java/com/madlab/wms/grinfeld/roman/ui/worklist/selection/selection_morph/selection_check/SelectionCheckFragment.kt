package com.madlab.wms.grinfeld.roman.ui.worklist.selection.selection_morph.selection_check

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.AbstractData
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.selection.Order
import com.madlab.wms.grinfeld.roman.entities.selection.OrderItem
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.selection_check_fragment.*

/**
 * Created by grinfeldra
 */

const val KEY_ORDER_ITEM = "order_item"
const val KEY_IS_EXIT = "key_is_exit"
const val KEY_ORDER = "order"

class SelectionCheckFragment : MvpAppCompatFragment(), AdapterView.OnItemSelectedListener, KeyEventListener, SelectionCheckView {

    @InjectPresenter
    lateinit var presenter: SelectionCheckPresenter

    @ProvidePresenter
    fun providePresenter() = SelectionCheckPresenter()

    private lateinit var order: Order

    private lateinit var progressOwner: ProgressOwner


    fun newInstance(items: ArrayList<OrderItem>, isExit: Boolean, order: Order) = SelectionCheckFragment().apply {
        arguments = Bundle().apply {
            putParcelableArrayList(KEY_ORDER_ITEM, items)
            putBoolean(KEY_IS_EXIT, isExit)
            putParcelable(KEY_ORDER, order)
        }
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (KeyEvent.KEYCODE_ENTER == keyCode && event?.action == KeyEvent.ACTION_UP) {
            fragmentManager?.popBackStack()
            return true
        } else if ((keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE)
                && event?.action == KeyEvent.ACTION_UP) {
            if (arguments?.getBoolean(KEY_IS_EXIT)!!) {
                completeDialog()
            } else {
                fragmentManager?.popBackStack()
            }
            return true
        }
        return false
    }

    private fun completeDialog() {
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(if (order.isComplete(order.id, order.mPercentMaxWeight))
                        getString(R.string.order_with_boxes_complete) else
                        getString(R.string.order_with_boxes_not_complete))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        presenter.reserveSelectionOrder(order)
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }


    override fun onCompleteReserveSelectionOrder() {
        val fm = activity?.supportFragmentManager
        val alertDialog = context?.let {
            AlertDialog.Builder(it)
                    .setMessage(getString(R.string.selection_is_complete))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        fm?.popBackStack()
                        fm?.popBackStack()
                    }
                    .setNegativeButton(getString(R.string.cancel)) { _, _ ->
                        for (i in 0 until fm?.backStackEntryCount!!) {
                            fm.popBackStack()
                        }
                    }
                    .create()
        }
        if (!alertDialog?.isShowing!!) {
            alertDialog.show()
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseFragmentActivity) {
            context.addKeyEventHandler(this)
        }
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onDetach() {
        if (context is BaseFragmentActivity) {
            (context as BaseFragmentActivity).removeKeyEventHandler(this)
        }
        super.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.selection_check_title))
        return inflater.inflate(R.layout.selection_check_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        order = arguments?.getParcelable(KEY_ORDER)!!
        val weightLeft = 1f
        val weightRight = 0.5f
        val listAdapter = MyAdapter<AbstractData>(context)
        val orderItems = arguments?.getParcelableArrayList<OrderItem>(KEY_ORDER_ITEM)
        listAdapter.setData(orderItems as List<AbstractData>?)
        Utils.setParamFromHeader(view, weightLeft, weightRight, getString(R.string.name), getString(R.string.str_count_optional))
        listAdapter.setWeight(weightLeft, weightRight)
        list_view.adapter = listAdapter
        list_view.onItemSelectedListener = this
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val orderItem = parent?.getItemAtPosition(position) as OrderItem
        txt_name.text = orderItem.name
        txt_code.text = getString(R.string.code_goods, orderItem.code)
        txt_need.text = getString(R.string.need, orderItem.need)
        txt_exist.text = getString(R.string.exist, orderItem.exist)
        txt_meta.text = if (orderItem.mClient.isEmpty()) getString(R.string.str_summary) else orderItem.mClient
    }


    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }
}