package com.madlab.wms.grinfeld.roman.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


public class ProductParam implements Parcelable {

    private static final String TAG = "#ProductParam";
    //тип данных (Единица учёта запаса - товар)
    private String euz;
    //код товара
    private String code;
    //название
    private String name;
    //надо
    private double required;
    //не требуется ввод габаритов (1 - требуется)
    private boolean isGab;
    //текущий коэффициент (множитель)
    private double koef;
    //название БЕИ
    private String unit;
    //признак весового товара. Если равен 1, то весовой, если 0 - штучный
    private boolean isWeight;
    //Дата изготовления
    private boolean isDateCreate;
    //Срок годности
    private boolean isValidUntil;
    //Требуется ввод номера партии
    private boolean isNumberPart;
    //Уже принятое количество
    private double accepted;
    //Тип поддона (typeTE). Не могу сказать, где используется, надо разбираться
    private String typeTE;
    //Тип товара (typeTov). Не могу сказать, где используется, надо разбираться
    private String typeTov;
    //ЕИ1
    private String ei1;
    //Коэф для ЕИ1
    private double koefEi1;
    //ЕИ2
    private String ei2;
    //Коэф для ЕИ2
    private double koefEi2;
    //ЕИ3
    private String ei3;
    //Коэф для ЕИ3
    private double koefEi3;
    //ЕИ4
    private String ei4;
    //Коэф для ЕИ4
    private double koefEi4;
    //срок годности
    private int shelfLife;
    //1 - год, 2 - месяц, 3 - день
    private int shelfLifeType;

    //1 = 1;ГКДАЛ62252;Долмио соус Традиционный 8х210г;1016;EU1733467;1;0;1;12/02/2019;12/12/2019;;127 Кор. ;332900400;РЦ Приемка;
//Ä��ОК;EUZ;ГКДАЛ62252;Долмио соус Традиционный 8х210г;8;0;8;Шт.;0;1;1;0;1036;546;0;Кор.;8;;;;;;;545;3;
    public ProductParam(String data) {
        try {
            String[] parseData = data.split(";");
            euz = parseData[1];
            code = parseData[2];
            name = parseData[3];
            required = parseData[4].equals("") ? 0 : Double.parseDouble(parseData[4].replace(",","."));
            isGab = parseData[5].equals("1");
            koef = parseData[6].equals("") ? 0 : Double.parseDouble(parseData[6].replace(",","."));
            unit = parseData[7];
            isWeight = parseData[8].equals("1");
            isDateCreate = parseData[9].equals("1");
            isValidUntil = parseData[10].equals("1");
            isNumberPart = parseData[11].equals("1");
            accepted = parseData[12].equals("") ? 0 : Double.parseDouble(parseData[12].replace(",","."));
            typeTE = parseData[13];
            typeTov = parseData[14];
            ei1 = parseData[15];
            koefEi1 = parseData[16].equals("") ? 0 : Double.parseDouble(parseData[16].replace(",","."));
            ei2 = parseData[17];
            koefEi2 = parseData[18].equals("") ? 0 : Double.parseDouble(parseData[18].replace(",","."));
            ei3 = parseData[19];
            koefEi3 = parseData[20].equals("") ? 0 : Double.parseDouble(parseData[20].replace(",","."));
            ei4 = parseData[21];
            koefEi4 = parseData[22].equals("") ? 0 : Double.parseDouble(parseData[22].replace(",","."));
            String strShelfLife = parseData[23].replaceAll("\\s", "").trim();
            shelfLife = Integer.parseInt(strShelfLife);
            shelfLifeType = Integer.parseInt(parseData[24]);
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    protected ProductParam(Parcel in) {
        euz = in.readString();
        code = in.readString();
        name = in.readString();
        required = in.readDouble();
        isGab = in.readByte() != 0;
        koef = in.readDouble();
        unit = in.readString();
        isWeight = in.readByte() != 0;
        isDateCreate = in.readByte() != 0;
        isValidUntil = in.readByte() != 0;
        isNumberPart = in.readByte() != 0;
        accepted = in.readDouble();
        typeTE = in.readString();
        typeTov = in.readString();
        ei1 = in.readString();
        koefEi1 = in.readDouble();
        ei2 = in.readString();
        koefEi2 = in.readDouble();
        ei3 = in.readString();
        koefEi3 = in.readDouble();
        ei4 = in.readString();
        koefEi4 = in.readDouble();
        shelfLife = in.readInt();
        shelfLifeType = in.readInt();
    }

    public static final Creator<ProductParam> CREATOR = new Creator<ProductParam>() {
        @Override
        public ProductParam createFromParcel(Parcel in) {
            return new ProductParam(in);
        }

        @Override
        public ProductParam[] newArray(int size) {
            return new ProductParam[size];
        }
    };

    public void setKoefEi1(double koefEi1) {
        this.koefEi1 = koefEi1;
    }

    public void setEi1(String ei1) {
        this.ei1 = ei1;
    }

    public String getEuz() {
        return euz;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getRequired() {
        return required;
    }

    public boolean isGab() {
        return isGab;
    }

    public double getKoef() {
        return koef;
    }

    public String getUnit() {
        return unit;
    }

    public boolean isWeight() {
        return isWeight;
    }

    public boolean isDateCreate() {
        return isDateCreate;
    }

    public boolean isValidUntil() {
        return isValidUntil;
    }

    public boolean isNumberPart() {
        return isNumberPart;
    }

    public double getAccepted() {
        return accepted;
    }

    public String getTypeTE() {
        return typeTE;
    }

    public String getTypeTov() {
        return typeTov;
    }

    public String getEi1() {
        return ei1;
    }

    public double getKoefEi1() {
        return koefEi1;
    }

    public String getEi2() {
        return ei2;
    }

    public double getKoefEi2() {
        return koefEi2;
    }

    public String getEi3() {
        return ei3;
    }

    public double getKoefEi3() {
        return koefEi3;
    }

    public String getEi4() {
        return ei4;
    }

    public double getKoefEi4() {
        return koefEi4;
    }

    public void setEuz(String euz) {
        this.euz = euz;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRequired(double required) {
        this.required = required;
    }

    public void setGab(boolean gab) {
        isGab = gab;
    }

    public void setKoef(int koef) {
        this.koef = koef;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setWeight(boolean weight) {
        isWeight = weight;
    }

    public void setDateCreate(boolean dateCreate) {
        isDateCreate = dateCreate;
    }

    public void setValidUntil(boolean validUntil) {
        isValidUntil = validUntil;
    }

    public void setNumberPart(boolean numberPart) {
        isNumberPart = numberPart;
    }

    public void setAccepted(double accepted) {
        this.accepted = accepted;
    }

    public int getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(int shelfLife) {
        this.shelfLife = shelfLife;
    }

    public int getShelfLifeType() {
        return shelfLifeType;
    }

    public void setShelfLifeType(int shelfLifeType) {
        this.shelfLifeType = shelfLifeType;
    }

    @Override
    public String toString() {
        return "ProductParametr{" +
                "te='" + euz + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", required=" + required +
                ", isGab=" + isGab +
                ", koef=" + koef +
                ", unit='" + unit + '\'' +
                ", isWeight=" + isWeight +
                ", isDateCreate=" + isDateCreate +
                ", isValidUntil=" + isValidUntil +
                ", isNumberPart=" + isNumberPart +
                ", accepted=" + accepted +
                ", typeTE='" + typeTE + '\'' +
                ", typeTov='" + typeTov + '\'' +
                ", ei1='" + ei1 + '\'' +
                ", koefEi1=" + koefEi1 +
                ", ei2='" + ei2 + '\'' +
                ", koefEi2=" + koefEi2 +
                ", ei3='" + ei3 + '\'' +
                ", koefEi3=" + koefEi3 +
                ", ei4='" + ei4 + '\'' +
                ", koefEi4=" + koefEi4 +
                ", shelfLife=" + shelfLife +
                ", shelfLifeType=" + shelfLifeType + "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(euz);
        dest.writeString(code);
        dest.writeString(name);
        dest.writeDouble(required);
        dest.writeByte((byte) (isGab ? 1 : 0));
        dest.writeDouble(koef);
        dest.writeString(unit);
        dest.writeByte((byte) (isWeight ? 1 : 0));
        dest.writeByte((byte) (isDateCreate ? 1 : 0));
        dest.writeByte((byte) (isValidUntil ? 1 : 0));
        dest.writeByte((byte) (isNumberPart ? 1 : 0));
        dest.writeDouble(accepted);
        dest.writeString(typeTE);
        dest.writeString(typeTov);
        dest.writeString(ei1);
        dest.writeDouble(koefEi1);
        dest.writeString(ei2);
        dest.writeDouble(koefEi2);
        dest.writeString(ei3);
        dest.writeDouble(koefEi3);
        dest.writeString(ei4);
        dest.writeDouble(koefEi4);
        dest.writeInt(shelfLife);
        dest.writeInt(shelfLifeType);
    }
}