package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.madlab.wms.grinfeld.roman.common.BaseView;
import com.madlab.wms.grinfeld.roman.entities.Order;

/**
 * Created by GrinfeldRa
 */
interface OrderPagerView extends BaseView {

    @StateStrategyType(SkipStrategy.class)
    void onCloseOrder(String s);

    @StateStrategyType(SkipStrategy.class)
    void isAllGoodsAcceptedInOrder(boolean b);

    @StateStrategyType(SkipStrategy.class)
    void onCompleteCheckOrder(String message, Order order, int isClose);
}
