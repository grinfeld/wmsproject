package com.madlab.wms.grinfeld.roman.ui.worklist.shipment.shipment_owner_morph.shipment_enter

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.madlab.wms.grinfeld.roman.common.BaseView
import com.madlab.wms.grinfeld.roman.entities.shpment.ShipmentGoods
import java.util.ArrayList

/**
 * Created by grinfeldra
 */
interface ShipmentEnterView : BaseView {
    @StateStrategyType(SkipStrategy::class)
    fun onCompleteGetGoodsFromShipment(data: ArrayList<ShipmentGoods>, te: String)

    @StateStrategyType(SkipStrategy::class)
    fun onComplete()
}