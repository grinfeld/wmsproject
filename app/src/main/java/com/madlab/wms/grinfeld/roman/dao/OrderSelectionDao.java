package com.madlab.wms.grinfeld.roman.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.madlab.wms.grinfeld.roman.entities.selection.Order;
import com.madlab.wms.grinfeld.roman.entities.selection.OrderItem;
import com.madlab.wms.grinfeld.roman.entities.selection.MyPrinter;

import java.util.List;

/**
 * Created by grinfeldra
 */
@Dao
public interface OrderSelectionDao {

    @Query("SELECT * FROM `order` WHERE mTaskNumber =:mTaskNumber")
    LiveData<Order> getLiveOrder(String mTaskNumber);

    @Query("SELECT * FROM `myprinter`")
    List<MyPrinter> getPrinters();

    @Query("SELECT * FROM `order` WHERE mTaskNumber =:mTaskNumber")
    Order getOrder(String mTaskNumber);

    @Query("SELECT * FROM OrderItem WHERE orderId =:orderId")
    List<OrderItem> getAllOrders(int orderId);

    @Query("SELECT * FROM OrderItem WHERE `Index` =:index AND orderId =:orderId")
    LiveData<OrderItem> getLiveOrderItem(int index, int orderId);

    @Query("SELECT * FROM OrderItem WHERE `Index` =:index AND orderId =:orderId")
    OrderItem getOrderItem(int index, int orderId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MyPrinter printers);

    @Insert()
    void insert(Order employee);

    @Insert()
    void insert(OrderItem orderItem);

    @Update
    void update(Order employee);

    @Update
    void update(OrderItem employee);

    @Delete
    void delete(Order employee);

    @Query("DELETE FROM OrderItem WHERE orderId =:orderId")
    void deleteAllOrderItem(int orderId);

    @Query("DELETE FROM myprinter")
    void deleteAllPrinter();
}
