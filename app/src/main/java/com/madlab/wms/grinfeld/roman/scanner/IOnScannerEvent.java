package com.madlab.wms.grinfeld.roman.scanner;

import com.madlab.wms.grinfeld.roman.entities.ScannerData;

public interface IOnScannerEvent {

    void onDataScanner(ScannerData goodsCard);

    void onStatusUpdateScanner(String scanStatus);

    void onErrorScanner(String error);
}
