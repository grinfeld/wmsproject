package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.madlab.wms.grinfeld.roman.R;
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter;
import com.madlab.wms.grinfeld.roman.adapter.order.OrderAdapter;
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity;
import com.madlab.wms.grinfeld.roman.utils.KeyEventListener;
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner;
import com.madlab.wms.grinfeld.roman.entities.ProductParam;
import com.madlab.wms.grinfeld.roman.entities.ScannerData;
import com.madlab.wms.grinfeld.roman.entities.GoodsInDoc;
import com.madlab.wms.grinfeld.roman.entities.Order;
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner;

import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner;
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent;
import com.madlab.wms.grinfeld.roman.ui.MainActivity;
import com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.ordercard.OrderCardFragment;
import com.madlab.wms.grinfeld.roman.api.Command;
import com.madlab.wms.grinfeld.roman.utils.Preference;
import com.madlab.wms.grinfeld.roman.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by GrinfeldRa
 */
public class OrderFragment extends MvpAppCompatFragment implements OrderView, IOnScannerEvent, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener, KeyEventListener {

    private static final String KEY_DATA = "key";
    private static final String KEY_TYPE = "key_type";
    private static final String TAG = "#OrderFragment";
    private Context context;
    private TextView
            txt_date_exp,
            txt_count,
            txt_te,
            txt_order_type;
    private View footer;
    private Order order;
    private ProgressOwner progressOwner;
    private int currentPosition;
    private ArrayList<GoodsInDoc> data = new ArrayList<>();
    private OrderAdapter orderAdapter;
    private AbstractScanner scanner;
    private ListView listView;
    private float leftWeight = 0.4f;
    private float rightWeight = 1f;
    private int type = 0;


    @InjectPresenter
    OrderPresenter presenter;

    @ProvidePresenter
    OrderPresenter providePresenter() {
        return new OrderPresenter();
    }

    public static OrderFragment newInstance(Order order, int type) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_DATA, order);
        args.putInt(KEY_TYPE, type);
        OrderFragment fragment = new OrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ProgressOwner) {
            progressOwner = (ProgressOwner) context;
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.good_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = view.findViewById(R.id.list_view);
        txt_date_exp = view.findViewById(R.id.txt_date_exp);
        txt_count = view.findViewById(R.id.txt_count);
        txt_te = view.findViewById(R.id.txt_te);
        footer = view.findViewById(R.id.footer);
        txt_order_type = view.findViewById(R.id.txt_order_type);
        orderAdapter = new OrderAdapter(context, data);
        listView.setAdapter(orderAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemSelectedListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Preference.getOrderConditionMode() == Command.TYPE_CONDITION) {
            txt_order_type.setText(getString(R.string.condition));
        } else {
            txt_order_type.setText(getString(R.string.defective));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume" );
        scanner = new FactoryScanner().getScanner(context, this);
        scanner.onResume();
        if (getArguments() != null) {
            order = getArguments().getParcelable(KEY_DATA);
            type = getArguments().getInt(KEY_TYPE);
            presenter.getGoodsInOrder(type, order);
        }
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).addKeyEventHandler(this);
        }
        listView.setOnKeyListener(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause" );
        if (context instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) context).removeKeyEventHandler(this);
        }
        listView.setOnKeyListener(null);
    }

    @Override
    public void onDetach() {
        scanner.onStop();
        super.onDetach();
    }


    @Override
    public void onCloseOrder(String s) {
        if (getActivity() != null) {
            data.clear();
            new AlertDialog.Builder(context)
                    .setMessage(s)
                    .setPositiveButton("OK", (dialog, which) -> {
                        if (getActivity() != null) {
                            getActivity().onBackPressed();
                        }
                    }).show();
        }
    }

    @Override
    public void onCompleteDeleteGoods() {
        data.remove(currentPosition);
        orderAdapter.notifyDataSetChanged();
        presenter.getGoodsInOrder(type, order);
    }

    @Override
    public void onCompleteGetGoodsInOrder(List<GoodsInDoc> goodsInDoc) {
        data.clear();
        data.addAll(goodsInDoc);
        orderAdapter = new OrderAdapter(context, data);
        listView.setAdapter(orderAdapter);
    }


    @Override
    public void onCompleteLoadParamPallet(String response, ScannerData scannerData) {
        if (response.contains("OK")) {
            if (getActivity() != null) {
                scanner.onStop();
                ((MainActivity) getActivity()).changeFragment(OrderCardFragment.newInstance(null,
                        scannerData, order, Command.TYPE_GET, data, type));
            }
        } else {
            try {
                String[] error = response.split(";");
                Toast.makeText(context, error[1], Toast.LENGTH_LONG).show();
            } catch (ArrayIndexOutOfBoundsException e) {
                showError(e.getMessage());
            }
        }
    }

    @Override
    public void onCompleteSentToPrintTE() {
        Toast.makeText(context, getString(R.string.print_fragment_complete), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorLoadParamGoods(ScannerData scannerData, String s) {
        Spanned spanned;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            spanned = Html.fromHtml(s + "</br> Код: " + scannerData.getBarcode(), Html.FROM_HTML_MODE_COMPACT);
        } else {
            spanned = Html.fromHtml(s + "</br> Код: " + scannerData.getBarcode());
        }
        new AlertDialog.Builder(context)
                .setMessage(spanned)
                .setNegativeButton(getString(R.string.ok), null)
                .show();
    }

    @Override
    public void onCompleteLoadParamGoods(ProductParam productParam, ScannerData scannerData, int position) {
        if (getActivity() != null) {
            scanner.onStop();
            ((MainActivity) getActivity()).changeFragment(OrderCardFragment.newInstance(productParam, scannerData, order, position, data, type));
        }
    }


    @Override
    public void showError(String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressOwner.setProgressState(true);
    }

    @Override
    public void hideProgress() {
        progressOwner.setProgressState(false);
    }


    @Override
    public void onDataScanner(ScannerData scannerData) {
        switch (type) {
            case Command.TYPE_ORDER:
                if (scannerData.getTe() != null) {
                    if (Preference.getOrderConditionMode() == Command.TYPE_DEFECTIVE) {
                        Toast.makeText(context, getString(R.string.te_no_need), Toast.LENGTH_SHORT).show();
                    } else {
                        presenter.getParamOnPallet(order.getNumDoc(), order.getDateDoc(), scannerData);
                    }
                } else {
                    presenter.loadParamFromScanCode(order, scannerData, Command.TYPE_GET,0);
                }
                break;
            case Command.TYPE_RETURN:
                if (scannerData != null && scannerData.getBarcode() != null) {
                    presenter.loadParamFromScanCode(order, scannerData, Command.TYPE_GET,0);
                }else {
                    showError(getString(R.string.scan_euz));
                }
                break;
        }
    }

    @Override
    public void onStatusUpdateScanner(String scanStatus) {
        Log.d(TAG, scanStatus);
    }

    @Override
    public void onErrorScanner(String error) {
        Log.d(TAG, error);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
            AlertDialog alertDialog = new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage(getString(R.string.delete_item))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                        GoodsInDoc goodsInDoc = data.get(currentPosition);
                        presenter.deleteGoods(order.getNumDoc(), goodsInDoc.getPosition());
                    })
                    .setNegativeButton(getString(R.string.no), null)
                    .create();
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
            return true;
        } else if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_B) {
            txt_order_type.setText(getString(R.string.defective));
            Preference.setOrderConditionMode(Command.TYPE_DEFECTIVE);
            presenter.getGoodsInOrder(type, order);
            return true;
        } else if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_K) {
            txt_order_type.setText(getString(R.string.condition));
            Preference.setOrderConditionMode(Command.TYPE_CONDITION);
            presenter.getGoodsInOrder(type, order);
            return true;
        } else if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_P) {
            if (getActivity() != null) {
                ViewPager viewPager = getActivity().findViewById(R.id.viewpager);
                viewPager.setCurrentItem(1);
            }
            return true;
        } else if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_T) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.dialog1_te_message);
            String[] items = {"EUR", "FIN", "US"};
            int checkedItem = 0;
            builder.setSingleChoiceItems(items, checkedItem, (dialog, which) -> {
                AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                builder2.setTitle(getString(R.string.dialog2_te_message));
                final View customLayout = getLayoutInflater().inflate(R.layout.dialog_etxt, null);
                builder2.setView(customLayout);
                builder2.setPositiveButton(getString(R.string.ok), (dialog1, which1) -> {
                    EditText editText = customLayout.findViewById(R.id.editText);
                    presenter.sentToPrintTE(which + 1, Integer.parseInt(editText.getText().toString()));
                    dialog.dismiss();
                });
                AlertDialog dialog2 = builder2.create();
                if (!dialog2.isShowing()) {
                    dialog2.show();
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), null);
            AlertDialog dialog = builder.create();
            if (!dialog.isShowing()) {
                dialog.show();
            }
            return true;
        }

        return false;

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        footer.setVisibility(View.VISIBLE);
        currentPosition = position;
        GoodsInDoc goodsInDoc = (GoodsInDoc) parent.getItemAtPosition(currentPosition);
        txt_date_exp.setText(getString(R.string.shelf_life, goodsInDoc.getShelfLife()));
        txt_count.setText(getString(R.string.count, goodsInDoc.getCountStr()));
        if (goodsInDoc.getTe() == null || goodsInDoc.getTe().isEmpty()) {
            txt_te.setVisibility(View.GONE);
        } else {
            txt_te.setVisibility(View.VISIBLE);
            txt_te.setText(getString(R.string.te, goodsInDoc.getTe()));
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        footer.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        GoodsInDoc goodsInDoc = (GoodsInDoc) parent.getItemAtPosition(position);
        ScannerData scannerData = new ScannerData(goodsInDoc.getCode());
        scannerData.setTe(goodsInDoc.getTe());
        scannerData.setCount(goodsInDoc.getCount());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        try {
            scannerData.setProduction_date(simpleDateFormat.parse(goodsInDoc.getProdDate()));
            scannerData.setExpiration_date(simpleDateFormat.parse(goodsInDoc.getShelfLife()));
        } catch (ParseException e) {
            Log.d(TAG, e.getMessage());
        }

        presenter.loadParamFromScanCode(order, scannerData, Command.TYPE_EDIT, goodsInDoc.getPosition());
    }
}
