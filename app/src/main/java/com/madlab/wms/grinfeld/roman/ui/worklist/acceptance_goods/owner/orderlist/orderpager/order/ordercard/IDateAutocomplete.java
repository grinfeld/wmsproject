package com.madlab.wms.grinfeld.roman.ui.worklist.acceptance_goods.owner.orderlist.orderpager.order.ordercard;

/**
 * Created by GrinfeldRa
 */
public interface IDateAutocomplete {

    void onDateAutocomplete();

}
