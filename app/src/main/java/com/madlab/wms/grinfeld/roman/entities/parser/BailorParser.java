package com.madlab.wms.grinfeld.roman.entities.parser;

import com.madlab.wms.grinfeld.roman.entities.Bailor;

import java.util.ArrayList;
import java.util.List;

public class BailorParser {

    public static List<Bailor> getList(String dataFromServer) {
        ArrayList<Bailor> list = new ArrayList<>();
        if (dataFromServer == null)
            return list;
        String[] rows = dataFromServer.split("[|]");
        for (String row : rows) {
            String[] values = row.split(";");
            list.add(new Bailor(
                    values[0],
                    values[1],
                    Integer.parseInt(values[2]),
                    Integer.parseInt(values[3]),
                    Integer.parseInt(values[4])));
        }
        return list;
    }
}
