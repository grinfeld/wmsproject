package com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_select_euz

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.madlab.wms.grinfeld.roman.R
import com.madlab.wms.grinfeld.roman.adapter.MyAdapter
import com.madlab.wms.grinfeld.roman.api.Command
import com.madlab.wms.grinfeld.roman.common.BaseFragmentActivity
import com.madlab.wms.grinfeld.roman.utils.ProgressOwner
import com.madlab.wms.grinfeld.roman.entities.InventoryCellProduct
import com.madlab.wms.grinfeld.roman.entities.InventoryEUZ
import com.madlab.wms.grinfeld.roman.entities.ScannerData
import com.madlab.wms.grinfeld.roman.scanner.AbstractScanner
import com.madlab.wms.grinfeld.roman.scanner.FactoryScanner
import com.madlab.wms.grinfeld.roman.scanner.IOnScannerEvent
import com.madlab.wms.grinfeld.roman.ui.MainActivity
import com.madlab.wms.grinfeld.roman.ui.worklist.inventory.inventory_product.InventoryProductList
import com.madlab.wms.grinfeld.roman.utils.Utils
import kotlinx.android.synthetic.main.inventory_list_euz.*

/**
 * Created by grinfeldra
 */
const val KEY_LIST_EUZ = "key_euz"

class InventorySelectEUZFragment : MvpAppCompatFragment(), InventorySelectEUZView, AdapterView.OnItemSelectedListener, IOnScannerEvent, AdapterView.OnItemClickListener {

    @InjectPresenter
    lateinit var presenter: InventorySelectEUZPresenter

    @ProvidePresenter
    fun providePresenter() = InventorySelectEUZPresenter()

    private lateinit var scanner: AbstractScanner
    private lateinit var progressOwner: ProgressOwner


    fun newInstance(listEUZ: ArrayList<InventoryEUZ>) = InventorySelectEUZFragment().apply {
        arguments = Bundle().apply {
            putParcelableArrayList(KEY_LIST_EUZ, listEUZ)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProgressOwner) {
            progressOwner = context
        }
    }

    override fun onResume() {
        super.onResume()
        scanner = FactoryScanner().getScanner(context, this)
        scanner.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as BaseFragmentActivity).setActionBarTitle(getString(R.string.inventory_euz))
        return inflater.inflate(R.layout.inventory_list_euz, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = arguments?.getParcelableArrayList<InventoryEUZ>(KEY_LIST_EUZ)
        val adapter = MyAdapter<InventoryEUZ>(context, list)
        list_view.adapter = adapter
        adapter.setWeight(1f, 0.7f)
        Utils.setParamFromHeader(view, 1f, 0.7f, getString(R.string.name), getString(R.string.state))
        list_view.onItemSelectedListener = this
        list_view.onItemClickListener = this
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val inventoryEUZ = parent?.getItemAtPosition(position) as InventoryEUZ
        presenter.loadInventoryNum(inventoryEUZ.code)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val inventoryEUZ = parent?.getItemAtPosition(position) as InventoryEUZ
        txt1.text = inventoryEUZ.code.plus("\\").plus(inventoryEUZ.warehouse)
        txt2.text = inventoryEUZ.name
    }

    override fun onCompleteLoadProductInInventoryEUZ(list: ArrayList<InventoryCellProduct>?,
                                                     createDate: String, inventoryNum: String) {
        (activity as MainActivity).changeFragment(InventoryProductList.newInstance(list,
                inventoryNum, createDate, Command.TYPE_EUZ))
    }

    override fun onDataScanner(goodsCard: ScannerData?) {
        if (goodsCard?.barcode != null) {
            presenter.loadProductFromBarcode(goodsCard.barcode)
        } else {
            showError(getString(R.string.error_barcode))
        }
    }

    override fun onStatusUpdateScanner(scanStatus: String?) {

    }

    override fun onErrorScanner(error: String?) {

    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressOwner.setProgressState(true)
    }

    override fun hideProgress() {
        progressOwner.setProgressState(false)
    }

    override fun onPause() {
        super.onPause()
        scanner.onPause()
    }

    override fun onStop() {
        super.onStop()
        scanner.onStop()
    }
}